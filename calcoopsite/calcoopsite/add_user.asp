<%@Language=JavaScript%>
<!--#include file="javascript/verify_state.js"-->
<%
Response.Buffer = true
%>
<%
verify_state();
%>
<html>
<head>
<title>Add User</title>
</head>
<body bgcolor="#C0C0C0">
<form name="NewUserForm" method="post" action="sql_insert.asp" onsubmit=hash()>
<p><font color="#3333CC" size="1" face="Verdana">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To add a new user, enter the following:</font></p>
<table border="0" width="37%">
  <tr>
    <td width="44%">
      <p align="right"><font size="1" face="Verdana" color="#3333CC">Username:</font></td>
    <td width="67%"><input type="text" name="new_username"></td>
  </tr>
  <tr>
    <td width="44%">
      <p align="right"><font size="1" face="Verdana" color="#3333CC">Password:</font></td>
    <td width="67%"><!--webbot bot="Validation" B-Value-Required="TRUE"
      I-Minimum-Length="8" --><input type="password" name="new_password" size="20"></td>
  </tr>
  <tr>
    <td width="67%">
      <p align="right"><font size="1" face="Verdana" color="#3333CC">Re-type Password:</font>
    </td>
    <td width="40%">
      <!--webbot bot="Validation" B-Value-Required="TRUE" I-Minimum-Length="8"
      -->
      <input type="password" name="retyped_password" size="20">
    </td>
  </tr>
  <tr>
    <td width="50%">
      <p align="right"><font face="Verdana" size="1" color="#3333CC">User Role:</font>
    </td>
    <td width="50%">
      <p align="left"><select size="1" name="user_role">
      <option>Portsampler
      <option>Poweruser
      <option>Manager
      </select></p>
    </td>
  </tr>
  <tr>
    <td width="50%">
      <p align="right"><font face="Verdana" size="1" color="#3333CC">Port
      Complex:</font>
    </td>
    <td width="50%">
      <input type="text" name="port_complex" value="0" size="20">
    </td>
  </tr>
  <tr>
    <td width="100%" colspan="2">
      <p align="center"><font face="Verdana" size="1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="submit" value="Submit"></font></p>
    </td>
  </tr>
</table>
<input type="hidden" name="hash_value" value="0">
<input type="hidden" name="submitOK" value="0">
<script src="javascript/md5.js"></script>
<script type="text/javascript">
function hash()
{
x=document.NewUserForm
user=x.new_username.value
pass=x.new_password.value
retype=x.retyped_password.value
if (user == "" || pass == "" || retype == "") {
	alert("All fields must be provided!")
} else if (pass != retype) {
	alert("You misstyped the password!")
} else {
test=hex_md5(x.new_password.value)
x.hash_value.value=test
x.submitOK.value = 1
}
}
</script>
</form>
</body>
</html>
