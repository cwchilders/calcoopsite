<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RS_MATURITY
Dim RS_MATURITY_cmd
Dim RS_MATURITY_numRows

Set RS_MATURITY_cmd = Server.CreateObject ("ADODB.Command")
RS_MATURITY_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RS_MATURITY_cmd.CommandText = "SELECT * FROM calcom.dbo.maturity_codes ORDER BY species_group, sex, code" 
RS_MATURITY_cmd.Prepared = true

Set RS_MATURITY = RS_MATURITY_cmd.Execute
RS_MATURITY_numRows = 0
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Code List Home</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.BasicSubhead {
	font-family: "Times New Roman", Times, serif;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body background="/calcoopsite/images/waves 038_SMALL.jpg">
<div align="center" class="BasicHeading1">CODE LIST PAGE</div>
<p><span class="BasicSubhead"><span class="BasicSubhead">From this page you can access our current codes. You will have the option of downloading the code list as an ASCII, comma delimited file</span>. To return to the previous page, use the back button on your browser.</span></p>

<form>
<p align="center" class="BasicSubhead style1"><a href="mat_code_list.asp">Maturity Codes</a></p>
<p align="center" class="BasicSubhead style1"><a href="species_code_list.asp">Species Codes</a></p>
<p align="center" class="BasicSubhead style1"><a href="mcat_code_list.asp">Market Categories Codes</a></p>
<p align="center" class="BasicSubhead style1"><a href="port_code_list.asp">Port Codes</a></p>
<p align="center" class="BasicSubhead style1"><a href="gear_code_list.asp">Gear Codes</a></p>
<p align="center" class="BasicSubhead style1"><a href="size_code_list.asp">Size Codes</a></p>

<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>

<p>&nbsp;</p>
<p align="center" class="BasicSubhead style1"><a href="documentation.asp">DOCUMENTATION PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="qry_all_home.asp">DATA RETRIEVAL PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="default.asp">HOME PAGE</a></p>
</form>
</body>
</html>
<%
RS_MATURITY.Close()
Set RS_MATURITY = Nothing
%>
