<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Fishery Data Documentation</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling">
<meta name="description" content="This page provides access to CALCOM database and port sampling documentation." />
<link href="User Login_NAV4Layer.css" rel="stylesheet" type="text/css" />
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {font-size: 10pt}
.BasicHeading1 .BasicHeading1 {
	font-size: 24pt;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body class="BasicHeading1">
<div align="center" class="BasicHeading1">Documentation For the CALCOM Database</div>
<p align="center" class="OceanSubhead style3">&nbsp;</p>

<p align="left" class="BasicSubhead">CALCOM is the database used by the California Cooperative Groundfish Survey.  It is the repository for all raw and processed data from the program.  Some of these data are available from PacFIN; however, PacFIN is not updated as often as CALCOM and does include all of the datasets available.  Most of the data is for groundfish; however, we have landings for all commercial species from California from 1916 through the current year.
  The data managers are committed to making the data readily available to all users.  To this end, we support three methods of obtaining data from the system: 1) via this website, 2) through direct connections to the database, and 3) by requesting us to extract the data for you. Note that data governed by confidentiality is not available through this website.</p>
<p align="left" class="BasicSubhead">Direct access using Open DataBase Connectivity (ODBC) is a technology that allows a huge variety of programs, working on on virtually any operating system, to directly access relational databases.  Some of the programs that can use this technology are: Microsoft Access, Excel, R, SAS, Matlab, Visual Studio and ARC View. For more information about making ODBC connections to CALCOM, contact <a href="mailto:don.pearson@noaa.gov">Don Pearson</a> (NMFS).</p>
<p align="left" class="BasicSubhead">The third method of access is by contacting the data managers and having us extract the data for you.  This is the appropriate method for one time access, or for complicated data requests.  The data managers will respond as promptly as possible.</p>

<p align="left" class="BasicSubhead">&nbsp;</p>
<p align="center" class="OceanSubhead style3"><a href="download/CALCOM_Data_availability.pdf">Summary of Available Data</a></p>
<p align="center" class="OceanSubhead style3"><a href="master_document/SqlServer.PORICHTHYS.calcom/default.htm">Master Database Documentation</a></p>
<p align="center" class="OceanSubhead style3"><a href="download/blocks_regions_complexes.pdf">CDFG Blocks, Port Complexes and Coastal Regions Charts</a></p>
<p align="center" class="OceanSubhead style3"><a href="code_list_home.asp">Code lists</a></p>
<p align="center" class="OceanSubhead style3"><a href="download/User_forms.pdf">User access forms and otolith request form</a></p>
<p align="center" class="OceanSubhead style3"><a href="download/CALCOM_REFERENCES.pdf">Useful Literature</a></p>
<p align="center" class="OceanSubhead style3">&nbsp;</p>

<p align="center" class="OceanSubhead style3"><a href="qry_all_home.asp" class="BLD">DATA ACCESS PAGES</a></p>
<p align="center" class="OceanSubhead style3"><a href="default.asp" class="BLD">RETURN TO HOME PAGE</a></p>

</body>
</html>
