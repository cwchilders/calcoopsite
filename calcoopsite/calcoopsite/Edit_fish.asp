<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RSFISH__MMColParam
RSFISH__MMColParam = Session("SampleNo")
If (Request.form("Sample_no") <>"") Then 
 RSFISH__MMColParam = request.form("sample_no")
 else
 RSFISH_MMColParam = Session("SampleNo")
 
End If

%>
<%
Dim RSFISH
Dim RSFISH_cmd
Dim RSFISH_numRows

Set RSFISH_cmd = Server.CreateObject ("ADODB.Command")
RSFISH_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSFISH_cmd.CommandText = "SELECT * FROM calcom.dbo.current_fish WHERE sample_no = ? order by sample_no, clust_no, fish_no" 
RSFISH_cmd.Prepared = true
RSFISH_cmd.Parameters.Append RSFISH_cmd.CreateParameter("param1", 200, 1, 255, RSFISH__MMColParam) ' adVarChar

Set RSFISH = RSFISH_cmd.Execute
RSFISH_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RSFISH_numRows = RSFISH_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Fish Page</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>

<p align="center" class="BasicHeading1">EDIT FISH PAGE</p>

<p>&nbsp;</p>
<form id="form1" name="form1" method="post" action="update_fish.asp">
  <table border="1">
    <tr>
      <td>sample_no</td>
      <td>clust_no</td>
      <td>fish_no</td>
      <td>species</td>
      <td>sex</td>
      <td>maturity</td>
      <td>flength</td>
      <td>weight</td>
      <td>age</td>
      <td>&nbsp;</td>
    </tr>
    <% While ((Repeat1__numRows <> 0) AND (NOT RSFISH.EOF)) %>
      <tr>
        <td><%=(RSFISH.Fields.Item("sample_no").Value)%></td>
        <td><%=(RSFISH.Fields.Item("clust_no").Value)%></td>
        <td><%=(RSFISH.Fields.Item("fish_no").Value)%></td>
        <td><%=(RSFISH.Fields.Item("species").Value)%></td>
        <td><%=(RSFISH.Fields.Item("sex").Value)%></td>
         <td><%=(RSFISH.Fields.Item("maturity").Value)%></td>
        <td><%=(RSFISH.Fields.Item("flength").Value)%></td>
        <td><%=(RSFISH.Fields.Item("weight").Value)%></td>
        <td><%=(RSFISH.Fields.Item("age").Value)%></td>
        <td>
        <label><a href="Update_fish.asp?sampno=<%=(RSFISH.Fields.Item("sample_no").Value)%>&clustno=<%=(RSFISH.Fields.Item("clust_no").Value)%>&fishno=<%=(RSFISH.Fields.Item("fish_no").Value)%>">EDIT</a></label>        </td>
      </tr>
      <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  RSFISH.MoveNext()
Wend
%>
  </table>
</form>

<p align="center" class="CoolSubhead"><a href="Edit_Home.asp">Return To Edit Home Page</a></p>
<p align="center" class="CoolSubhead"><a href="PortSamplerPage.asp">Return To Sampler Home Page</a></p>
</body>
</html>
<%
RSFISH.Close()
Set RSFISH = Nothing
%>
