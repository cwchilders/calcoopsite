<%@ Language=JavaScript%>
<!--#include file ="javascript/IIS_Gen_3.0_ConnectionPool.js"-->
<!--#include file ="javascript/IIS_Gen_3.0_Recordset.js"-->
<!--#include file ="javascript/IIS_ServerHTMLEncode.js"-->
<!--#include file ="javascript/MkOptnFmRecordset.js"-->
<HTML>
<HEAD>
<META NAME="GENERATOR" CONTENT="Elemental Software Drumbeat 2000 3.01.369">
<LINK REL="StyleSheet" TYPE="text/css" HREF="User%20Login_NAV4Layer.css">
<TITLE>Sample Help</TITLE>
<SCRIPT LANGUAGE="JavaScript">
<!--
var Form1 = null;
var FormButton1 = null;
//-->
</SCRIPT>
<SCRIPT SRC="javascript/DBCommon.js"></SCRIPT>
<SCRIPT SRC="javascript/DBFrmBtn.js"></SCRIPT>
<SCRIPT SRC="javascript/DBForm.js"></SCRIPT>
<SCRIPT SRC="javascript/help_sample.js"></SCRIPT>

<SCRIPT SRC="javascript/DBRelPos.js"></SCRIPT>

<!-- CSS styles and positioning for IE 4 browsers. -->

<LINK REL="StyleSheet" TYPE="text/css" HREF="User%20Login_IE4.css">
<STYLE ID="IE4_ABSOLUTE_POSITIONING_STYLE_SHEET">
#Text1 {
	position:absolute;
	left:192;
	top:30;
	width:162px;
	z-index:1
}
#Text10 { position:absolute;left:60;top:70;width:600;z-index:11 }
#Text2 { position:absolute;left:60;top:120;width:410;z-index:3 }
#Text3 { position:absolute;left:60;top:160;width:420;z-index:4 }
#Text4 { position:absolute;left:60;top:200;width:540;z-index:5 }
#Text5 { position:absolute;left:60;top:230;width:469;z-index:6 }
#Text6 { position:absolute;left:60;top:270;width:540;z-index:7 }
#Text7 { position:absolute;left:60;top:310;width:640;z-index:8 }
#Text8 { position:absolute;left:60;top:350;width:610;z-index:9 }
#Text9 { position:absolute;left:60;top:390;width:540;z-index:10 }
#Text11 { position:absolute;left:60;top:420;width:510;z-index:12 }
#Text12 { position:absolute;left:60;top:460;width:480;z-index:13 }
#Text14 { position:absolute;left:60;top:490;width:341;z-index:15 }
#Text13 { position:absolute;left:60;top:520;width:450;z-index:14 }
#Text15 { position:absolute;left:60;top:560;width:350;z-index:16 }
#Text16 { position:absolute;left:60;top:600;width:500;z-index:17 }
#Text17 { position:absolute;left:60;top:660;width:590;z-index:18 }
#Text18 { position:absolute;left:60;top:740;width:590;z-index:19 }
#Text19 { position:absolute;left:60;top:820;width:580;z-index:20 }
#DBStyleFormButton1 { position:absolute;width:136;z-index:2 }
#EndOfPage { position:absolute;left:0;top:849;width:0 }
</STYLE>


<STYLE ID="LINK_STYLE_SHEET">
:link {text-decoration: none ;color: rgb(0,153,255);}
:visited {text-decoration: none ;color: rgb(0,128,0);}
:active {text-decoration: none ;color: rgb(255,128,64);}
:hover {font-family: Times New Roman, serif;font-style: normal;font-weight: 400;font-size: 12pt;text-align: left;color: rgb(255,0,0);}
body {
	background-color: #CCC;
}
</STYLE>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"></HEAD>
<BODY   BGColor="#80ffff" LEFTMARGIN=0 TOPMARGIN=0 MARGINWIDTH=0 MARGINHEIGHT=0>
<p>
<A NAME="StartOfPage"></A></p>
<p align="center" class="BasicHeading1">Help For Sample Page </p>
<P>
  <span class="BasicSubhead">CHECK ME :&nbsp; Check this box if there is something unusual about the sample - be sure to describe the problem in the comments box.</span></P>
<p class="BasicSubhead"> SAMPLE # :&nbsp; This must be a unique number and is required.</p>
<p class="BasicSubhead"> PORT :&nbsp; One of the available port codes.&nbsp; This is required.</p>
<p class="BasicSubhead"> BOAT# :&nbsp; A valid boat id or use one of the following:&nbsp; SHORE, SKIFF</p>
<p class="BasicSubhead"> DATE :&nbsp; Landed date in following format - 01/18/2001&nbsp; This is required.</p>
<p class="BasicSubhead"> GEAR : One of the available gear codes, UNK can be selected.&nbsp; This is required.</p>
<p class="BasicSubhead"> MARKET CATEGORY : Enter the market category the sample came from.&nbsp; This can be entered later.</p>
<p class="BasicSubhead"> LANDING WGT : Landing weight for this market category.&nbsp; This may be entered later.</p>
<p class="BasicSubhead"> TICKET # :&nbsp; Landing reciept ticket number (if known).&nbsp; Be sure to enter the letter code.</p>
<p class="BasicSubhead"> SAMPLER : Enter your lastname in UPPERCASE.&nbsp; This is required.</p>
<p class="BasicSubhead"> OTOLITHS:&nbsp; Check this box if any otoliths were collected in this sample.</p>
<p class="BasicSubhead"> LIVE :&nbsp; Check this box if this is a live fish sample.</p>
<p class="BasicSubhead"> COMMENTS:&nbsp; Add any comments needed:&nbsp; BE CONCISE.&nbsp; This field is not meant as a way to communicate general problems to the data managers.</p>
<p class="BasicSubhead"> COUNT OF SPECIES&nbsp; IN CLUSTER 1 :&nbsp; The number of different species in cluster 1.&nbsp; Be very careful.&nbsp; You will not be able to change this and you will be required to enter data for EXACTLY this number of species in cluster 1.&nbsp; This is required.</p>
<p class="BasicSubhead"> TOTAL WEIGHT OF CLUSTER 1 :&nbsp; Enter the total sampled weight of all species combined in CLUSTER 1.&nbsp; Be very careful, you will not be able to change this.&nbsp; When entering the data for this cluster, the individual weights of all species must add up to this.&nbsp; This is required.</p>
<p class="BasicSubhead"> COUNT AND WEIGHT OF CLUSTER 2 :&nbsp; If a second cluster was taken, repeat as above.</p>


<DIV ID="EndOfPage"><A NAME="EndOfPage"></A></DIV>
</BODY>
</HTML>
