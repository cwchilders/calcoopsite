<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim MM_editAction
MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Server.HTMLEncode(Request.QueryString)
End If

' boolean to abort record edit
Dim MM_abortEdit
MM_abortEdit = false
%>
<%
' IIf implementation
Function MM_IIf(condition, ifTrue, ifFalse)
  If condition = "" Then
    MM_IIf = ifFalse
  Else
    MM_IIf = ifTrue
  End If
End Function
%>
<%
If (CStr(Request("MM_update")) = "form2") Then
  If (Not MM_abortEdit) Then
    ' execute the update
    Dim MM_editCmd

    Set MM_editCmd = Server.CreateObject ("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_CALCOM_DB_STRING
    MM_editCmd.CommandText = "UPDATE calcom.dbo.current_samples SET sampler = ?, check_me = ?, cal_port = ?, boat_no = ?, sample_date = ?, gear = ?, mark_cat = ?, total_wgt = ?, pink_ticket = ?, pink_ticket2 = ?, pink_ticket3 = ?, otoliths = ?, live_fish = ?, observed_trip = ?, efp_trip= ?, ifq_trip = ?, dressed = ?, [size] = ?, comments = ? WHERE sample_no = ?" 
    MM_editCmd.Prepared = true
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param1", 201, 1, 15, Request.Form("sampler_rs")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param2", 201, 1, 1, MM_IIF(Request.Form("check_me"), request.form("check_me"), null)) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param3", 201, 1, 15, Request.Form("port_list")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param4", 5, 1, -1, Request.Form("boat_no")) ' addouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param5", 135, 1, -1, MM_IIF(Request.Form("Land_date"), Request.Form("Land_date"), null)) ' adDBTimeStamp
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param6", 201, 1, 5, Request.Form("Gear_list")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param7", 5, 1, -1, MM_IIF(Request.Form("mcat"), Request.Form("mcat"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param8", 5, 1, -1, MM_IIF(Request.Form("Land_wgt"), Request.Form("Land_wgt"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param9", 201, 1, 7, Request.Form("Rcpt1")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param10", 201, 1, 7, Request.Form("Rcpt2")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param11", 201, 1, 7, Request.Form("rcpt3")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param12", 201, 1, 1, Request.Form("otolith_list")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param13", 201, 1, 1, MM_IIF(Request.Form("Live_list"), request.form("live_list"), NULL)) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param14", 201, 1, 1, MM_IIF(Request.Form("observed_list"), request.form("observed_list"), NULL)) ' adLongVarChar
	MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param15", 201, 1, 1, MM_IIF(Request.Form("efp_list"), request.form("efp_list"), NULL)) ' adLongVarChar
	MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param16", 201, 1, 1, MM_IIF(Request.Form("ifq_list"), request.form("ifq_list"), NULL)) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param17", 201, 1, 1, Request.Form("Dressed_list")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param18", 201, 1, 3, Request.Form("Size_list")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param19", 201, 1, 250, Request.Form("Comments")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param20", 200, 1, 9, Request.Form("MM_recordId")) ' adVarChar
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    ' append the query string to the redirect URL
    Dim MM_editRedirectUrl
    MM_editRedirectUrl = "Edit_Home.asp"
    If (Request.QueryString <> "") Then
      If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0) Then
        MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
      Else
        MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
      End If
    End If
    Response.Redirect(MM_editRedirectUrl)
  End If
End If
%>
<%
Dim RSheader__RS_header_sno
RSheader__RS_header_sno = "1"
If (request.form("sample_no") <> "") Then 
  RSheader__RS_header_sno = request.form("sample_no")
End If
%>
<%
Dim RSheader_MMColParam
RSheader_MMColParam = "1"
If (Request.Form("sample_no")  <> "") Then 
  RSheader_MMColParam = Request.Form("sample_no") 
End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Header Data</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>

<FORM name="form1" method="post" action="">
  <table width="900" border="1" align="center">
    <caption align="top" class="BasicHeading1">
    Edit Header Data Page
    </caption>
     <tr>
    <td height="49" bordercolor="3" class="CoolSubhead"><div align="center">
    <input type="button" name="headhelp" id="headhelp" onclick="MM_openBrWindow('help_sample.asp','','scrollbars=yes,width=600,height=500')"value="Page Help" /></div></td>
    <td valign="middle" class="CoolSubhead"></a>
    <div align="center">
    <input type="button" name="samplist" id="samplist" onclick="MM_openBrWindow('sample_list.asp','','scrollbars=yes,width=1000,height=500')"value="Sample List" /></div></td>
    <td><div align="center">
    <input type="button" name="codelist" id="codelist" onclick="MM_openBrWindow('code_list_home.asp','','scrollbars=yes,width=600,height=500')"value="Code List" /></div></td>
  </tr>
</table>

</FORM>
<!-- if request("sample_no")="" then %>
  <form id="form1" name="form1" method="post" action="edit_header.asp">
   <span class="CoolSubhead">Sample# to Edit</span>
   <input type="text" name="Sample_no" id="Sample_no" />
  <input type="submit" name="Submit_sample" id="Submit_sample" value="Get Sample#" />
</form>
  else %>
-->
<%
Dim RSheader
Dim RSheader_cmd
Dim RSheader_numRows
%>
<%
Dim RSheader__portcomp
RSheader__portcomp = "1"
If (Session("port_complex") <> "") Then 
  RSheader__portcomp = Session("port_complex")
End If
%>
<%
Set RSheader_cmd = Server.CreateObject ("ADODB.Command")
RSheader_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSheader_cmd.CommandText = "SELECT * FROM calcom.dbo.current_samples WHERE sample_no = ? and port_complex=?" 
RSheader_cmd.Prepared = true
RSheader_cmd.Parameters.Append RSheader_cmd.CreateParameter("param1", 200, 1, 255, RSheader__RS_header_sno) ' adVarChar
RSheader_cmd.Parameters.Append RSheader_cmd.CreateParameter("param2", 200, 1, 15, RSheader__portcomp) ' adVarChar

Set RSheader = RSheader_cmd.Execute
RSheader_numRows = 0
%>
<form id="form2" name="form2" method="POST" action="<%=MM_editAction%>">
  <p><strong>Sampler</strong>
    <input name="sampler_rs" type="text" id="sampler_rs" value="<%=(RSheader.Fields.Item("sampler").Value)%>" />
Check Me
    <select name="Check_me" id="check_me" title="<%=(RSheader.Fields.Item("check_me").Value)%>">
      <option value="Y" <%If (Not isNull((RSheader.Fields.Item("check_me").Value))) Then If ("Y" = CStr((RSheader.Fields.Item("check_me").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>Y</option>
<option value="N" <%If (Not isNull((RSheader.Fields.Item("check_me").Value))) Then If ("N" = CStr((RSheader.Fields.Item("check_me").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>N</option>
    </select>  
  <p><strong>Port</strong>
    <select name="port_list" id="port_list" title="<%=(RSheader.Fields.Item("cal_port").Value)%>">
    <option value="ALB" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("ALB" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>ALB</option>
      <option value="AVL" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("AVL" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>AVL</option>
      <option value="BCR" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("BCR" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>BCR</option>
<option value="BDG" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("BDG" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>BDG</option>
      <option value="BOL" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("BOL" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>BOL</option>
<option value="BRG" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("BRG" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>BRG</option>
      <option value="CRS" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("CRS" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>CRS</option>
      <option value="CRZ" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("CRZ" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>CRZ</option>
      <option value="DNA" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("DNA" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>DNA</option>
      <option value="ERK" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("ERK" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>ERK</option>
      <option value="FLN" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("FLN" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>FLN</option>
<option value="MCR" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("MCR" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>MCR</option>
      <option value="MNT" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("MNT" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>MNT</option>
      <option value="MOS" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("MOS" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>MOS</option>
<option value="MRO" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("MRO" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>MRO</option>
      <option value="NWB" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("NWB" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>NWB</option>
      <option value="OCA" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("OCA" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OCA</option>
      <option value="OCM" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("OCM" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OCM</option>
<option value="OCN" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("OCN" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OCN</option>
      <option value="OLA" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("OLA" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OLA</option>
      <option value="OSD" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("OSD" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OSD</option>
      <option value="OSL" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("OSL" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OSL</option>
      <option value="OXN" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("OXN" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OXN</option>
      <option value="PRN" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("PRN" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>PRN</option>
<option value="SB" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("SB" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>SB</option>
      <option value="SF" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("SF" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>SF</option>
      <option value="SIM" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("SIM" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>SIM</option>
      <option value="TRM" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("TRM" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>TRM</option>
<option value="VEN" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("VEN" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>VEN</option>
      <option value="WLM" <%If (Not isNull((RSheader.Fields.Item("cal_port").Value))) Then If ("WLM" = CStr((RSheader.Fields.Item("cal_port").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>WLM</option>
    </select>
Boat#
<input name="boat_no" type="text" id="boat_no" value="<%=(RSheader.Fields.Item("boat_no").Value)%>" />
  <strong>Landing Date</strong>
  <input name="Land_date" type="text" id="Land_date" value="<%=(RSheader.Fields.Item("sample_date").Value)%>" />
  </p>
  <p><strong>Gear</strong>
    <select name="GEAR_LIST" id="GEAR_LIST" title="<%=(RSheader.Fields.Item("gear").Value)%>">
      <option value="DGN" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("DGN" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>DGN</option>
      <option value="DNT" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("DNT" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>DNT</option>
      <option value="DVG" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("DVG" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>DVG</option>
      <option value="FPT" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("FPT" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>FPT</option>
      <option value="FTS" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("FTS" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>FTS</option>
      <option value="GFL" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("GFL" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>GFL</option>
      <option value="GFS" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("GFS" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>GFS</option>
      <option value="GFT" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("GFT" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>GFT</option>
      <option value="GLN" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("GLN" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>GLN</option>
      <option value="HKL" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("HKL" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>HKL</option>
      <option value="LGL" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("LGL" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>LGL</option>
      <option value="MDT" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("MDT" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>MDT</option>
      <option value="OTH" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("OTH" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OTH</option>
      <option value="RLT" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("RLT" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>RLT</option>
      <option value="SST" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("SST" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>SST</option>
      <option value="TRL" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("TRL" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>TRL</option>
      <option value="UNK" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("UNK" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>UNK</option>
      <option value="VHL" <%If (Not isNull((RSheader.Fields.Item("gear").Value))) Then If ("VHL" = CStr((RSheader.Fields.Item("GEAR").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>VHL</option>
    </select>
    <strong>Market Category</strong>
  <input name="mcat" type="text" id="mcat" value="<%=(RSheader.Fields.Item("mark_cat").Value)%>" />
  <strong>Landing Wgt</strong>
  <input name="Land_wgt" type="text" id="Land_wgt" value="<%=(RSheader.Fields.Item("total_wgt").Value)%>" />
  </p>
  <p>Reciept1
    <input name="Rcpt1" type="text" id="Rcpt1" value="<%=(RSheader.Fields.Item("pink_ticket").Value)%>" />
  Reciept2
  <input name="Rcpt2" type="text" id="Rcpt2" value="<%=(RSheader.Fields.Item("pink_ticket2").Value)%>" />
  Reciept3
  <input name="rcpt3" type="text" id="rcpt3" value="<%=(RSheader.Fields.Item("pink_ticket3").Value)%>" />
  </p>
  <p>Otoliths
    <select name="OTOLITH_LIST" id="OTOLITH_LIST" title="<%=(RSheader.Fields.Item("otoliths").Value)%>">
      <option value="Y" <%If (Not isNull((RSheader.Fields.Item("otoliths").Value))) Then If ("Y" = CStr((RSheader.Fields.Item("otoliths").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>Y</option>
<option value="N" <%If (Not isNull((RSheader.Fields.Item("otoliths").Value))) Then If ("N" = CStr((RSheader.Fields.Item("otoliths").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>N</option>
    </select>
    
  Live  
  <select name="LIVE_LIST" id="LIVE_LIST" title="<%=(RSheader.Fields.Item("live_fish").Value)%>">
    <option value="Y" <%If (Not isNull((RSheader.Fields.Item("live_fish").Value))) Then If ("Y" = CStr((RSheader.Fields.Item("live_fish").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>Y</option>
    <option value="N" <%If (Not isNull((RSheader.Fields.Item("live_fish").Value))) Then If ("N" = CStr((RSheader.Fields.Item("live_fish").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>N</option>
  <option value="P" <%If (Not isNull((RSheader.Fields.Item("live_fish").Value))) Then If ("P" = CStr((RSheader.Fields.Item("live_fish").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>P</option>
  
  </select>
  Observed
  <select name="OBSERVED_LIST" id="OBSERVED_LIST" title="<%=(RSheader.Fields.Item("observed_trip").Value)%>">
    <option value="Y" <%If (Not isNull((RSheader.Fields.Item("observed_trip").Value))) Then If ("Y" = CStr((RSheader.Fields.Item("observed_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>Y</option>
    <option value="N" <%If (Not isNull((RSheader.Fields.Item("observed_trip").Value))) Then If ("N" = CStr((RSheader.Fields.Item("observed_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>N</option>
<option value="C" <%If (Not isNull((RSheader.Fields.Item("observed_trip").Value))) Then If ("C" = CStr((RSheader.Fields.Item("observed_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>C</option>
<option value="U" <%If (Not isNull((RSheader.Fields.Item("observed_trip").Value))) Then If ("U" = CStr((RSheader.Fields.Item("observed_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>UNKNOWN</option>
  </select>
  EFP
  <select name="EFP_LIST" id="EFP_LIST" title="<%=(RSheader.Fields.Item("EFP_trip").Value)%>">
    <option value="Y" <%If (Not isNull((RSheader.Fields.Item("EFP_trip").Value))) Then If ("Y" = CStr((RSheader.Fields.Item("EFP_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>Y</option>
    <option value="N" <%If (Not isNull((RSheader.Fields.Item("EFP_trip").Value))) Then If ("N" = CStr((RSheader.Fields.Item("EFP_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>N</option>
<option value="U" <%If (Not isNull((RSheader.Fields.Item("EFP_trip").Value))) Then If ("U" = CStr((RSheader.Fields.Item("EFP_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>UNKNOWN</option>
  </select>
    IFQ
  <select name="IFQ_LIST" id="IFQ_LIST" title="<%=(RSheader.Fields.Item("IFQ_trip").Value)%>">
    <option value="Y" <%If (Not isNull((RSheader.Fields.Item("IFQ_trip").Value))) Then If ("Y" = CStr((RSheader.Fields.Item("IFQ_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>Y</option>
    <option value="N" <%If (Not isNull((RSheader.Fields.Item("IFQ_trip").Value))) Then If ("N" = CStr((RSheader.Fields.Item("IFQ_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>N</option>
<option value="U" <%If (Not isNull((RSheader.Fields.Item("IFQ_trip").Value))) Then If ("U" = CStr((RSheader.Fields.Item("IFQ_trip").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>UNKNOWN</option>
  </select>
  </p>
  <p>Dressed
    <select name="DRESSED_LIST" id="DRESSED_LIST" title="<%=(RSheader.Fields.Item("dressed").Value)%>">
      <option value="Y" <%If (Not isNull((RSheader.Fields.Item("dressed").Value))) Then If ("Y" = CStr((RSheader.Fields.Item("dressed").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>Y</option>
      <option value="N" <%If (Not isNull((RSheader.Fields.Item("dressed").Value))) Then If ("N" = CStr((RSheader.Fields.Item("dressed").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>N</option>
<option value="W" <%If (Not isNull((RSheader.Fields.Item("dressed").Value))) Then If ("W" = CStr((RSheader.Fields.Item("dressed").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>WINGS</option>
    </select>
  Size
  <select name="SIZE_LIST" id="SIZE_LIST" title="<%=(RSheader.Fields.Item("size").Value)%>">
    <option value="OR" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("OR" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>OCEAN RUN</option>
 <option value="XXL" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("XXL" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>XXL</option>
    <option value="XL" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("XL" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>XL</option>
    <option value="L" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("L" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>L</option>
    <option value="M" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("M" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>M</option>
    <option value="S" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("S" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>S</option>
    <option value="XS" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("XS" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>XS</option>
 <option value="XXS" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("XXS" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>XXS</option>
    <option value="UNK" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("UNK" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>UNKNOWN</option>
<option value="DIS" <%If (Not isNull((RSheader.Fields.Item("size").Value))) Then If ("DIS" = CStr((RSheader.Fields.Item("size").Value))) Then Response.Write("selected=""selected""") : Response.Write("")%>>DISCARD</option>    
  </select>
  </p>
  <p>Comments
    <textarea name="Comments" id="Comments" cols="45" rows="5"><%=(RSheader.Fields.Item("comments").Value)%></textarea>
  </p>
  <p>
    <input type="submit" name="submit" id="submit" value="Submit Changes" />
  </p>

  <input type="hidden" name="MM_update" value="form2" />
  <input type="hidden" name="MM_recordId" value="<%= RSheader.Fields.Item("sample_no").Value %>" />
</form>
<p>
  <%
RSheader.Close()
Set RSheader = Nothing
%>
  <!--
end if %>
-->
</p>
<form>
  <input type="button" value="Back To Previous Page"onclick="history.go(-1);return true;" />
</form>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>

