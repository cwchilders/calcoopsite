<%@LANGUAGE="JAVASCRIPT"%>
<% Response.Expires = 0 %>
<%
// redirect with URL parameters
MM_redirectPage = 'SampHeader_Entry.asp';
if (MM_redirectPage == '') MM_redirectPage = Request.ServerVariables('URL');
if (String(MM_redirectPage).indexOf('?') == -1) MM_redirectPage += '?' + Request.QueryString;

// First time through - display form only
Session("PageVisit") = 3;
if (String(Request("hidSubmit1")) == "undefined")
{
	// not in edit mode
	if (Session("EditMode") == false)
	{

		var SpeciesCodeC1 = new Array(Session("txtSpeciesCountC1"));
		var SpeciesWeightC1 = new Array(Session("txtSpeciesCountC1"));
		var FishCountC1 = new Array(Session("txtSpeciesCountC1"));

		var SpeciesCodeC2 = new Array(Session("txtSpeciesCountC2"));
		var SpeciesWeightC2 = new Array(Session("txtSpeciesCountC2"));
		var FishCountC2 = new Array(Session("txtSpeciesCountC2"));

		for (counter=1 ; counter <= Session("txtSpeciesCountC1") ; counter++)
		{
			SpeciesCodeC1[counter] = String(Request("txtSpeciesCodeC1" + String(counter)));
			SpeciesWeightC1[counter] = String(Request("txtSpeciesWeightC1" + String(counter)));
			FishCountC1[counter] = String(Request("txtFishCountC1" + String(counter)));
		}
		Session("SpeciesCodeC1") = SpeciesCodeC1;
		Session("SpeciesWeightC1") = SpeciesWeightC1;
		Session("FishCountC1") = FishCountC1;

		for (counter=1 ; counter <= Session("txtSpeciesCountC2") ; counter++)
		{
			SpeciesCodeC2[counter] = String(Request("txtSpeciesCodeC2" + String(counter)));
			SpeciesWeightC2[counter] = String(Request("txtSpeciesWeightC2" + String(counter)));
			FishCountC2[counter] = String(Request("txtFishCountC2" + String(counter)));
		}
		Session("SpeciesCodeC2") = SpeciesCodeC2;
		Session("SpeciesWeightC2") = SpeciesWeightC2;
		Session("FishCountC2") = FishCountC2;
	}
	// edit mode
	else
	{
		// create recordset from sampleno
		Session("SampleNo") = String(Session("xxSampleNo"));
		Session("PageVisit")= 3
        //Session("sampleno")="99001"
		// Count the clusters
		var rsFish = Server.CreateObject("ADODB.Recordset");
		rsFish.ActiveConnection = "dsn=calcom;uid=websi;pwd=70fixed;";
		rsFish.Source = "select distinct species, weight, total from calcom.dbo.current_clusts where sample_no = '" + Session("SampleNo") + "' and clust_no = 2 order by species"
		rsFish.CursorType = 3;
		rsFish.CursorLocation = 2;
		rsFish.LockType = 3;
		rsFish.Open();
		var rsFish_numRows = 0;

		Session("txtSpeciesCountC2") = rsFish.RecordCount;

		// Cluster 1 in Edit Mode
		var rsFish = Server.CreateObject("ADODB.Recordset");
		rsFish.ActiveConnection = "dsn=calcom;uid=websi;pwd=70fixed;";
		rsFish.Source = "select distinct current_clusts.species, current_clusts.weight, current_clusts.total, current_fish.fish_no from current_clusts, current_fish "
		rsFish.Source += "where current_clusts.sample_no=current_fish.sample_no and "
		rsFish.Source += "current_clusts.clust_no=current_fish.clust_no and "
		rsFish.Source += "current_clusts.species=current_fish.species and "
		rsFish.Source += "current_clusts.sample_no = '" + Session("SampleNo") + "' and current_clusts.clust_no = 1 order by current_fish.fish_no"

		rsFish.CursorType = 3;
		rsFish.CursorLocation = 2;
		rsFish.LockType = 3;
		rsFish.Open();
		var rsFish_numRows = 0;

		Session("txtSpeciesCountC1") = rsFish.RecordCount;

		var SpeciesCodeC1 = new Array(Session("txtSpeciesCountC1"));
		var SpeciesWeightC1 = new Array(Session("txtSpeciesCountC1"));
		var FishCountC1 = new Array(Session("txtSpeciesCountC1"));

		counter = 0;
		SpeciesCount = 0;
		LastSpecies = "";
		while (counter++ != Session("txtSpeciesCountC1"))
		{
			CurrentSpecies = String(rsFish("species"))

			if (LastSpecies != CurrentSpecies)//new species
				{
				LastSpecies = String(rsFish("species"))
				SpeciesCount += 1;

				SpeciesCodeC1[SpeciesCount] = String(rsFish("species"));
				SpeciesWeightC1[SpeciesCount] = String(rsFish("weight"));
				FishCountC1[SpeciesCount] = String(rsFish("total"));
				}
			rsFish.MoveNext();
		}

		Session("txtSpeciesCountC1") = SpeciesCount;
		Session("SpeciesCodeC1") = SpeciesCodeC1;
		Session("SpeciesWeightC1") = SpeciesWeightC1;
		Session("FishCountC1") = FishCountC1;

		// Cluster 2 in Edit Mode
		// if there are more than 0 species in cluster 2
		if (Session("txtSpeciesCountC2") > 0)
		{
			var rsFish = Server.CreateObject("ADODB.Recordset");
			rsFish.ActiveConnection = "dsn=calcom;uid=websi;pwd=70fixed;";
			rsFish.Source = "select distinct current_clusts.species, current_clusts.weight, current_clusts.total, current_fish.fish_no from current_clusts, current_fish "
			rsFish.Source += "where current_clusts.sample_no=current_fish.sample_no and "
			rsFish.Source += "current_clusts.clust_no=current_fish.clust_no and "
			rsFish.Source += "current_clusts.species=current_fish.species and "
			rsFish.Source += "current_clusts.sample_no = '" + Session("SampleNo") + "' and current_clusts.clust_no = 2 order by current_fish.fish_no"

			rsFish.CursorType = 3;
			rsFish.CursorLocation = 2;
			rsFish.LockType = 3;
			rsFish.Open();

			Session("txtSpeciesCountC2") = rsFish.RecordCount;

			var SpeciesCodeC2 = new Array(Session("txtSpeciesCountC2"));
			var SpeciesWeightC2 = new Array(Session("txtSpeciesCountC2"));
			var FishCountC2 = new Array(Session("txtSpeciesCountC2"));

			counter = 0;
			SpeciesCount = 0;
			LastSpecies = "";
			while (counter++ != Session("txtSpeciesCountC2"))
			{
				CurrentSpecies = String(rsFish("species"))

				if (LastSpecies != CurrentSpecies)//new species
					{
					LastSpecies = String(rsFish("species"))
					SpeciesCount += 1;

					SpeciesCodeC2[SpeciesCount] = String(rsFish("species"));
					SpeciesWeightC2[SpeciesCount] = String(rsFish("weight"));
					FishCountC2[SpeciesCount] = String(rsFish("total"));
					}
				rsFish.MoveNext();
			}

			Session("txtSpeciesCountC2") = SpeciesCount;

			Session("SpeciesCodeC2") = SpeciesCodeC2;
			Session("SpeciesWeightC2") = SpeciesWeightC2;
			Session("FishCountC2") = FishCountC2;
		}

		// Create recordset of all fish
		var rsFish = Server.CreateObject("ADODB.Recordset");
		rsFish.ActiveConnection = "dsn=calcom;uid=websi;pwd=70fixed;";
		rsFish.Source = "select * from calcom.dbo.current_fish where sample_no = '" + Session("SampleNo") + "' order by sample_no, clust_no, fish_no";
		rsFish.CursorType = 0;
		rsFish.CursorLocation = 2;
		rsFish.LockType = 3;
		rsFish.Open();
	}
}
// Insert or Update Mode
else
{
	var cmdFish = Server.CreateObject("ADODB.Command");
	cmdFish.ActiveConnection = "dsn=calcom;uid=websi;pwd=70fixed;";
    Session("PageVisit") = 3;
	counter = 0;
	RealFishCounter = 0
	while (counter++ != Session("txtSpeciesCountC1"))
	{
		fishcounter = 0
		while (fishcounter++ != Session("FishCountC1")[counter])
		{
			RealFishCounter += 1;

			if (Session("EditMode") == false)
			{	// Insert Mode
				fish_values = "'" + Session("SampleNo") + "', " + "1, "
				fish_values += RealFishCounter + ", "
				fish_values += "'" + (Session("SpeciesCodeC1")[counter]).toUpperCase() + "', "
				fish_values += Request("selSexC1" + counter + fishcounter) + ", "
				if (String(Request("txtLengthC1" + counter + fishcounter))== "") fish_values += "null,"; else fish_values += String(Request("txtLengthC1" + counter + fishcounter)) + ", "
				fish_values += Request("txtMaturityC1" + counter + fishcounter) + ", "
				if (Request("txtWeightC1" + counter + fishcounter) != "")
					fish_values += Request("txtWeightC1" + counter + fishcounter) + ", "
				else
					fish_values += "null, "
				fish_values += "null";
				cmdFish.CommandText = "INSERT INTO calcom.dbo.current_fish (sample_no, clust_no, fish_no, species, sex, flength, maturity, weight, age) VALUES (" + fish_values + ") ";
			}
			else
			{	// Update Mode
				fish_value = "SET sex = " + Request("selSexC1" + counter + fishcounter) + ", "
				if (String(Request("txtLengthC1" + counter + fishcounter))== "") fish_value += "flength = null,"; else fish_value += "flength = " + String(Request("txtLengthC1" + counter + fishcounter)) + ", "
				fish_value += "maturity = " + Request("txtMaturityC1" + counter + fishcounter) + ", "
				if (Request("txtWeightC1" + counter + fishcounter) != "")
					fish_value += "weight = " + Request("txtWeightC1" + counter + fishcounter)
				else
					fish_value += "weight = null "
				fish_value += " WHERE sample_no = '" + Session("SampleNo") + "' and clust_no = 1 and species = '" + Session("SpeciesCodeC1")[counter]+ "' and fish_no = " + RealFishCounter;
				cmdFish.CommandText = "UPDATE calcom.dbo.current_fish " + fish_value;
			}
			cmdFish.CommandType = 1;
			cmdFish.CommandTimeout = 0;
			cmdFish.Prepared = true;
			cmdFish.Execute();
		}
	}
	if (Session("txtSpeciesCountC2") > 0)
	{
		counter = 0;
		RealFishCounter = 0

		while (counter++ != Session("txtSpeciesCountC2"))
		{
			fishcounter = 0
			while (fishcounter++ != Session("FishCountC2")[counter])
			{
				RealFishCounter += 1;
				if (Session("EditMode") == false)
				{	// Insert Mode
					fish_values = "'" + Session("SampleNo") + "'," + "2,"
					fish_values += RealFishCounter + ","
					fish_values += "'" + (Session("SpeciesCodeC2")[counter]).toUpperCase() + "', "
					fish_values += Request("selSexC2" + counter + fishcounter) + ","
					if (String(Request("txtLengthC2" + counter + fishcounter))== "") fish_values += "null,"; else fish_values += String(Request("txtLengthC2" + counter + fishcounter)) + ","
					fish_values += Request("txtMaturityC2" + counter + fishcounter) + ","
					if (Request("txtWeightC2" + counter + fishcounter) != "")
						fish_values += Request("txtWeightC2" + counter + fishcounter) + ","
					else
						fish_values += "null, "
					fish_values += "null";

					cmdFish.CommandText = "INSERT INTO calcom.dbo.current_fish (sample_no, clust_no, fish_no, species, sex, flength, maturity, weight, age) VALUES (" + fish_values + ") ";
				}
				else
				{	// Update Mode
					fish_value = "SET sex = " + Request("selSexC2" + counter + fishcounter) + ", "
					
					if (String(Request("txtLengthC2" + counter + fishcounter))== "") fish_value += "flength = null,"; else fish_value += "flength = " + String(Request("txtLengthC2" + counter + fishcounter)) + ", "
					fish_value += "maturity = " + Request("txtMaturityC2" + counter + fishcounter) + ", "
					if (Request("txtWeightC2" + counter + fishcounter) != "")
						fish_value += "weight = " + Request("txtWeightC2" + counter + fishcounter)
					else
						fish_value += "weight = null "
					fish_value += " WHERE sample_no = '" + Session("SampleNo") + "' and clust_no = 2 and species = '" + Session("SpeciesCodeC2")[counter]+ "' and fish_no = " + RealFishCounter;
					cmdFish.CommandText = "UPDATE calcom.dbo.current_fish " + fish_value;
				}
				cmdFish.CommandType = 1;
				cmdFish.CommandTimeout = 0;
				cmdFish.Prepared = true;
				cmdFish.Execute();
			}
		}
	}
Response.Redirect(MM_redirectPage);
}
%>
<HTML>
<HEAD>
<TITLE>Fish</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="User Login_IE4.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
<style type="text/css">
<!--
.style1 {font-size: 14px}
.style2 {
	color: #0000FF;
	font-weight: bold;
}
.style3 {
	font-size: 24px;
	font-weight: bold;
}
.style4 {
	font-size: 18px;
	font-weight: bold;
	color: #FF0000;
}
body {
	background-color: #CCC;
}
-->
</style>
</HEAD>
<BODY bgcolor="#99FFFF">
<FORM name="form2" method="post" action="">
  <table width="900" border="1" ALIGN="CENTER">
    <caption align="top" class="BasicHeading1">
    <span class="style3">      FISH DATA ENTRY    </span>
    </caption>
     <tr>
    <td height="49" bordercolor="3" class="CoolSubhead"><div align="center">
    <input type="button" name="headhelp" id="headhelp" onClick="MM_openBrWindow('help_fish.asp','','scrollbars=yes,width=600,height=500')"value="Page Help" /></div></td>
    <td valign="middle" class="CoolSubhead"></a>
    <div align="center">
    <input type="button" name="samplist" id="samplist" onClick="MM_openBrWindow('sample_list.asp','','scrollbars=yes,width=600,height=500')"value="Sample List" /></div></td>
    <td><div align="center">
    <input type="button" name="codelist" id="codelist" onClick="MM_openBrWindow('code_list_home.asp','','scrollbars=yes,width=600,height=500')"value="Code List" /></div></td>
  </tr>
</table>
</FORM>
<p align="center" class="style4">DO NOT USE THE BACK BUTTON FROM THIS PAGE
<FORM name="form1" method="post" action="fish_entry.asp">
<SCRIPT language="JavaScript">
function ValidateForm()
{
	var msgNull = "";
	var msgMat = "";
	var msgLen = "";

	// Validate
	for (i=0; i < document.forms[1].elements.length; i++)
	{
		if (document.forms[1].elements[i].type == "text")
		{

			if (String(document.forms[1].elements[i].name).indexOf('txtMaturity') == 0)
			{
				if (document.forms[1].elements[i].value.length != 1)
					msgMat = "Maturity fields must be a single digit.\n";
			}
		}
	}

	if (msgNull + msgMat + msgLen != "")
	{
		alert(msgNull + msgMat + msgLen);
	}
	else
	{
		if (document.form1.hidSubmit1.value == "undefined")
		{
			document.form1.hidSubmit1.value="none";
		}
		document.form1.submit();
	}
}
</SCRIPT>
  <TABLE width="560" border="0" align="center" cellpadding="2" cellspacing="0">
    <TR>
      <TD colspan="3" bgcolor="#CCCCCC"><B><FONT size="3">Cluster #1</FONT></B></TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96">&nbsp;</TD>
      <TD width="207">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD colspan="2">&nbsp;</TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96">&nbsp;</TD>
      <TD width="207">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
	<% RealFishCounter = 0%>
    <% counter = 0%>
    <% while (counter++ < Session("txtSpeciesCountC1")) { %>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD colspan="2" bgcolor="#999999"><B>Species <%= counter%>:
        <%Response.write(Session("SpeciesCodeC1")[counter])%></B></TD>
      <TD width="97" bgcolor="#999999">&nbsp;</TD>
      <TD width="96" bgcolor="#999999">&nbsp;</TD>
      <TD width="207" bgcolor="#999999">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64"><span class="style1">Sex</span></TD>
      <TD width="97"><span class="style1">Maturity</span></TD>
      <TD width="96"><span class="style1">Length</span></TD>
      <TD width="207"><span class="style1">Weight</span></TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <% FishCounter = 0%>
    <% while (FishCounter++ < Session("FishCountC1")[counter]) { %>
    <% RealFishCounter += 1%>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">
        <DIV align="right"><span class="style1">Fish</span><FONT size="2"> <%=RealFishCounter%></FONT></DIV>
      </TD>
      <TD><SELECT name="selSexC1<%=counter%><%=FishCounter%>" id="selSexC1<%=counter%><%=FishCounter%>">
        <option value="9">9</option>
        <option value="1">1</option>
        <option value="2">2</option>
       </SELECT>
      </TD>
        <TD><Select name="txtMaturityC1<%=counter%><%=FishCounter%>" id="txtmaturityCl<%=counter%><%=FishCounter%>">
          <option value="9">9</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
         </Select>
      </TD>
      <TD width="96">
        <INPUT type="text" name="txtLengthC1<%=counter%><%=FishCounter%>" value="<%if (Session("EditMode") == true) Response.write(rsFish('flength'))%>"size="15">
      </TD>
      <TD width="207">
        <INPUT type="text" name="txtWeightC1<%=counter%><%=FishCounter%>" value="<%if (Session("EditMode") == true) Response.write(rsFish('weight'))%>"size="15">
      </TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <% if (Session("EditMode") == true) rsFish.MoveNext();
    } // While%>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64">&nbsp;</TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96" align="right">
        <DIV align="right"><%Response.write('Species Wt.:')%></DIV>
      </TD>
      <TD width="207"><%Response.write(Session("SpeciesWeightC1")[counter] + ' lbs.')%>
      </TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <%}%>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64">&nbsp;</TD>
      <TD colspan="2">
        <DIV align="right"><B><%Response.write('Total Cluster Wt')%> </B></DIV>
      </TD>
      <TD width="207"><B><%=Session("txtSpeciesWeightC1")%> lbs.
        </B></TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <TR>
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64">&nbsp;</TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96">&nbsp;</TD>
      <TD width="207">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <%if (Session("txtSpeciesCountC2") > 0){ %>
    <TR>
      <TD colspan="3" bgcolor="#CCCCCC"><B><FONT size="3">Cluster #2</FONT></B></TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96">&nbsp;</TD>
      <TD width="207">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD colspan="2">&nbsp;</TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96">&nbsp;</TD>
      <TD width="207">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <% RealFishCounter = 0%>
    <% counter = 0%>
    <% while (counter++ < Session("txtSpeciesCountC2")) { %>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD colspan="2" bgcolor="#999999"><B>Species <%= counter%>:
        <%Response.write(Session("SpeciesCodeC2")[counter])%></B></TD>
      <TD width="97" bgcolor="#999999">&nbsp;</TD>
      <TD width="96" bgcolor="#999999">&nbsp;</TD>
      <TD width="207" bgcolor="#999999">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64"><span class="style1">Sex</span></TD>
      <TD width="97"><span class="style1">Maturity</span></TD>
      <TD width="96"><span class="style1">Length</span></TD>
      <TD width="207"><span class="style1">Weight</span></TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <% FishCounter = 0%>
    <% while (FishCounter++ < Session("FishCountC2")[counter]) { %>
    <% RealFishCounter += 1%>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">
        <DIV align="right"><span class="style1">Fish</span><FONT size="2"> <%=RealFishCounter%></FONT></DIV>
      </TD>
      <TD><SELECT name="selSexC2<%=counter%><%=FishCounter%>" id="selSexC2<%=counter%><%=FishCounter%>">
        <option value="9">9</option>
        <option value="1">1</option>
        <option value="2">2</option>
        </SELECT>
      </TD>
        <TD><Select name="txtMaturityC2<%=counter%><%=FishCounter%>" id="txtmaturityC2<%=counter%><%=FishCounter%>">
          <option value="9">9</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
          <option value="6">6</option>
         </Select>
      </TD>
      <TD width="96">
        <INPUT type="text" name="txtLengthC2<%=counter%><%=FishCounter%>" value="<%if (Session("EditMode") == true) Response.write(rsFish('flength'))%>" size="15">
      </TD>
      <TD width="207">
        <INPUT type="text" name="txtWeightC2<%=counter%><%=FishCounter%>" value="<%if (Session("EditMode") == true) Response.write(rsFish('weight'))%>" size="15">
      </TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <% if (Session("EditMode") == true) rsFish.MoveNext();
    } // While%>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64">&nbsp;</TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96" align="right"><%Response.write('Species Wt:')%> </TD>
      <TD width="207"><%Response.write(Session("SpeciesWeightC2")[counter]+' lbs.')%>
      </TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <%}%>
    <TR bgcolor="#CCCCCC">
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64">&nbsp;</TD>
      <TD colspan="2">
        <DIV align="right"><B><%Response.write('Total Cluster Wt')%> </B></DIV>
      </TD>
      <TD width="207"><B><%= Session("txtSpeciesWeightC2")%> lbs.
        </B></TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <TR>
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp;</TD>
      <TD width="64">&nbsp;</TD>
      <TD width="97">&nbsp;</TD>
      <TD width="96">&nbsp;</TD>
      <TD width="207">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
    <%}%>
    <TR>
      <TD width="1">&nbsp;</TD>
      <TD width="54">&nbsp; </TD>
      <TD width="64">&nbsp;</TD>
      <TD width="97">
        <INPUT type="button" name="btnSaveFish" value="Save Fish " onClick = "ValidateForm();">
      </TD>
      <TD width="96">
        <INPUT type="hidden" name="hidSubmit1">
      </TD>
      <TD width="207">&nbsp;</TD>
      <TD width="13">&nbsp;</TD>
    </TR>
  </TABLE>
  <P>&nbsp;</P>
</FORM>
<p align="center" class="CoolSubhead"><a href="PortSamplerPage.asp">Exit and Return To Main Menu Without Saving Fish Data</a></p>
</BODY>
</HTML>

