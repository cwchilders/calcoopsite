<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../Connections/CALCOM_DB.asp" -->
<%
Dim MM_editAction
MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Server.HTMLEncode(Request.QueryString)
End If

' boolean to abort record edit
Dim MM_abortEdit
MM_abortEdit = false
%>
<%
' IIf implementation
Function MM_IIf(condition, ifTrue, ifFalse)
  If condition = "" Then
    MM_IIf = ifFalse
  Else
    MM_IIf = ifTrue
  End If
End Function
%>
<%
If (CStr(Request("MM_update")) = "form2") Then
  If (Not MM_abortEdit) Then
    ' execute the update
    Dim MM_editCmd

    Set MM_editCmd = Server.CreateObject ("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_CALCOM_DB_STRING
    MM_editCmd.CommandText = "UPDATE dbo.cc_com_fish SET sample_no = ?, clust_no = ?, fish_no = ?, species = ?, sex = ?, flength = ?, maturity = ?, weight = ?, age = ? WHERE sample_no = ?" 
    MM_editCmd.Prepared = true
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param1", 201, 1, 11, Request.Form("sample_no")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param2", 5, 1, -1, MM_IIF(Request.Form("clust_no"), Request.Form("clust_no"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param3", 5, 1, -1, MM_IIF(Request.Form("fish_no"), Request.Form("fish_no"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param4", 201, 1, 4, Request.Form("species")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param5", 5, 1, -1, MM_IIF(Request.Form("sex"), Request.Form("sex"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param6", 5, 1, -1, MM_IIF(Request.Form("flength"), Request.Form("flength"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param7", 201, 1, 1, Request.Form("maturity")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param8", 5, 1, -1, MM_IIF(Request.Form("weight"), Request.Form("weight"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param9", 5, 1, -1, MM_IIF(Request.Form("age"), Request.Form("age"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param10", 200, 1, 11, Request.Form("MM_recordId")) ' adVarChar
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    ' append the query string to the redirect URL
    Dim MM_editRedirectUrl
    MM_editRedirectUrl = "../Edit_Home.asp"
    If (Request.QueryString <> "") Then
      If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0) Then
        MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
      Else
        MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
      End If
    End If
    Response.Redirect(MM_editRedirectUrl)
  End If
End If
%>
<%
Dim RSfish__MMColParam
RSfish__MMColParam = "1"
If (Request.Form("sample_no") <> "") Then 
  RSfish__MMColParam = Request.Form("sample_no")
End If
%>
<%
Dim RSfish
Dim RSfish_cmd
Dim RSfish_numRows

Set RSfish_cmd = Server.CreateObject ("ADODB.Command")
RSfish_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSfish_cmd.CommandText = "SELECT * FROM dbo.cc_com_fish WHERE sample_no = ? order by clust_no, fish_no" 
RSfish_cmd.Prepared = true
RSfish_cmd.Parameters.Append RSfish_cmd.CreateParameter("param1", 200, 1, 255, RSfish__MMColParam) ' adVarChar

Set RSfish = RSfish_cmd.Execute
RSfish_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RSfish_numRows = RSfish_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>Untitled Document</title>

<link href="../User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #00FFFF;
}
-->
</style>

</head>

<body>
<div align="center" class="BasicHeading1">Edit Fish Page</div>
<p>&nbsp;</p>
<% if request("sample_no")="" then %>
<form id="form1" name="form1" method="post" action="../edit_fish.asp">
  <span class="CoolSubhead">Sample# to Edit</span>
  <input type="text" name="Sample_no" id="Sample_no" />
  <input type="submit" name="Submit_sample" id="Submit_sample" value="Get Sample#" />
</form>
<% else %>
<form id="form2" name="form2" method="POST" action="<%=MM_editAction%>">

<table border="1">
  <tr>
    <td>sample_no</td>
    <td>clust_no</td>
    <td>fish_no</td>
    <td>species</td>
    <td>sex</td>
    <td>flength</td>
    <td>maturity</td>
    <td>weight</td>
    <td>age</td>
  </tr>
  <% While ((Repeat1__numRows <> 0) AND (NOT RSfish.EOF)) %>
    <tr>
      <td><input name="sample_no" type="text" id="sample_no" value="<%=(RSfish.Fields.Item("sample_no").Value)%>" /></td>
      <td><input name="clust_no" type="text" id="clust_no" value="<%=(RSfish.Fields.Item("clust_no").Value)%>" /></td>
      <td><input name="fish_no" type="text" id="fish_no" value="<%=(RSfish.Fields.Item("fish_no").Value)%>" /></td>
      <td><input name="species" type="text" id="species" value="<%=(RSfish.Fields.Item("species").Value)%>" /></td>
      <td><input name="sex" type="text" id="sex" value="<%=(RSfish.Fields.Item("sex").Value)%>" /></td>
      <td><input name="flength" type="text" id="flength" value="<%=(RSfish.Fields.Item("flength").Value)%>" /></td>
      <td><input name="maturity" type="text" id="maturity" value="<%=(RSfish.Fields.Item("maturity").Value)%>" /></td>
      <td><input name="weight" type="text" id="weight" value="<%=(RSfish.Fields.Item("weight").Value)%>" /></td>
      <td><input name="age" type="text" id="age" value="<%=(RSfish.Fields.Item("age").Value)%>" /></td>
    </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  RSfish.MoveNext()
Wend
%>
</table>

<input type="hidden" name="MM_update" value="form2" />
<input type="hidden" name="MM_recordId" value="<%= RSfish.Fields.Item("sample_no").Value %>" />
<input type="submit" name="submit" id="submit" value="Submit" />
</form>
<% end if %>
</body>
</html>
<%
RSfish.Close()
Set RSfish = Nothing
%>
