<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="../Connections/CALCOM_DB.asp" -->
<%
Dim RSfish__MMColParam
RSfish__MMColParam = "1"
If (Request.Form("sample_no") <> "") Then 
  RSfish__MMColParam = Request.Form("sample_no")
End If
%>
<%
Dim RSfish
Dim RSfish_cmd
Dim RSfish_numRows

Set RSfish_cmd = Server.CreateObject ("ADODB.Command")
RSfish_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSfish_cmd.CommandText = "SELECT * FROM dbo.cc_com_fish WHERE sample_no = ? order by clust_no, fish_no" 
RSfish_cmd.Prepared = true
RSfish_cmd.Parameters.Append RSfish_cmd.CreateParameter("param1", 200, 1, 255, RSfish__MMColParam) ' adVarChar

Set RSfish = RSfish_cmd.Execute
RSfish_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RSfish_numRows = RSfish_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- TemplateBeginEditable name="doctitle" -->
<title>Untitled Document</title>
<!-- TemplateEndEditable -->
<link href="../User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #00FFFF;
}
-->
</style>
<!-- TemplateBeginEditable name="head" --><!-- TemplateEndEditable -->
</head>

<body>
<div align="center" class="BasicHeading1">Edit Fish Page</div>
<p>&nbsp;</p>
<% if request("sample_no")="" then %>
<form id="form1" name="form1" method="post" action="../edit_fish.asp">
  <span class="CoolSubhead">Sample# to Edit</span>
  <input type="text" name="Sample_no" id="Sample_no" />
  <input type="submit" name="Submit_sample" id="Submit_sample" value="Get Sample#" />
</form>
<% else %>
<table border="1">
  <tr>
    <td>sample_no</td>
    <td>clust_no</td>
    <td>fish_no</td>
    <td>species</td>
    <td>sex</td>
    <td>flength</td>
    <td>maturity</td>
    <td>weight</td>
    <td>age</td>
  </tr>
  <% While ((Repeat1__numRows <> 0) AND (NOT RSfish.EOF)) %>
    <tr>
      <td><%=(RSfish.Fields.Item("sample_no").Value)%></td>
      <td><%=(RSfish.Fields.Item("clust_no").Value)%></td>
      <td><%=(RSfish.Fields.Item("fish_no").Value)%></td>
      <td><%=(RSfish.Fields.Item("species").Value)%></td>
      <td><!-- TemplateBeginEditable name="editsex" --><%=(RSfish.Fields.Item("sex").Value)%><!-- TemplateEndEditable --></td>
      <td><!-- TemplateBeginEditable name="editflength" --><%=(RSfish.Fields.Item("flength").Value)%><!-- TemplateEndEditable --></td>
      <td><!-- TemplateBeginEditable name="editmaturity" --><%=(RSfish.Fields.Item("maturity").Value)%><!-- TemplateEndEditable --></td>
      <td><!-- TemplateBeginEditable name="EditRegion4" --><%=(RSfish.Fields.Item("weight").Value)%><!-- TemplateEndEditable --></td>
      <td><%=(RSfish.Fields.Item("age").Value)%></td>
    </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  RSfish.MoveNext()
Wend
%>
</table>
<% end if %>
</body>
</html>
<%
RSfish.Close()
Set RSfish = Nothing
%>
