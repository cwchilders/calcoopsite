<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Help Edit Fish</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">EDIT HELP PAGE</p>
<p class="BasicSubhead">From this page you can choose a sample number to edit. You can either delete a sample, edit header data, or edit fish data. Cluster data cannot be edited. To change cluster data the sample must be deleted and reentered. To edit a sample, enter the sample number in the correct option and press the submit button.</p>
<p>&nbsp;</p>
</body>
</html>
