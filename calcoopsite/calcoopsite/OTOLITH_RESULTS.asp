<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim rslands__SPCODE
rslands__SPCODE = "BCAC"
If (REQUEST.FORM("OTOSP") <> "") Then 
  rslands__SPCODE = REQUEST.FORM("OTOSP")
End If
%>
<%
Dim rslands
Dim rslands_cmd
Dim rslands_numRows
Dim filesys, txtfile

Set rslands_cmd = Server.CreateObject ("ADODB.Command")
rslands_cmd.ActiveConnection = MM_CALCOM_DB_STRING
rslands_cmd.CommandText = "SELECT YEAR, SPECIES, SOURCE, CT FROM calcom.dbo.WEB_OTO_VU WHERE SPECIES=?" 
rslands_cmd.Prepared = true
rslands_cmd.Parameters.Append rslands_cmd.CreateParameter("param1", 200, 1, 255, rslands__SPCODE) ' adVarChar

Set rslands = rslands_cmd.Execute
rslands_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rslands_numRows = rslands_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Groundfish Otolith Data</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling otoliths">
<meta name="description" content="This page is the otolith inventory result page for the California commercial fishery from the California Cooperative Groundfish Survey (CCGS) and CALCOM." />

<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {font-weight: bold}
.style2 {font-size: 18px}
-->
</style>
</head>

<body>
<p align="center" class="BasicHeading1">OTOLITH INVENTORY RESULTS</p>
<p>&nbsp;</p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>
<p>&nbsp;</p>
<p class="style1"><a href="/user_download/OTOLITH_CTS.txt" class="style2">download the file</a></p>
<p class="style2"><strong>Year, Species, Source, Total Number</strong></p>
<form id="form1" name="form1" method="post" action="">
  <table width="700" border="5">
  <% set filesys=createobject("scripting.filesystemobject")
     set txtfile=filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/OTOLITH_CTS.txt")
	 txtfile.writeline("year, species, source, total")
	 %>
    <% 
While ((Repeat1__numRows <> 0) AND (NOT rslands.EOF)) 
%>
      <tr>
        <td><%=(rslands.Fields.Item("YEAR").Value)%></td>
        <td><%=(rslands.Fields.Item("SPECIES").Value)%></td>
        <td><%=(rslands.Fields.Item("SOURCE").Value)%></td>
        <td><%=(rslands.Fields.Item("CT").Value)%></td>
      </tr>
      <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.writeline(rslands.Fields.Item("YEAR").Value &","& rslands.Fields.Item("SPECIES").Value &","& rslands.Fields.Item("SOURCE").Value &","& rslands.Fields.Item("CT").Value)
  rslands.MoveNext()
Wend
%>

    </table>
</form>

<p>&nbsp;</p>
</body>
</html>
<%
rslands.Close()
Set rslands = Nothing
%>
