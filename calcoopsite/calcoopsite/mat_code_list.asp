<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RS_MATURITY
Dim RS_MATURITY_cmd
Dim RS_MATURITY_numRows
dim filesys, txtfile
Set RS_MATURITY_cmd = Server.CreateObject ("ADODB.Command")
RS_MATURITY_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RS_MATURITY_cmd.CommandText = "select * from calcom.dbo.maturity_codes order by species_group, sex, code" 
RS_MATURITY_cmd.Prepared = true

Set RS_MATURITY = RS_MATURITY_cmd.Execute
RS_MATURITY_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RS_MATURITY_numRows = RS_MATURITY_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Maturity Codes</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {
	font-size: 24px;
	color: #FF0000;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">Maturity Codes</p>
<p>&nbsp;</p>
<p class="style1">After clicking the download link, right click on the file and select SAVE AS...Use the back button on your browser to return to the previous page</p>

<p>&nbsp;</p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>

<p>&nbsp;</p>
<p class="style1"><a href="download/mat_code.txt" class="style2">download the file</a></p>
<form id="form1" name="form1" method="post" action="">
  
  <table border="1">
    <tr>
      <td>species_group</td>
      <td>sex</td>
      <td>code</td>
      <td>description</td>
    </tr>
    <% set filesys = createobject("scripting.filesystemobject")
	   set txtfile= filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/mat_code.txt")
	   txtfile.WriteLine("species_group,sex,code,description")%>
	   
	   
     <%While ((Repeat1__numRows <> 0) AND (NOT RS_MATURITY.EOF)) %>
      <tr>
        <td><%=(RS_MATURITY.Fields.Item("species_group").Value)%></td>
        <td><%=(RS_MATURITY.Fields.Item("sex").Value)%></td>
        <td><%=(RS_MATURITY.Fields.Item("code").Value)%></td>
        <td><%=(RS_MATURITY.Fields.Item("description").Value)%></td>
      </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.WriteLine(RS_MATURITY.Fields.Item("species_group").Value &","& RS_MATURITY.Fields.Item("sex").Value &","& RS_MATURITY.Fields.Item("code").Value &","& RS_MATURITY.Fields.Item("description").Value)
  RS_MATURITY.MoveNext()
Wend
%>
  </table>
</form>

</body>
</html>
<%
RS_MATURITY.Close()
Set RS_MATURITY = Nothing
%>
