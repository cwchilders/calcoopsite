<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RSSpeciesCodes
Dim RSSpeciesCodes_cmd
Dim RSSpeciesCodes_numRows

Set RSSpeciesCodes_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes order by common_name" 
RSSpeciesCodes_cmd.Prepared = true

Set RSSpeciesCodes = RSSpeciesCodes_cmd.Execute
RSSpeciesCodes_numRows = 0
%>
<%
Dim RSSpeciesCodes0
Dim RSSpeciesCodes0_cmd
Dim RSSpeciesCodes0_numRows

Set RSSpeciesCodes0_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes0_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes0_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes where age_comp='Y' order by common_name" 
RSSpeciesCodes0_cmd.Prepared = true

Set RSSpeciesCodes0 = RSSpeciesCodes0_cmd.Execute
RSSpeciesCodes0_numRows = 0
%>
<%
Dim RSSpeciesCodes1
Dim RSSpeciesCodes1_cmd
Dim RSSpeciesCodes1_numRows

Set RSSpeciesCodes1_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes1_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes1_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes where len_comp='Y' order by common_name" 
RSSpeciesCodes1_cmd.Prepared = true

Set RSSpeciesCodes1 = RSSpeciesCodes1_cmd.Execute
RSSpeciesCodes1_numRows = 0
%>
<%
Dim RSSpeciesCodes3
Dim RSSpeciesCodes3_cmd
Dim RSSpeciesCodes3_numRows

Set RSSpeciesCodes3_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes3_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes3_cmd.CommandText = "SELECT mark_cat, description FROM calcom.dbo.market_categories where block_summary='Y' order by description" 
RSSpeciesCodes3_cmd.Prepared = true

Set RSSpeciesCodes3 = RSSpeciesCodes3_cmd.Execute
RSSpeciesCodes3_numRows = 0
%>
<%
Dim RSSpeciesCodes4
Dim RSSpeciesCodes4_cmd
Dim RSSpeciesCodes4_numRows

Set RSSpeciesCodes4_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes4_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes4_cmd.CommandText = "SELECT mark_cat, description FROM calcom.dbo.market_categories where trawl_block='Y' order by description" 
RSSpeciesCodes4_cmd.Prepared = true

Set RSSpeciesCodes4 = RSSpeciesCodes4_cmd.Execute
RSSpeciesCodes4_numRows = 0
%>

<%
Dim RSOTOSPECIES
Dim RSOTOSPECIES_cmd
Dim RSOTOSPECIES_numRows

Set RSOTOSPECIES_cmd = Server.CreateObject ("ADODB.Command")
RSOTOSPECIES_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSOTOSPECIES_cmd.CommandText = "select * from calcom.dbo.web_oto_sp_vu order by common_name" 
RSOTOSPECIES_cmd.Prepared = true

Set RSOTOSPECIES = RSOTOSPECIES_cmd.Execute
RSOTOSPECIES_numRows = 0
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Groundfish Data</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling data">
<meta name="description" content="This page is the commercial fishery data page for the California Cooperative Groundfish Survey (CCGS) and CALCOM." />

<style type="text/css">
<!--
body {
	background-color: #CCC;
	text-align: center;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	color: #333;
	font-weight: bold;
}
.style2 {
	color: #0000FF;
	font-weight: bold;
}
.style4 {
	font-size: 24px;
	font-weight: bold;
}
body,td,th {
	color: #000;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<p align="center" class="BasicHeading1">CALCOM Database Access Page</p>
<p align="left" class="BasicSubhead">From this page you can access much of our most commonly used data from the CALCOM database. Since it is not practical to make all datasets available from the website, more detailed queries can be done using direct connections to the database. Furthermore, any dataset subject to confidentiality rules (like port samples) are not available through the website. Individuals requiring more detailed access should refer to the DATA DESCRIPTION page in the documentation section and then contact us. Most of the data in this section is related to California Commercial Groundfish; however, commercial landings of all species (groundfish and non-groundfish) are included.</p>
<p align="left" class="BasicSubhead">Each set of data which is accesible from this website has a short description of what is included and can be downloaded into a comma delimited, ASCII file. </p>
<p>&nbsp;</p>

<p align="center" class="BasicSubhead style1"><a href="mailto:Don.pearson@noaa.gov">Contact the DBA</a></p>
<p>&nbsp;</p>


<p align="center" class="BasicSubhead style1"><a href="regulation_main.asp">FEDERAL COMMERCIAL REGULATIONS</a></p>
<p align="center" class="BasicSubhead style1"><a href="qry_all_landings.asp">COMMERCIAL LANDINGS</a></p>
<p align="center" class="BasicSubhead style1"><a href="qry_all_biological.asp">OTOLITHS, AGE, AND LENGTH EXPANSIONS</a></p>
<p align="center" class="BasicSubhead style1"><a href="documentation.asp">DOCUMENTATION PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="code_list_home.asp">CODE LISTS</a></p>
<p align="center" class="BasicSubhead style1"><a href="default.asp">RETURN TO HOME PAGE</a></p>
</body>
</html>
<%
RSSpeciesCodes.Close()
Set RSSpeciesCodes = Nothing
%>
<%
RSSpeciesCodes1.Close()
Set RSSpeciesCodes1 = Nothing
%>
<%
RSOTOSPECIES.Close()
Set RSOTOSPECIES = Nothing
%>
<%
RSSPECIEScodes3.Close()
Set RSSPECIEScodes3 = Nothing
%>
<%
RSSPECIEScodes4.Close()
Set RSSPECIEScodes4 = Nothing
%>