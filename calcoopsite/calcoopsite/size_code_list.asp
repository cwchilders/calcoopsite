<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RS_SIZE
Dim RS_SIZE_cmd
Dim RS_SIZE_numRows
dim filesys, txtfile
Set RS_SIZE_cmd = Server.CreateObject ("ADODB.Command")
RS_SIZE_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RS_SIZE_cmd.CommandText = "select * from calcom.dbo.size_codes" 
RS_SIZE_cmd.Prepared = true

Set RS_SIZE = RS_SIZE_cmd.Execute
RS_SIZE_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RS_SIZE_numRows = RS_SIZE_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Size Codes</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {
	font-size: 24px;
	color: #FF0000;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">Size Codes</p>
<p>&nbsp;</p>
<p class="style1">After clicking the download link, right click on the file and select SAVE AS...</p>

<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>
<p class="style1"><a href="/user_download/size_code.txt" class="style2">download the file</a></p>
<form id="form1" name="form1" method="post" action="">
  
  <table border="1">
    <tr>
      <td>Size Code</td>
      <td>description</td>
    </tr>
    <% set filesys = createobject("scripting.filesystemobject")
	   set txtfile= filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/size_code.txt")
	   txtfile.WriteLine("size_code,description")%>
	   
	   
     <%While ((Repeat1__numRows <> 0) AND (NOT RS_SIZE.EOF)) %>
      <tr>
        <td><%=(RS_SIZE.Fields.Item("size_code").Value)%></td>
        <td><%=(RS_SIZE.Fields.Item("description").Value)%></td>
      </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.WriteLine(RS_SIZE.Fields.Item("size_code").Value &","& RS_SIZE.Fields.Item("description").Value)
  RS_SIZE.MoveNext()
Wend
%>
  </table>
</form>

</body>
</html>
<%
RS_SIZE.Close()
Set RS_SIZE = Nothing
%>
