<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim MM_editAction
MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Server.HTMLEncode(Request.QueryString)
End If

' boolean to abort record edit
Dim MM_abortEdit
MM_abortEdit = false
%>
<%
' *** Delete Record: construct a sql delete statement and execute it

If (CStr(Request("MM_delete")) = "form2" And CStr(Request("MM_recordId")) <> "") Then

  If (Not MM_abortEdit) Then
    ' execute the delete
    Set MM_editCmd = Server.CreateObject ("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_CALCOM_DB_STRING
    MM_editCmd.CommandText = "DELETE FROM calcom.dbo.current_samples WHERE sample_no = ?"
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param1", 200, 1, 11, Request.Form("MM_recordId")) ' adVarChar
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    ' append the query string to the redirect URL
    Dim MM_editRedirectUrl
    MM_editRedirectUrl = "Edit_Home.asp"
    If (Request.QueryString <> "") Then
      If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0) Then
        MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
      Else
        MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
      End If
    End If
    Response.Redirect(MM_editRedirectUrl)
  End If

End If
%>
<%
Dim RSdelete__MMColParam
RSdelete__MMColParam = "1"
If (Request.Form("sample_no") <> "") Then 
  RSdelete__MMColParam = Request.Form("sample_no")
End If
%>
<%
Dim RSdelete
Dim RSdelete_cmd
Dim RSdelete_numRows

Set RSdelete_cmd = Server.CreateObject ("ADODB.Command")
RSdelete_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSdelete_cmd.CommandText = "SELECT * FROM calcom.dbo.current_samples WHERE sample_no = ?" 
RSdelete_cmd.Prepared = true
RSdelete_cmd.Parameters.Append RSdelete_cmd.CreateParameter("param1", 200, 1, 11, RSdelete__MMColParam) ' adVarChar

Set RSdelete = RSdelete_cmd.Execute
RSdelete_numRows = 0
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Delete</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>

<!--
 if request("sample_no")="" then %>
<form id="form1" name="form1" method="post" action="edit_delete.asp">
  <span class="CoolSubhead">Sample# to Edit</span>
  <input type="text" name="Sample_no" id="Sample_no" />
  <input type="submit" name="Submit_sample" id="Submit_sample" value="Get Sample#" />
</form>
else %>
-->
<form id="form2" name="form2" method="POST" action="<%=MM_editAction%>">
Sample#
<input name="sample_no" type="text" id="sample_no" value="<%=(RSdelete.Fields.Item("sample_no").Value)%>" />
Port
<input name="cal_port" type="text" id="cal_port" value="<%=(RSdelete.Fields.Item("cal_port").Value)%>" />
Landing Date
<input name="sample_date" type="text" id="sample_date" value="<%=(RSdelete.Fields.Item("sample_date").Value)%>" />
Mcat
<input name="mark_cat" type="text" id="mark_cat" value="<%=(RSdelete.Fields.Item("mark_cat").Value)%>" />
<input type="submit" name="submit" id="submit" value="Delete This Sample" />
<input type="hidden" name="MM_delete" value="form2" />
<input type="hidden" name="MM_recordId" value="<%= RSdelete.Fields.Item("sample_no").Value %>" />
</form>
<p>
  <!-- end if %>
-->
</p>
<form>
  <input type="button" value="Back To Previous Page"onclick="history.go(-1);return true;">
</form>
<p>&nbsp; </p>
</body>
</html>
<%
RSdelete.Close()
Set RSdelete = Nothing
%>
