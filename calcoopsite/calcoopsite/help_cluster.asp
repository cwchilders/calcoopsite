<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style2 {color: #FF0000}
-->
</style></head>

<body>
<div align="center" class="BasicHeading1">
  <p>Help For Cluster Entry Page</p>
  <p align="left" class="BasicSubhead"><span class="style2">Once submitted, this data cannot be edited. You must delete the sample</span>.</p>
  <p align="left" class="BasicSubhead">You cannot change the number of species, species weight, or number of clusters.</p>
  <p align="left" class="BasicSubhead">You must use valid species codes (characters not numeric market category codes).</p>
  <p align="left" class="BasicSubhead">All weights are in whole pounds and cannot exceed 999 lbs for the entire cluster.</p>
  <p align="left">&nbsp;</p>
  <p>&nbsp;</p>
</div>

<p>&nbsp;</p>
</body>
</html>
