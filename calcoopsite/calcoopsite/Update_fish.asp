<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim MM_editAction
MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Server.HTMLEncode(Request.QueryString)
End If

' boolean to abort record edit
Dim MM_abortEdit
MM_abortEdit = false
%>
<%
' IIf implementation
Function MM_IIf(condition, ifTrue, ifFalse)
  If condition = "" Then
    MM_IIf = ifFalse
  Else
    MM_IIf = ifTrue
  End If
End Function
%>
<%
If (CStr(Request("MM_update")) = "form1") Then
  If (Not MM_abortEdit) Then
    ' execute the update
    Dim MM_editCmd

    Set MM_editCmd = Server.CreateObject ("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_CALCOM_DB_STRING
    MM_editCmd.CommandText = "UPDATE calcom.dbo.current_fish SET sex = ?, maturity = ?, flength = ?, weight = ? WHERE sample_no = ? and clust_no= ? and fish_no=?"
    MM_editCmd.Prepared = true
	
	
	
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param1", 5, 1, -1, MM_IIF(Request.Form("sex"), Request.Form("sex"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param2", 5, 1, -1, Request.Form("maturity")) ' addouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param3", 5, 1, -1, MM_IIF(Request.Form("flength"), Request.Form("flength"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param4", 5, 1, -1, MM_IIF(Request.Form("weight"), Request.Form("weight"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param5", 200, 1, 9, Request.Form("MM_recordId")) ' adlongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param6", 5, 1, -1, MM_IIF(Request.Form("Clust_no"), request.form("clust_no"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param7", 5, 1, -1, MM_IIF(Request.Form("Fish_no"),request.form("fish_no"), null)) ' adDouble
    
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close

    ' append the query string to the redirect URL
    Dim MM_editRedirectUrl
    MM_editRedirectUrl = "Edit_Fish.asp"
	session("SampleNo")=Request.Form("MM_recordId")
    If (Request.QueryString <> "") Then
      If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0) Then
        MM_editRedirectUrl = MM_editRedirectUrl & "?" & request.QueryString
      Else
        MM_editRedirectUrl = MM_editRedirectUrl & "&" & request.QueryString
      End If
    End If
    Response.Redirect(MM_editRedirectUrl)
  End If
End If
%>
<%
Dim RSFISHED__MMColParam
Dim RSFISHED__MMColParam1
Dim RSFISHED__MMColParam2
RSFISHED__MMColParam = "1"
If (Request.QueryString("sampno") <> "") Then 
  RSFISHED__MMColParam = Request.querystring("sampno")
  RSFISHED__MMColParam1 = Request.querystring("Clustno")
  RSFISHED__MMColParam2 = Request.querystring("fishno")
End If
%>
<%
Dim RSFISHED
Dim RSFISHED_cmd
Dim RSFISHED_numRows

Set RSFISHED_cmd = Server.CreateObject ("ADODB.Command")
RSFISHED_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSFISHED_cmd.CommandText = "SELECT sample_no, clust_no, fish_no, species, sex, flength, maturity, weight FROM calcom.dbo.current_fish WHERE sample_no = ? and clust_no = ? and fish_no = ?" 
RSFISHED_cmd.Prepared = true
RSFISHED_cmd.Parameters.Append RSFISHED_cmd.CreateParameter("param1", 200, 1, 11, RSFISHED__MMColParam) ' adVarChar
RSFISHED_cmd.Parameters.Append RSFISHED_cmd.CreateParameter("param2", 5, 1, -1, RSFISHED__MMColParam1) ' adVarChar
RSFISHED_cmd.Parameters.Append RSFISHED_cmd.CreateParameter("param3", 5, 1, -1, RSFISHED__MMColParam2) ' adVarChar
Set RSFISHED = RSFISHED_cmd.Execute
RSFISHED_numRows = 0
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Fish Page</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">Edit Fish Record Page</p>
<p>&nbsp;</p>
<form id="form1" name="form1" method="POST" action="<%=MM_editAction%>">
  <label>
   <input name="sample_no" type="hidden" id="sample_no" value="<%=(RSFISHED.Fields.Item("sample_no").Value)%>"/>
  
  </label>
  <label>
  <input name="clust_no" type="hidden" id="clust_no" value="<%=(RSFISHED.Fields.Item("clust_no").Value)%>" />
  </label>
  <label>
  <input name="fish_no" type="hidden" id="fish_no" value="<%=(RSFISHED.Fields.Item("fish_no").Value)%>" />
  </label>
  <label>
   <input name="species" type="hidden" id="species" value="<%=(RSFISHED.Fields.Item("species").Value)%>" />
  </label>
  <table width="200" border="1" align="center">
    <tr>
      <td>Sample#</td>
      <td><label>
      <%=response.write(RSFISHED.FIELDS.ITEM("SAMPLE_NO").VALUE)%>
      </label>
      </td>
    </tr>
    <tr>
      <td>Cluster#</td>
      <td><label>
      <%=response.write(RSFISHED.FIELDS.ITEM("CLUST_NO").VALUE)%>
      </label>
      </td>
    </tr>
    <tr>
      <td>Fish#</td>
      <td><label>
      <%=response.write(RSFISHED.FIELDS.ITEM("FISH_NO").VALUE)%>
      </label>
      </td>
    </tr>
    <tr>
      <td>Species</td>
      <td><label>
      <%=response.write(RSFISHED.FIELDS.ITEM("SPECIES").VALUE)%>
      </label>
      </td>
    </tr>
    <tr>
      <td>Sex</td>
      <td><label>
        <input name="sex" type="text" id="sex" value="<%=(RSFISHED.Fields.Item("sex").Value)%>" />
      </label></td>
    </tr>
    <tr>
      <td>Maturity</td>
      <td><label>
        <input name="maturity" type="text" id="maturity" value="<%=(RSFISHED.Fields.Item("maturity").Value)%>" />
</label></td>
    </tr>
    <tr>
      <td>Flength</td>
      <td><label>
        <input name="flength" type="text" id="flength" value="<%=(RSFISHED.Fields.Item("flength").Value)%>" />
      </label></td>
    </tr>
    <tr>
      <td>Weight</td>
      <td><label>
        <input name="weight" type="text" id="weight" value="<%=(RSFISHED.Fields.Item("weight").Value)%>" />
      </label></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><label>
        <input type="submit" name="submit" id="submit" value="Update Record" />
      </label></td>
    </tr>
  </table>

  <input type="hidden" name="MM_update" value="form1" />
  <input type="hidden" name="MM_recordId" value="<%= RSFISHED.Fields.Item("sample_no").Value %>" />
</form>
<p>&nbsp;</p>
<p align="center" class="CoolSubhead"><a href="Edit_Home.asp">Exit Without Updating</a></p>
</body>
</html>
<%
RSFISHED.Close()
Set RSFISHED = Nothing
%>
