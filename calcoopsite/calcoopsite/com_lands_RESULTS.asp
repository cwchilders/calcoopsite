<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim rslands__SPCODE
rslands__SPCODE = "BCAC"
If (REQUEST.FORM("comlandSP") <> "") Then 
  rslands__SPCODE = REQUEST.FORM("comlandSP")
End If
%>
<%
Dim rslands
Dim rslands_cmd
Dim rslands_numRows
Dim filesys, txtfile

Set rslands_cmd = Server.CreateObject ("ADODB.Command")
rslands_cmd.ActiveConnection = MM_CALCOM_DB_STRING
rslands_cmd.CommandText = "SELECT YEAR, species, port_complex, gear_grp, live, source, sum(pounds) as lbs FROM calcom.dbo.Com_lands WHERE species=? group by year, species, port_complex, gear_grp, live, source order by year, port_complex, species" 
rslands_cmd.Prepared = true
rslands_cmd.Parameters.Append rslands_cmd.CreateParameter("param1", 200, 1, 255, rslands__SPCODE) ' adVarChar

Set rslands = rslands_cmd.Execute
rslands_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rslands_numRows = rslands_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Fishery Data </TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling, commercial landings">
<meta name="description" content="This page provides reconstructed commercial fishery landings data from the California Cooperative Groundfish Survey (CCGS) and CALCOM by species and port complex.

" />
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {font-weight: bold}
.style2 {font-size: 18px}
-->
</style>
</head>

<body>
<p align="center" class="BasicHeading1">Commercial Landings By Species</p>
<p class="style2"><strong>Note: the landings presented here represent the Best Available Data from 1969-current year. The data come from the com_lands table in CALCOM. NOTE: Catch Reconstruction data is no longer available from CALCOM</strong>.</p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>
<p class="style1"><a href="/user_download/com_lands_landings.txt" class="style2">download the file</a></p>
<p class="style2"><strong>Year, species, port_complex, gear_grp, live, source, pounds</strong></p>
<form id="form1" name="form1" method="post" action="">
  <table width="700" border="5">
  <% set filesys=createobject("scripting.filesystemobject")
     set txtfile=filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/com_lands_landings.txt")
	 txtfile.writeline("year, species, port_complex, gear_grp, live, source, pounds")
	 %>
    <% 
While ((Repeat1__numRows <> 0) AND (NOT rslands.EOF)) 
%>
      <tr>
        <td><%=(rslands.Fields.Item("YEAR").Value)%></td>
        <td><%=(rslands.Fields.Item("species").Value)%></td>
        <td><%=(rslands.Fields.Item("port_complex").Value)%></td>
        <td><%=(rslands.Fields.Item("gear_grp").Value)%></td>
         <td><%=(rslands.Fields.Item("live").Value)%></td>
        <td><%=(rslands.Fields.Item("source").Value)%></td>
        <td><%=(rslands.Fields.Item("lbs").Value)%></td>
      </tr>
      <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.writeline(rslands.Fields.Item("YEAR").Value &","& rslands.Fields.Item("species").Value &","& ","& rslands.Fields.Item("port_complex").Value & "," & rslands.Fields.Item("gear_grp").Value &","& rslands.Fields.Item("live").Value & "," & rslands.Fields.Item("source").Value &","& rslands.Fields.Item("lbs").Value)
  rslands.MoveNext()
Wend
%>

    </table>
</form>


<p>&nbsp;</p>


</body>
</html>
<%
rslands.Close()
Set rslands = Nothing
%>
