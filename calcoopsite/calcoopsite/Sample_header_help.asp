<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample_header_help_page</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style2 {color: #FF0000}
-->
</style>
</head>

<body class="BasicHeading1">
<div align="center">
  <p>SAMPLE HEADER PAGE HELP</p>
</div>
<p class="BasicSubhead">1. All fields in BOLD are required.</p>
<p class="BasicSubhead">2. Check Me Box is used to indicate that the Data Managers need to view something on this page: if you use this box, you MUST include something in the COMMENTS field.</p>
<p class="BasicSubhead">3. Sampler: this your assigned sampler name</p>
<p class="BasicSubhead">4. Port: select the port where the sample was taken, if not present, contact Brenda.</p>
<p class="BasicSubhead">5. Boat#: enter the vessel ID.</p>
<p class="BasicSubhead">6. Land Date: date the landing was made in the form MM/DD/YYYY where leading zeros are omitted.</p>
<p class="BasicSubhead">7. Gear: select the gear code, use UNK if not known or code is not present on the list.</p>
<p class="BasicSubhead">8. Market Category: enter the NUMERIC code for the market category - NOT A SPECIES CODE (which are characters).</p>
<p class="BasicSubhead">9. Landed Weight: enter a numeric value, leave blank if it is unknown. Do not use commas.</p>
<p class="BasicSubhead">10. RCPT#1, RCPT#2, RCPT#3: Landing receipt numbers (be sure to include the leading letter). If only one receipt, use RCPT#1.</p>
<p class="BasicSubhead">11. Otoliths: were otoliths collected?</p>
<p class="BasicSubhead">12. Live: were the fish landed as live?</p>
<p class="BasicSubhead">13. Observed: was there an observer on the trip?</p>
<p class="BasicSubhead">14. EFP: was this trip made under an Experimental Fishing Permit?</p>
<p class="BasicSubhead">15. IFQ: was thip trip made under an Individual Fishing Quota permit?</p>
<p class="BasicSubhead">16. Dressed: were the fish sampled when they were dressed? For skates, were they landed as wings?</p>
<p class="BasicSubhead">17. Size: OR (Ocean Run=unsorted by size); otherwise select the size as reported on the reciept. If you are not sure, use the Unknown code.</p>
<p class="BasicSubhead">18. Comments: indicate any brief comments you may have regarding the sample.</p>
<p class="BasicSubhead">19. CLUSTER INFORMATION: <span class="style2">once this data is entered and submitted, it cannot be changed and the sample will need to be deleted and reentered.</span> This data is used by the CLUSTER ENTRY PAGE.</p>
<p class="BasicSubhead">&nbsp;&nbsp;&nbsp;A. weight in whole pounds for the entire cluster</p>
<p class="BasicSubhead">&nbsp;&nbsp;&nbsp;B. count of the number of species (not individual fish) in the cluster</p>
<p class="BasicSubhead">&nbsp;</p>

<p class="CoolSubhead">&nbsp;</p>
</body>
</html>
