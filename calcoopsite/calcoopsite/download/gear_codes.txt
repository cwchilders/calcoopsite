By Gear code,,
calcom_code,description,gear_grp
BCG,BEACH GEAR (A-FRAME? Or THROW NET?) ,OTH
BMT,BEAM TRAWL,TWL
BTR,TROLL - (GROUNDFISH OR OTHER FISH),HKL
CLP,CRAB AND LOBSTER POT,FPT
CPT,CRAB TRAP - HOOP NET,FPT
DGN,GILL NET -  DRIFT,NET
DNT,DANISH/SCOTTISH SEINE,TWL
DPA,DIP NET- A FRAME,OTH
DPN,DIP NET - SCOOP OR BRAIL,OTH
DSG,DOUBLE RIGGED (PO SHRIMP),TWL
DVA,DIVING - ABALONE,OTH
DVG,SPEAR,OTH
DVK,DIVING,OTH
DVU,DIVING-URCHIN,OTH
ENT,ENTRAPPING,FPT
FPT,FISH TRAP,FPT
FTS,SELECTIVE TRAWL,GFS
GFL,TRAWL - FOOTROPE GREATER THAN 8 INCHES IN DIAMETER,TWL
GFS,TRAWL - FOOTROPE LESS THAN 8 INCHES IN DIAMETER,TWL
GFT,GROUNDFISH(OTTER) TRAWL,TWL
GLN,GILL NET - SET,NET
HKL,HOOK AND LINE,HKL
HKM,MOOCHING - SALMON,HKL
HKO,MISC LINE,HKL
HKT,TROLL-ALBACORE,HKL
HSL,HORIZNTAL SET LINE,HKL
JIG,JIG - ALBACORE,HKL
KLP,KELP BARGE,OTH
LGL,LONG LINE-SET,HKL
MC1,MISCELLANEOUS,UNK
MC2,TRAPS - SEATTLE TYPE (SABLE),FPT
MC3,HAND TAKE,OTH
MC4,RAFT HERRING ROE ON KELP,OTH
MDT,MIDWATER TRAWL,MDT
NET,NET-COULD BE ANY GEAR,UNK
NTL,LIFT NET,OTH
NTS,SHRIMP NET,OTH
ODG,DREDGE,OTH
ONT,CAST NET,OTH
OPT,FYKE NET,OTH
OTH,SPEAR/HARPOON,OTH
OTW,TRAWL NETS,TWL
PMH,HAND PUMP,OTH
PMP,FISH PUMP,OTH
PRT,PAIR TRAWL,TWL
PRW,PRAWN TRAP,FPT
PTL,PELAGIC TRAWL,TWL
PTW,PAIR TRAWL,TWL
RAK,RAKES,OTH
RLT,ROLLER TRAWL,TWL
SE1,ENCIRCLING NETS,OTH
SE2,LAMPARA,OTH
SEB,BEACH SEINE,OTH
SED,DRUM PURSE SEINE,OTH
SEH,HALF RING,OTH
SEL,LAMPARA - BAIT,OTH
SEP,PURSE SEINE,OTH
SST,SINGLE RIGGED (PO SHRIMP),TWL
STN,ENTANGLING NETS,NET
TLB,BALLOON TRAWL,TWL
TLG,TROLL LONGLINE,HKL
TLP,PARANZELLA,TWL
TML,TRAMMEL NET,NET
TRL,TROLL -B17 SALMON AND OTHERS,HKL
UN1,UNKNOWN,UNK
UN2,UNKNOWN,UNK
UNK,UNKNOWN,UNK
VHL,VERTICAL HOOK AND LINE (PORTUGUESE LONGLINE),HKL
VSL,VERTICAL SET LINE,HKL

By Description,,
calcom_code,description,gear_grp
TLB,BALLOON TRAWL,TWL
BCG,BEACH GEAR (A-FRAME? Or THROW NET?) ,OTH
SEB,BEACH SEINE,OTH
BMT,BEAM TRAWL,TWL
ONT,CAST NET,OTH
CLP,CRAB AND LOBSTER POT,FPT
CPT,CRAB TRAP - HOOP NET,FPT
DNT,DANISH/SCOTTISH SEINE,TWL
DPN,DIP NET - SCOOP OR BRAIL,OTH
DPA,DIP NET- A FRAME,OTH
DVK,DIVING,OTH
DVA,DIVING - ABALONE,OTH
DVU,DIVING-URCHIN,OTH
DSG,DOUBLE RIGGED (PO SHRIMP),TWL
ODG,DREDGE,OTH
SED,DRUM PURSE SEINE,OTH
SE1,ENCIRCLING NETS,OTH
STN,ENTANGLING NETS,NET
ENT,ENTRAPPING,FPT
PMP,FISH PUMP,OTH
FPT,FISH TRAP,FPT
OPT,FYKE NET,OTH
DGN,GILL NET -  DRIFT,NET
GLN,GILL NET - SET,NET
GFT,GROUNDFISH(OTTER) TRAWL,TWL
SEH,HALF RING,OTH
PMH,HAND PUMP,OTH
MC3,HAND TAKE,OTH
HKL,HOOK AND LINE,HKL
HSL,HORIZNTAL SET LINE,HKL
JIG,JIG - ALBACORE,HKL
KLP,KELP BARGE,OTH
SE2,LAMPARA,OTH
SEL,LAMPARA - BAIT,OTH
NTL,LIFT NET,OTH
LGL,LONG LINE-SET,HKL
MDT,MIDWATER TRAWL,MDT
HKO,MISC LINE,HKL
MC1,MISCELLANEOUS,UNK
HKM,MOOCHING- SALMON,HKL
NET,NET-COULD BE ANY GEAR,UNK
PRT,PAIR TRAWL,TWL
PTW,PAIR TRAWL,TWL
TLP,PARANZELLA,TWL
PTL,PELAGIC TRAWL,TWL
PRW,PRAWN TRAP,FPT
SEP,PURSE SEINE,OTH
MC4,RAFT HERRING ROE ON KELP,OTH
RAK,RAKES,OTH
RLT,ROLLER TRAWL,TWL
FTS,SELECTIVE TRAWL,GFS
NTS,SHRIMP NET,OTH
SST,SINGLE RIGGED (PO SHRIMP),TWL
DVG,SPEAR,OTH
OTH,SPEAR/HARPOON,OTH
TML,TRAMMEL NET,NET
MC2,TRAPS - SEATTLE TYPE (SABLE),FPT
GFL,TRAWL - FOOTROPE GREATER THAN 8 INCHES IN DIAMETER,TWL
GFS,TRAWL - FOOTROPE LESS THAN 8 INCHES IN DIAMETER,TWL
OTW,TRAWL NETS,TWL
BTR,TROLL - (GROUNDFISH OR OTHER FISH),HKL
TRL,TROLL -B17 SALMON AND OTHERS,HKL
TLG,TROLL LONGLINE,HKL
HKT,TROLL-ALBACORE,HKL
UN1,UNKNOWN,UNK
UN2,UNKNOWN,UNK
UNK,UNKNOWN,UNK
VHL,VERTICAL HOOK AND LINE (PORTUGUESE LONGLINE),HKL
VSL,VERTICAL SET LINE,HKL
