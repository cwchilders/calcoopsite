<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RS_MCAT
Dim RS_MCAT_cmd
Dim RS_MCAT_numRows
dim filesys, txtfile
Set RS_MCAT_cmd = Server.CreateObject ("ADODB.Command")
RS_MCAT_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RS_MCAT_cmd.CommandText = "select * from calcom.dbo.market_categories where mark_cat not in (248, 265) order by mark_cat" 
RS_MCAT_cmd.Prepared = true

Set RS_MCAT = RS_MCAT_cmd.Execute
RS_MCAT_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RS_MCAT_numRows = RS_MCAT_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Market Categories</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {
	font-size: 24px;
	color: #FF0000;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">Market Category Codes</p>
<p>&nbsp;</p>
<p class="style1">Note: Market categories should not be mistaken for species codes. A market category is a sort group used by fishermen and dealers.</p>
<p class="style1">After clicking the download link, right click on the file and select SAVE AS...Use the back button on your browser to return to the previous page</p>
<p>&nbsp;</p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>

<p>&nbsp;</p>
<p class="style1"><a href="user_download/mcat_code.txt" class="style2">download the file</a></p>
<form id="form1" name="form1" method="post" action="">
  
  <table border="1">
    <tr>
      <td>Market Category</td>
      <td>Description</td>
      <td>Nominal Species</td>
      <td>Species Group</td>
    </tr>
    <% set filesys = createobject("scripting.filesystemobject")
	   set txtfile= filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/mcat_code.txt")
	   txtfile.WriteLine("mark_cat, description, nominal_species, species_grp")%>
	   
	   
     <%While ((Repeat1__numRows <> 0) AND (NOT RS_MCAT.EOF)) %>
      <tr>
        <td><%=(RS_MCAT.Fields.Item("mark_cat").Value)%></td>
        <td><%=(RS_MCAT.Fields.Item("description").Value)%></td>
        <td><%=(RS_MCAT.Fields.Item("nominal_species").Value)%></td>
        <td><%=(RS_MCAT.Fields.Item("species_grp").Value)%></td>
      </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.WriteLine(RS_MCAT.Fields.Item("mark_cat").Value &","& RS_MCAT.Fields.Item("description").Value &","& RS_MCAT.Fields.Item("nominal_species").Value &","& RS_MCAT.Fields.Item("species_grp").Value)
  RS_MCAT.MoveNext()
Wend
%>
  </table>
</form>

</body>
</html>
<%
RS_MCAT.Close()
Set RS_MCAT = Nothing
%>
