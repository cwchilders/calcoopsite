<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim rslands__SPCODE
rslands__SPCODE = "BCAC"
If (REQUEST.FORM("species_or_group") <> "") Then 
  rslands__SPCODE = REQUEST.FORM("species_or_group")
End If
%>
<%
Dim rslands
Dim rslands_cmd
Dim rslandsparameter
Dim rslands_numRows
Dim filesys, txtfile

Set rslands_cmd = Server.CreateObject ("ADODB.Command")
set rslands = Server.createobject("ADODB.Recordset")

rslands_cmd.ActiveConnection = MM_CALCOM_DB_STRING

rslands_cmd.commandtext="select * from reg_oy_abc_ofl where species_or_group = ? order by year"
rslands_cmd.Prepared = true
rslands_cmd.Parameters.Append rslands_cmd.CreateParameter("@species", 200, 1, 255, rslands__SPCODE) ' adVarChar

Set rslands = rslands_cmd.Execute
rslands_numRows = 0
%>

<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rslands_numRows = rslands_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>Regulation By Species Only Results</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling, commercial landings, regulations">

<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	font-weight: bold;
	text-align: center;
}
.style2 {font-size: 18px}
.style4 a {
	font-weight: bold;
	font-size: 24px;
}
.basicheading1 a {
	font-size: 24px;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<p align="center" class="BasicHeading1">ABCs, OYs, OFLs, ACLs, and HG Results</p>

<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>

<p align="center" class="style4"><a href="qry_all_home.asp">Return to Data Retrieval Main Page</a></p> 

<p align="left" class="basicheading1"><a href="/user_download/reg_oy_abc_hg.txt">download the file</a></p>
<p class="style2"><strong>species_or_group, year, location_id, oy, ofl, abc_pre2011, abc_post2010, acl, hg, comments</strong></p>
<form id="form1" name="form1" method="post" action="">
  <table width="700" border="5">
<tr>
	<td>Species</td>
	<td>Year</td>
	<td>Location Id</td>
	<td>OY</td>
    <td>OFL</td>
	<td>ABC Pre2011</td>
	<td>ABC Post 2010</td>
	<td>ACL</td>
	<td>HG</td>
	<td>Comments</td>
</tr>

  <% set filesys=createobject("scripting.filesystemobject")
     set txtfile=filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/reg_oy_abc_hg.txt")
	 txtfile.writeline("species_or_group, year, location_id, oy, ofl, abc_pre2011, abc_post2010, acl, hg, comments")
	 %>
    <% 
While ((Repeat1__numRows <> 0) AND (NOT rslands.EOF)) 
%>
      <tr>
        <td><%=(rslands.Fields.Item("species_or_group").Value)%></td>
        <td><%=(rslands.Fields.Item("Year").Value)%></td>
        <td><%=(rslands.Fields.Item("Location_id").Value)%></td>
        <td><%=(rslands.Fields.Item("OY").Value)%></td>
        <td><%=(rslands.Fields.Item("OFL").Value)%></td>
        <td><%=(rslands.Fields.Item("abc_pre2011").Value)%></td>
        <td><%=(rslands.Fields.Item("abc_post2010").Value)%></td>
       
        <td><%=(rslands.Fields.Item("ACL").Value)%></td>
        <td><%=(rslands.Fields.Item("HG").Value)%></td>
        <td><%=(rslands.Fields.Item("Comments").Value)%></td>
      </tr>
      <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.writeline(rslands.Fields.Item("species_or_group").Value & " | " & rslands.Fields.Item("year").Value & " | " & rslands.Fields.Item("location_id").Value & " | " & rslands.Fields.Item("oy").Value & " | " &  rslands.Fields.Item("ofl").Value & " | " & rslands.Fields.Item("abc_pre2011").Value & " | " & rslands.Fields.Item("abc_post2010").Value & " | " & rslands.Fields.Item("acl").Value & " | " & rslands.Fields.Item("hg").Value & " | " & rslands.Fields.Item("comments").Value)
  rslands.MoveNext()
Wend
%>

    </table>
</form>


<p>&nbsp;</p>

</body>
</html>
<%
rslands.Close()
Set rslands = Nothing
%>
