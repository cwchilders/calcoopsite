<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>

<%
Dim RS_GEAR
Dim RS_GEAR_cmd
Dim RS_GEAR_numRows
dim filesys, txtfile
MM_CALCOM_DB_STRING = "dsn=calcom;uid=donp; pwd=Yy51456BOAT7;"
Set RS_GEAR_cmd = Server.CreateObject ("ADODB.Command")
RS_GEAR_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RS_GEAR_cmd.CommandText = "select * from calcom.dbo.gear_codes order by calcom_code" 
RS_GEAR_cmd.Prepared = true

Set RS_GEAR = RS_GEAR_cmd.Execute
RS_GEAR_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RS_GEAR_numRows = RS_GEAR_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Gear Codes</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {
	font-size: 24px;
	color: #FF0000;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">Gear Codes</p>
<p>&nbsp;</p>
<p class="style1">After clicking the download link, right click on the file and select SAVE AS...</p>
<p class="style1">&nbsp;</p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>
<p class="style1"><a href="user_download/gear_code.txt" class="style2">download the file</a></p>
<form id="form1" name="form1" method="post" action="">
  
  <table border="1">
    <tr>
      <td>CALCOM Code</td>
      <td>CDFG Code</td>
      <td>Description</td>
      <td>Gear Grp</td>
    </tr>
    <% set filesys = createobject("scripting.filesystemobject")
	   set txtfile= filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/gear_code.txt")
	   txtfile.WriteLine("calcom_code,cdfg_code,description,gear_grp")%>
	   
	   
     <%While ((Repeat1__numRows <> 0) AND (NOT RS_GEAR.EOF)) %>
      <tr>
        <td><%=(RS_GEAR.Fields.Item("calcom_code").Value)%></td>
        <td><%=(RS_GEAR.Fields.Item("CDFG_code").Value)%></td>
        <td><%=(RS_GEAR.Fields.Item("description").Value)%></td>
        <td><%=(RS_GEAR.Fields.Item("gear_grp").Value)%></td>
      </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.WriteLine(RS_GEAR.Fields.Item("calcom_code").Value &","& RS_GEAR.Fields.Item("cdfg_code").Value &","& RS_GEAR.Fields.Item("description").Value &","& RS_GEAR.Fields.Item("gear_grp").Value)
  RS_GEAR.MoveNext()
Wend
%>
  </table>
</form>

</body>
</html>
<%
RS_GEAR.Close()
Set RS_GEAR = Nothing
%>
