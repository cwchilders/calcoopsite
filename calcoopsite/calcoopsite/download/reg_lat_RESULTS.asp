<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim rslands__nlat
dim rslands_slat
rslands_nlat = "5000"
rslands_slat = "3100"
If (REQUEST.FORM("north_lat") <> "") Then 
  rslands__nlat = REQUEST.FORM("north_lat")
End If
If (REQUEST.FORM("south_lat") <> "") Then 
  rslands__slat = REQUEST.FORM("south_lat")
End If
%>
<%
Dim rslands
Dim rslands_cmd
Dim rslandsparameter
Dim rslands_numRows
Dim filesys, txtfile

Set rslands_cmd = Server.CreateObject ("ADODB.Command")
set rslands = Server.createobject("ADODB.Recordset")

rslands_cmd.ActiveConnection = MM_CALCOM_DB_STRING

rslands_cmd.commandtext="select a.regulation_date, b.location_id, a.regulation, b.states, b.inpfcs from reg_commercial_regulations a, reg_all_locations b where a.location_id=b.location_id and ? >= b.south_lat and ? <=b.north_lat order by a.regulation_date"
rslands_cmd.Prepared = true
rslands_cmd.Parameters.Append rslands_cmd.CreateParameter("@slat", 5, 1, -1, Request.Form("south_lat")) ' adDouble
rslands_cmd.Parameters.Append rslands_cmd.CreateParameter("@nlat", 5, 1, -1, Request.Form("north_lat")) ' adDouble

Set rslands = rslands_cmd.Execute
rslands_numRows = 0
%>

<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rslands_numRows = rslands_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>Regulations By Latitude</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling, commercial landings, regulations">

<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	font-weight: bold;
	text-align: center;
}
.style2 {font-size: 18px}
.style4 a {
	font-weight: bold;
	font-size: 24px;
}
.basicheading1 a {
	font-size: 24px;
	font-weight: bold;
}
-->
</style>
</head>

<body>
<p align="center" class="BasicHeading1">Regulations by Latitude Range</p>
<p class="style2"><strong>Note: the regulations returned here include all regulations partially or fully within a specified range of latitudes.</strong></p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>

<p align="center" class="style4"><a href="qry_all_home.asp">Return to Data Retrieval Main Page</a></p> 

<p align="left" class="basicheading1"><a href="/user_download/reg_latitudes.txt">download the file</a></p>
<p class="style2"><strong>regulation_date, location_id, states, inpfcs, regulation</strong></p>
<form id="form1" name="form1" method="post" action="">
  <table width="1249" border="5">
<tr>
	<td width="182">Regulation Date</td>
	<td width="131">Location Id</td>
    <td width="211">States</td>
    <td width="206">INPFCs</td>
	<td width="477">Regulation</td>
</tr>
  <% set filesys=createobject("scripting.filesystemobject")
     set txtfile=filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/reg_latitudes.txt")
     txtfile.writeline(date)
     txtfile.writeline("parameters: " & request.form("south_lat") & "  " & request.form("north_lat"))
 	 txtfile.writeline("regulation_date | location_id | regulation")
	 %>
    <% 
While ((Repeat1__numRows <> 0) AND (NOT rslands.EOF)) 
%>
      <tr>
        <td><%=(rslands.Fields.Item("regulation_date").Value)%></td>
        <td><%=(rslands.Fields.Item("location_id").Value)%></td>
        <td><%=(rslands.Fields.Item("states").Value)%></td>
        <td><%=(rslands.Fields.Item("inpfcs").Value)%></td>
        <td><%=(rslands.Fields.Item("regulation").Value)%></td>
      </tr>
      <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.writeline(rslands.Fields.Item("regulation_date").Value & " | " & rslands.Fields.Item("location_id").Value & " | " & rslands.Fields.Item("States").Value & " | " & rslands.Fields.Item("Inpfcs").Value & " | " & rslands.Fields.Item("regulation").Value)
  rslands.MoveNext()
Wend
%>

    </table>
</form>


<p>&nbsp;</p>

</body>
</html>
<%
rslands.Close()
Set rslands = Nothing
%>
