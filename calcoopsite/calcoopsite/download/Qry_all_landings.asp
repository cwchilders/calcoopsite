<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RSSpeciesCodes
Dim RSSpeciesCodes_cmd
Dim RSSpeciesCodes_numRows

Set RSSpeciesCodes_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes order by common_name" 
RSSpeciesCodes_cmd.Prepared = true

Set RSSpeciesCodes = RSSpeciesCodes_cmd.Execute
RSSpeciesCodes_numRows = 0
%>
<%
Dim RSSpeciesCodes0
Dim RSSpeciesCodes0_cmd
Dim RSSpeciesCodes0_numRows

Set RSSpeciesCodes0_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes0_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes0_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes where age_comp='Y' order by common_name" 
RSSpeciesCodes0_cmd.Prepared = true

Set RSSpeciesCodes0 = RSSpeciesCodes0_cmd.Execute
RSSpeciesCodes0_numRows = 0
%>
<%
Dim RSSpeciesCodes1
Dim RSSpeciesCodes1_cmd
Dim RSSpeciesCodes1_numRows

Set RSSpeciesCodes1_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes1_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes1_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes where len_comp='Y' order by common_name" 
RSSpeciesCodes1_cmd.Prepared = true

Set RSSpeciesCodes1 = RSSpeciesCodes1_cmd.Execute
RSSpeciesCodes1_numRows = 0
%>
<%
Dim RSSpeciesCodes3
Dim RSSpeciesCodes3_cmd
Dim RSSpeciesCodes3_numRows

Set RSSpeciesCodes3_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes3_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes3_cmd.CommandText = "SELECT mark_cat, description FROM calcom.dbo.market_categories where block_summary='Y' order by description" 
RSSpeciesCodes3_cmd.Prepared = true

Set RSSpeciesCodes3 = RSSpeciesCodes3_cmd.Execute
RSSpeciesCodes3_numRows = 0
%>
<%
Dim RSSpeciesCodes4
Dim RSSpeciesCodes4_cmd
Dim RSSpeciesCodes4_numRows

Set RSSpeciesCodes4_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes4_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes4_cmd.CommandText = "SELECT mark_cat, description FROM calcom.dbo.market_categories where trawl_block='Y' order by description" 
RSSpeciesCodes4_cmd.Prepared = true

Set RSSpeciesCodes4 = RSSpeciesCodes4_cmd.Execute
RSSpeciesCodes4_numRows = 0
%>

<%
Dim RSOTOSPECIES
Dim RSOTOSPECIES_cmd
Dim RSOTOSPECIES_numRows

Set RSOTOSPECIES_cmd = Server.CreateObject ("ADODB.Command")
RSOTOSPECIES_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSOTOSPECIES_cmd.CommandText = "select * from calcom.dbo.web_oto_sp_vu order by common_name" 
RSOTOSPECIES_cmd.Prepared = true

Set RSOTOSPECIES = RSOTOSPECIES_cmd.Execute
RSOTOSPECIES_numRows = 0
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Groundfish Data</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling data">
<meta name="description" content="This page is the commercial fishery data page for the California Cooperative Groundfish Survey (CCGS) and CALCOM." />

<style type="text/css">
<!--
body {
	background-color: #CCC;
	text-align: center;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
.style2 {
	color: #0000FF;
	font-weight: bold;
}
.style4 {
	font-size: 24px;
	font-weight: bold;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<p align="center" class="BasicHeading1">Retrieve Commercial Landings</p>
<p align="left" class="BasicSubhead">From this page you can commercial landings of both groundfish and non-groundfish.  Landings data reside in several database tables, each dataset with its own set of strengths and weaknesses.</p>
<p align="center" class="BasicSubhead style1"><a href="documentation.asp">DATABASE DOCUMENTATION PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="code_list_home.asp">CODE LISTS</a></p>

<p align="center" class="BasicSubhead style1"><a href="query_all_home.asp">RETURN TO DATA ACCESS PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="default.asp">RETURN TO HOME PAGE</a></p>
<p align="left" class="BasicSubhead">RECONSTRUCTED ANNUAL COMMERCIAL LANDINGS:  This dataset has annual commercial landings from 1916 to the current year for rockfish, and 1931-to the current year for all other species.  The information provided are a combination of Catch Reconstruction and expanded port sample data. The output file contains the following columns: year, gear group, region, species, source, and pounds. Gear type is limited to trawl versus non-trawl prior to 1969. It should be noted that since this is a combination of Catch Reconstruction and commercial port sample expansions, some columns present in the database have been dropped including:  market category, quarter, live (whether the species was landed as live fish), port_complex, and CDFG Block.  These columns are not included since they are not present in both datasets.  To access this information, direct database connections are required and multiple tables would need to be accessed.</p>
<form id="form1" name="form1" method="post" action="final_reconstruction_results.asp">
  <table width="700" border="5">
    <tr>
      <td width="507"><p class="CoolSubhead">Common Name</p></td>
      <td width="169"></td>
      </tr>
      <tr>
      <td><select name="COMLANDSP" id="COMLANDSP">
        <%
While (NOT RSSpeciesCodes.EOF)
%>
        <option value="<%=(RSSpeciesCodes.Fields.Item("species").Value)%>"><%=(RSSpeciesCodes.Fields.Item("common_name").Value)%></option>
        <%
  RSSpeciesCodes.MoveNext()
Wend
If (RSSpeciesCodes.CursorType > 0) Then
  RSSpeciesCodes.MoveFirst
Else
  RSSpeciesCodes.Requery
End If
%>
      </select></td>
      <td><input type="submit" name="fr1" id="fr1" value="Submit" /></td>
    </tr>
  </table>
</form>
<p class="CoolSubhead">&nbsp;</p>
<p align="left" class="BasicSubhead">BLOCK SUMMARY LANDINGS: This dataset contains California Department of Fish and Wildlife block summary information from 1931-1968. All gears are combined. The output file contains: year, assigned block, market category, region caught, and pounds. Two options are available for retrieval: by market category or by region. Retrievals by market category will provide annual summaries by block for the market category. Retrievals by region will have annual summaries for all species by block for the selected region. Charts showing block locations and regions can be found in the online documentation. Market category definitions can be found in the online code lists. To obtain monthly resolution of the block summary data, you must use a direct database connection. Catches made outside of California and landed in the state are not included. Note that market category is not a species code - it is a sort group and may contain significant quantities of species other than decribed by the market category definition!</p>
<p class="CoolSubhead">By Market Category</p>
<form id="form5" name="form5" method="post" action="block_summary_mcat_results.asp">
  <table width="700" border="5">
    <tr>
      <td width="508"><p class="CoolSubhead">Market Category</p></td>
      <td width="168"></td>
      </tr>
      <tr>
      <td><select name="blockSP" id="blockSP">
        <%
While (NOT RSSpeciesCodes3.EOF)
%>
        <option value="<%=(RSSpeciesCodes3.Fields.Item("mark_cat").Value)%>"><%=(RSSpeciesCodes3.Fields.Item("description").Value)%></option>
        <%
  RSSpeciesCodes3.MoveNext()
Wend
If (RSSpeciesCodes3.CursorType > 0) Then
  RSSpeciesCodes3.MoveFirst
Else
  RSSpeciesCodes3.Requery
End If
%>
      </select></td>
      <td><input type="submit" name="fr5" id="fr5" value="Submit" /></td>
    </tr>
  </table>
</form>
<p class="CoolSubhead">By Region</p>
<form id="form6" name="form6" method="post" action="block_summary_reg_results.asp">
  <table width="700" border="5">
    <tr>
    <td width="508"><p class="CoolSubhead">Region</p></td>
    <td width="168"></td>
    </tr>
    <tr>
      <td><select name="Region" id="Region">
        
        <option value="1">Region 1, Crescent City</option>
        <option value="2">Region 2, Eureka, Fort Bragg</option>
        <option value="3">Region 3, San Francisco Bay</option>
        <option value="4">Region 4, Bodega Bay, San Francisco</option>
        <option value="5">Region 5, Monterey</option>
        <option value="6">Region 6, Morro Bay, Santa Barbara</option>
        <option value="7">Region 7, Los Angeles</option>
        <option value="8">Region 8, San Diego</option>
      </select></td>
      <td><input type="submit" name="fr6" id="fr6" value="Submit" /></td>
    </tr>
  </table>
</form>

<p class="CoolSubhead">&nbsp;</p>
<p align="left" class="BasicSubhead">TRAWL BLOCK SUMMARY LANDINGS: This dataset contains California Department of Fish and Wildlife block summary information for trawl gear from 1925-2010. To obtain data for more recent years, you must access the CDFW trawl logs which are publicly available through PacFIN. The output file contains: year, assigned block, market category, region caught, area (Northern, Central, Southern, and Outside California) and pounds. Two options are available for retrieval: by market category or by Area. Retrievals by market category will provide annual summaries by block for the market category. Retrievals by Area will show all annual summaries for all species by block for the selected area. Charts showing block locations can be found in the online documentation. Market category definitions can be found in the online code lists. Northern California includes Crescent City, Eureka, and Fort Bragg. Central California includes Bodega Bay, San Francisco, Monterey, Morro Bay, and Santa Barbara. Southern California includes Los Angeles and San Diego. Outside California includes Oregon and Mexico. Monthly resolution of the trawl block summary information is not available online but is available via ODBC connections to the CALCOM database. Catches made outside of California and landed in the state are included. Note that market category is not a species code - it is a sort group and may contain significant quantities of species other than decribed by the market category definition!</p>

<p class="CoolSubhead">By Market Category</p>
<form id="form7" name="form7" method="post" action="TRAWL_block_MCAT_results.asp">
  <table width="700" border="5">
    <tr>
    <td width="507"><p class="CoolSubhead">Market Category</p></td>
    <td width="169"></td>
    </tr>
    <tr>
      <td><select name="blockSP" id="blockSP">
        <%
While (NOT RSSpeciesCodes4.EOF)
%>
        <option value="<%=(RSSpeciesCodes4.Fields.Item("mark_cat").Value)%>"><%=(RSSpeciesCodes4.Fields.Item("description").Value)%></option>
        <%
  RSSpeciesCodes4.MoveNext()
Wend
If (RSSpeciesCodes4.CursorType > 0) Then
  RSSpeciesCodes4.MoveFirst
Else
  RSSpeciesCodes4.Requery
End If
%>
      </select></td>
      <td><input type="submit" name="fr7" id="fr7" value="Submit" /></td>
    </tr>
  </table>
</form>
<p class="CoolSubhead">By Area</p>
<form id="form8" name="form8" method="post" action="trawl_block_area_results.asp">
  <table width="700" border="5">
    <tr>
    <td width="507"><p class="CoolSubhead">Area</p></td>
    <td width="169"></td>
    </tr>
    <tr>
      <td><select name="Area" id="Area">
        
        <option value="Northern California">Northern California</option>
        <option value="Central California">Central California</option>
        <option value="Southern California">Southern California</option>
        <option value="Outside California">Outside California</option>
      </select></td>
      <td><input type="submit" name="fr8" id="fr8" value="Submit" /></td>
    </tr>
  </table>
</form>
</body>
</html>
<%
RSSpeciesCodes.Close()
Set RSSpeciesCodes = Nothing
%>
<%
RSSpeciesCodes1.Close()
Set RSSpeciesCodes1 = Nothing
%>
<%
RSOTOSPECIES.Close()
Set RSOTOSPECIES = Nothing
%>
<%
RSSPECIEScodes3.Close()
Set RSSPECIEScodes3 = Nothing
%>
<%
RSSPECIEScodes4.Close()
Set RSSPECIEScodes4 = Nothing
%>