          Connecting To The CALCOM Database With ODBC

     The first step in connecting to CALCOM with ODBC is to obtain a user account and
password from the database administrator Don.Pearson@NOAA.gov.  At that time you may need
to sign a confidentiality agreement depending on what datasets you wish to access.
     ODBC drivers are typically installed by default on all Windows machines.  On Apple
computers, ODBC drivers must be purchased (about $40 per seat).  Drivers for other operating
systems are available and can be found on the internet.  The following describes how to make
connections from Windows.  Some of the information is relevant to other operating systems
including driver (SQL Server), Server Name (128.114.3.187), and protocol (TCP/IP).

From your computer, execute the following steps:
START->SETTINGS->CONTROL PANEL->ADMINISTRATIVE TOOLS->ODBC

Under the User DSN tab:
     Press ADD
     On the driver list scroll down to SQL Server and press FINISH
     Give it a name (any name will work, for example CALCOM)
     under Server,  type 128.114.3.187
     Press NEXT
     Choose "With SQL Server authentication" ...
     Enter the Login ID provided to you - not case sensitive
     Enter the Password provided to you - case sensitive
     Under CLIENT CONFIGURATION
          select TCP/IP
          unselect Dynamically Determine Port and enter 1433 for 
               the port number
          Press OKAY
     Press NEXT
     Press NEXT
     Press FINISH
     Press TEST DATA SOURCE
          It should respond Tests Completed Successfully
     Press OKAY
     Press OKAY
     Press OKAY - this closes it and you are ready to go

If you experience problems, let the DBA know.
