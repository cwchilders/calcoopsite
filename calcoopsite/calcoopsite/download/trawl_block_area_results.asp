<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim rslands__SPCODE
rslands__SPCODE = "Northern California"
If (REQUEST.FORM("Area") <> "") Then 
  rslands__SPCODE = REQUEST.FORM("Area")
End If
%>
<%
Dim rslands
Dim rslands_cmd
Dim rslands_numRows
Dim filesys, txtfile

Set rslands_cmd = Server.CreateObject ("ADODB.Command")
rslands_cmd.ActiveConnection = MM_CALCOM_DB_STRING
rslands_cmd.CommandText = "SELECT area,YEAR,mark_cat, species, block, sum(pounds) as pounds FROM calcom.dbo.CR_trawl_block_summary WHERE area=? group by area, year, mark_cat, species, block order by year, block" 
rslands_cmd.Prepared = true
rslands_cmd.Parameters.Append rslands_cmd.CreateParameter("param1", 200, 1, 255, rslands__SPCODE) ' adVarChar

Set rslands = rslands_cmd.Execute
rslands_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rslands_numRows = rslands_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Groundfish Data</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Commercial Fishery Landings">
<meta name="description" content="This page is the commercial trawl fishery data page for the California Cooperative Groundfish Survey (CCGS) and CALCOM." />

<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {font-weight: bold}
.style2 {font-size: 18px}
-->
</style>
</head>

<body>
<p align="center" class="BasicHeading1">Commercial Trawl Block Summary Landings By Region</p>
<p class="style2"><strong>Note: the market categories presented here do not neccesarily represent the actual species compositions for rockfish since port sample data have not been applied to these values. For example, the bocaccio landings are probably quite a bit larger than shown since they are often landed as unspecified rockfish.</strong></p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>
<p class="style1"><a href="/user_download/block_summary_sp.txt" class="style2">download the file</a></p>
<p class="style2"><strong>Area, Year, Market Category, species, CDFG block, pounds</strong></p>
<form id="form1" name="form1" method="post" action="">
  <table width="700" border="5">
  <% set filesys=createobject("scripting.filesystemobject")
     set txtfile=filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/block_summary_sp.txt")
	 txtfile.writeline("area, year, mark_cat, species, block, pounds")
	 %>
    <% 
While ((Repeat1__numRows <> 0) AND (NOT rslands.EOF)) 
%>
      <tr>
        <td><%=(rslands.Fields.Item("Area").Value)%></td>
        <td><%=(rslands.Fields.Item("YEAR").Value)%></td>
        <td><%=(rslands.Fields.Item("mark_cat").Value)%></td>
        <td><%=(rslands.Fields.Item("species").Value)%></td>
        <td><%=(rslands.Fields.Item("block").Value)%></td>
        <td><%=(rslands.Fields.Item("pounds").Value)%></td>
      </tr>
      <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.writeline(rslands.Fields.Item("Area").Value &","& rslands.Fields.Item("YEAR").Value &","& rslands.Fields.Item("mark_cat").Value &","& rslands.Fields.Item("species").Value &","& rslands.Fields.Item("block").Value &","& rslands.Fields.Item("pounds").Value)
  rslands.MoveNext()
Wend
%>

    </table>
</form>


<p>&nbsp;</p>

<p><a href="http://www.ncdc.noaa.gov/oa/climate/cdmp/cdmp.html"><img src="images/cdmp05-logobig.jpg" alt="CDMP" width="91" height="90" /></a></p>

<span class="CoolSubhead">Catch Reconstruction efforts were funded by NOAA's Climate Database Modernization Program</span>
</body>
</html>
<%
rslands.Close()
Set rslands = Nothing
%>
