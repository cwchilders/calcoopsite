<%@ Language=JavaScript%>

<!--#include file ="javascript/IIS_Gen_3.0_ConnectionPool.js"-->
<!--#include file ="javascript/IIS_Gen_3.0_Recordset.js"-->

<%
Response.Buffer = true
%>
<!--var ConnectionCache = new ConnectionCache()
/* server-side recordset */

SecurityLogin.Open();
SecurityLogin.ProcessAction();
SecurityLogin.SetMessages("","");
SecurityLogin_Server();
%>

<%
function SecurityLogin_Server() {
if (String(Request("hash_value")) != "undefined") {
  var filter_string = "UserName" + "= '" + String(Request("UserName")) + "' AND " + "Password" + " = '" + String(Request("hash_value")) + "'";
  var toforward = "PWID"

  SecurityLogin.Filter(filter_string);

  if (SecurityLogin.IsBOF() && SecurityLogin.IsEOF())
    {
    	  Response.Clear
  		  Response.Redirect(escape("default.asp"));
    }
  else  {
	   Session("SessionPassword")=String(SecurityLogin.GetColumnValue("UserGroup"))
    if (toforward!="")
      Session("PWID")=String(SecurityLogin.GetColumnValue("PWID"))
	  Session("port_complex")=String(SecurityLogin.GetColumnValue("port_complex"))
	  Response.Clear
	  Response.Redirect(escape("PortSamplerPage.asp"));
  }
}
}
%>-->

<SCRIPT LANGUAGE=JavaScript RUNAT=Server>
// Support Script (602)
// This function replaces all the spaces in the
// in string with %20.  For use with URL strings.
function URLEscapeSpaces(strIn)
{

	var a = " "
	var b = "%20"
	var c = strIn

	var i = c.indexOf(a);
	var l = b.length;

	while (i != -1)	{
		c = c.substring(0,i) + b + c.substring(i + a.length,c.length);
		i = c.indexOf(a,i);
	}
	return c;

}
</SCRIPT>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
<HEAD>
<meta name="google-site-verification" content="xnCugQMLwMEy9uvhYgKi0yNLX-FrrhojawxunIUl51M" />
<meta name="verify-v1" content="U8qyT1/o/4nka3M81gcbDjjkIMiilHUgqB5Hih64fG0=" >
<TITLE>CALCOM - California Commercial Fishery Data</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling">
<meta name="description" content="This page is the login page for the California Cooperative Groundfish Survey (CCGS) and CALCOM." />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<SCRIPT LANGUAGE="JavaScript">
<!--
var PasswordCookie = null;
var LoginForm = null;
var UserName = null;
var Password = null;
//-->
</SCRIPT>
<SCRIPT SRC="javascript/DBCommon.js"></SCRIPT>
<SCRIPT SRC="javascript/DBCookieObj.js"></SCRIPT>
<SCRIPT SRC="javascript/DBEdtBox.js"></SCRIPT>
<SCRIPT SRC="javascript/Client_Gen_3.0_Recordset.js"></SCRIPT>
<SCRIPT SRC="javascript/DBForm.js"></SCRIPT>
<SCRIPT SRC="javascript/default.js"></SCRIPT>
<SCRIPT SRC="javascript/DBRelPos.js"></SCRIPT>

<style type="text/css">
<!--
#regular_text {
	color: #000;
}
#log_guest {
	font-size: 18px;
	font-weight: bold;
}
#about {
	font-weight: bold;
}
.BasicSubhead {
	font-weight: bold;
}
#data {
	font-weight: bold;
}
#docs {
	font-weight: bold;
}
#faq {
	font-weight: bold;
}
.ln {
	font-weight: bold;
}
.BLD {
	font-weight: bold;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #CCC;
}
-->
</style>
<BODY>

<p>
<A NAME="StartOfPage"></A></p>

<FORM  NAME="LoginForm" METHOD="Post" ACTION="validate.asp" onClick=hash()>
  <TABLE WIDTH=100% BORDER=0 CELLPADDING=0 CELLSPACING=0>
    <TR>
     <td width=213>&nbsp;</td>
     <td width=1141><p align="center" style="font-family: Tahoma, Geneva, sans-serif; font-weight: bold; font-size: 36px;">California Cooperative Groundfish Survey</p></td>
    </TR>
    <tr>
    <TD align=center valign=top WIDTH=213>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>

      <p><a href="qry_all_home.asp" class="BLD">DATA ACCESS</a></p>
      <p><a href="DOCUMENTATION.asp" class="BLD">DOCUMENTATION</a></p>
      <p><a href="CONTACT_US.asp" class="BLD">CONTACT US</a></p>

      <p>&nbsp;</p>
      <p class="BasicSubhead">PORT SAMPLER LOGIN</p>
      <p class="BasicSubhead" align="center">Username<input type="text" id="DBStyleUserName" name="UserName" size="11" class="UserLoginFormText" tabindex="1" maxlength=100  value="">
      </p>
      <p class="BasicSubhead" align="center"><span class="BasicHeading1"><span class="BasicSubhead">Password</span></span>
        <input type="PASSWORD" id="DBStylePassword" name="Password" size="11" class="UserLoginFormText" tabIndex="2" maxlength=100  value=""></p>
        <p><input type="submit" value="Login" > </p>
      <p>&nbsp;</p>
      <p>&nbsp;</p></TD>
    <TD align=left valign=top width=1141>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
<p>&nbsp;&nbsp;<span style="font-family: Arial, Helvetica, sans-serif; font-style: italic; font-weight: bold; color: #000;">Welcome to the California Cooperative Groundfish Survey Web Site. From this site you can access California commercial groundfish market sample data including: landings, age and length compositions, description of the program, and other data. All data is stored in the CALCOM database which can also be accessed independently of this site using direct (ODBC) connections (refer to the documentation for more information). Questions about port sampling operations can be directed to <a href="mailto:berwin@psmfc.org">Brenda Erwin</a> (PSMFC). Questions about the website or database access can be directed to <a href="mailto:don.pearson@noaa.gov">Don Pearson</a> (NMFS). Port Samplers must login with their username and password.</a></span></p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p>&nbsp;</p>
      <p align=center><img src="images/PSMFC_LOGO.jpg" alt="psmfc_logo" width="150" height="150"><img src="images/noaalogo.jpg" width="150" height="150" alt="noaa_logo"><img src="images/cdfw-logo-146x193.JPG" width="119" height="151" alt="cdfg_logo"><img src="images/callogo.JPG" width="156" height="152" alt="ccgs_logo"></p></TD></tr>
    
     
  </TABLE>

  <INPUT TYPE="Hidden" NAME="SecurityLogin_Action">
  <input type="hidden" name="hash_value" value="0">

  <script src="javascript/md5.js"></script>
  <script type="text/javascript">
  function hash()
  {
  pass=document.LoginForm.Password.value
  test=hex_md5(document.LoginForm.Password.value)
  document.LoginForm.hash_value.value=test
  }
</script>
</FORM>


<SCRIPT SRC="javascript/defaultInline.js"></SCRIPT>

<DIV ID="EndOfPage"><A NAME="EndOfPage"></A></DIV>

</BODY></HTML>

