function CookieDef(name,expires,path,domain,secure) 
{
	this.name = name;
	this.secure = secure;
	this.path = path;
	this.domain = domain;
	this.getValue = getCookie;
	this.setValue = setCookie;
	this.expire = deleteCookie;

	if (expires == 0) {
		this.expires = "";
	} else {
		//make cookie expire number of days after browsing page
		var today_date = new Date();
		var expire_seconds = today_date.getTime() + (expires * 24 * 60 * 60  * 1000);
		today_date.setTime(expire_seconds);
		this.expires = today_date.toGMTString();
	}
}

function getCV(offset) {
	var endstr = document.cookie.indexOf(";", offset);
	if (endstr == -1) endstr = document.cookie.length;
	return unescape(document.cookie.substring(offset, endstr));
}

function getCookie(name) {
	var arg = name + "=";
	var alen = arg.length;
	var clen = document.cookie.length;
	var i = 0;
	while (i < clen) {
		var j = i + alen;
		if (document.cookie.substring(i, j) == arg)
			return getCV(j);
		i = document.cookie.indexOf(" ", i) + 1;
		if (i == 0) break;
	}
	return "";
}

function setCookie(name,value) 
{
	if (this.getValue(name)) this.expire();
	document.cookie = name + "=" + escape(value) +
		((this.expires == "") ? "" : ("; expires=" + this.expires)) +
		((this.path == "") ? "" : ("; path=" + this.path)) +
		((this.domain == "") ? "" : ("; domain=" + this.domain)) +
		((this.secure == true) ? "; secure" : "");
}

function deleteCookie(name) {
	document.cookie = name + "=" + "" + "; expires=Thu,01-Jan-70 00:00:01 GMT";
}

