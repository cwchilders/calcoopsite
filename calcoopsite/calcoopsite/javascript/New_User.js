// Support Script (409)
function PopulateWith(TheList, Items)
{
  var iCount = TheList.getCount();
  for (var i=0; i<iCount; i++)
    if (TheList.elementID)
      TheList.elementID.options[0] = null;
    else
      alert("bad or wrong DE for state population");

  i = 0;
  iCount = Items.length;
 	for (i=0; i<iCount; i++)
    TheList.addOption(Items[i], i, "");

  TheList.setSelectedByPosition(0);
}
// Support Script (554)
function AddToValidateArray(strElementName)
{
    var strName = strElementName

    if (!document.ValidateArray) 
    {
        document.ValidateArray = new Array
    }

    document.ValidateArray[document.ValidateArray.length] = strName
}

// Support Script (582)
function ValidateNonBlank()
{
  var msg = "";
  var val = this.getText();  

  if (StripChars(" \n\t\r",val).length == 0)
  {
    if (Trim(this.ErrorMsg) != "")
      msg = "Required field. " + this.ErrorMsg
    else
      msg = "Required field. Please enter an appropriate value."
  }

  return msg;
}

// Support Script (555)
function StripChars(theFilter,theString)
{
	var strOut,i,curChar

	strOut = ""
	for (i=0;i < theString.length; i++)
	{		
		curChar = theString.charAt(i)
		if (theFilter.indexOf(curChar) < 0)	// if it's not in the filter, send it thru
			strOut += curChar		
	}	
	return strOut
}

function AllInRange(x,y,theString)
{
	var i, curChar
	
	for (i=0; i < theString.length; i++)
	{
		curChar = theString.charAt(i)
		if (curChar < x || curChar > y) //the char is not in range
			return false
	}
	return true
}


function reformat (s)
{
    var arg;
    var sPos = 0;
    var resultString = "";

    for (var i = 1; i < reformat.arguments.length; i++) {
       arg = reformat.arguments[i];
       if (i % 2 == 1) 
           resultString += arg;
       else 
       {
           resultString += s.substring(sPos, sPos + arg);
           sPos += arg;
       }
    }
    return resultString;
}

function Trim(theString)
{
 var i,firstNonWhite

 if (StripChars(" \n\r\t",theString).length == 0 ) return ""

	i = -1
	while (1)
	{
		i++
		if (theString.charAt(i) != " ")
			break	
	}
	firstNonWhite = i
	//Count the spaces at the end
	i = theString.length
	while (1)
	{
		i--
		if (theString.charAt(i) != " ")
			break	
	}	

	return theString.substring(firstNonWhite,i + 1)

}
// Support Script (588)

function ValidateZipCode()
{
	var msg = ""
	var theString = this.getText()
 var msgInvalid = "Please enter a valid Zip Code.\nSuch as 92008 or 92008-1337"

	var theLen = StripChars(" \n\t\r",theString).length
	if (theLen == 0)	
		if (!this.Required) return ""		
		else return "Required field.  " + msgInvalid

	theString = StripChars("- \n\r",theString)		

	if (!AllInRange("0","9",theString))
	{
		msg = msgInvalid
	}
	else if (theString.length != 5 && theString.length != 9)
	{
		msg = msgInvalid
	}

	if (msg == "")
	{
		if (theString.length == 9)		
			this.setText(reformat(theString,"",5,"-",4))
		else
   this.setText(theString)
   
	}
	return msg	
}
// Support Script (576)
function ValidateEMail()
{
   var msg = "";
   var val = this.getText();
   var msgInvalid = "Please enter a valid e-mail address\n(a valid e-mail address contains the @ character)";

  	var theLen = StripChars(" ",val).length
	  if (theLen == 0)	
		  if (!this.Required) return ""		
		  else return "Required field.  " + msgInvalid

   if (val.indexOf("@",0) < 0) 
   {
      msg = msgInvalid 
   }
   return msg;
}

// Support Script (604)
function subAwithBinC(a,b,c)
{

	var i = c.indexOf(a);
	var l = b.length;

	while (i != -1)	{
		c = c.substring(0,i) + b + c.substring(i + a.length,c.length);
		i = c.indexOf(a,i);
	}
	return c;

}
// Support Script (589)
function Validate(stopOnFailure)
{
	var ErrorMsg = "";
	var i
	var msg
	var tofocus = true;
	var ErrorMsg = "";
	
	// Go through the Validate Array that may or may not exist
	// and call the Validate function for all elements that have one.
	if (document.ValidateArray)
	{
		for (i = 0; i < document.ValidateArray.length; i ++)
		{
			msg = eval( document.ValidateArray[i] + ".Validate()")
			if (msg != "")
			{
				ErrorMsg += "\n\n" + document.ValidateArray[i] + ":  " + msg;
				if (tofocus) 
				{
					eval(document.ValidateArray[i] + ".focus()")
					tofocus = false;
				}
				
				if (stopOnFailure == "1") return ErrorMsg;
			}
  	}
  }
	return ErrorMsg;
}

function document_onLoad() {
var i = 0;
s = new Array(51);
s[i++] = "--"
s[i++] = "AK"
s[i++] = "AL"
s[i++] = "AZ"
s[i++] = "AR"
s[i++] = "CA"
s[i++] = "CO"
s[i++] = "CT"
s[i++] = "DE"
s[i++] = "FL"
s[i++] = "GA"
s[i++] = "HI"
s[i++] = "ID"
s[i++] = "IL"
s[i++] = "IN"
s[i++] = "IA"
s[i++] = "KS"
s[i++] = "KY"
s[i++] = "LA"
s[i++] = "ME"
s[i++] = "MD"
s[i++] = "MA"
s[i++] = "MI"
s[i++] = "MN"
s[i++] = "MS"
s[i++] = "MO"
s[i++] = "MT"
s[i++] = "NE"
s[i++] = "NV"
s[i++] = "NH"
s[i++] = "NJ"
s[i++] = "NM"
s[i++] = "NY"
s[i++] = "NC"
s[i++] = "ND"
s[i++] = "OH"
s[i++] = "OK"
s[i++] = "OR"
s[i++] = "PA"
s[i++] = "RI"
s[i++] = "SC"
s[i++] = "SD"
s[i++] = "TN"
s[i++] = "TX"
s[i++] = "UT"
s[i++] = "VT"
s[i++] = "VA"
s[i++] = "WA"
s[i++] = "WV"
s[i++] = "WI"
s[i++] = "WY"

PopulateWith(State, s);
FullName.Validate=ValidateNonBlank;
FullName.ErrorMsg = "A name is required. Please enter a valid name."
AddToValidateArray("FullName")
Address.Validate=ValidateNonBlank;
Address.ErrorMsg = "An Address is a required field. Please enter an address."
AddToValidateArray("Address")
City.Validate=ValidateNonBlank;
City.ErrorMsg = "City is a required field. Please enter a City."
AddToValidateArray("City")
ZipCode.Validate = ValidateZipCode;
ZipCode.Required = Number("1");
AddToValidateArray("ZipCode")
Phone.Validate=ValidateNonBlank;
Phone.ErrorMsg = "Phone number is a required field. Please enter a phone number."
AddToValidateArray("Phone")
Email.Validate = ValidateEMail;
Email.Required = Number("1");
AddToValidateArray("Email")
 }
function SaveProfileButton_onClick() {
var addChar = "?" 
var j
var okToSubmit = false;

if ("".length > 1)
{
    NewProfile.setAction(subAwithBinC(" ", "%20", ""));
}

// execute the onSubmit() event handler and try to 
// determine if it already validated the form
Result = NewProfile.onSubmit();

//   If there is no onSubmithander the return value is null
//   If there is a validation handler it returns true to submit
//   or false to not submit
if (Result==null)  // there is no validation already defined
{
    if ("1" == "1")
    {
        Result = Validate("0"); // don't stop on first error
        if (Result == "") okToSubmit = true;
        else alert("The form could not be submitted:" + Result);
    }
    else okToSubmit = true;
}
else // there is a validation already defined
{
    if (Result==true)
        okToSubmit = true;
}

if (okToSubmit) 
{
    // We have to
    // put the source in the query string so the generic database contracts
    // still work.

    // NOTE: this only works if the method of the form is POST


    act = NewProfile.getAction();
    if (act.indexOf("?") != -1)
    {    
        addChar = "&"
    }

    act += addChar + "SaveProfileButton=1"
    NewProfile.setAction(act);


    NewProfile.submit();
}
 }
function _SaveProfileButton_onClick() { if (SaveProfileButton) return SaveProfileButton.onClick(); }
function SaveProfileButton_onMouseOver() {
window.status="Save Profile and define a Login Name and Password";
return true;
 }
function _SaveProfileButton_onMouseOver() { if (SaveProfileButton) return SaveProfileButton.onMouseOver(); }
function SaveProfileButton_onMouseOut() {
window.status="";
return true;
 }
function _SaveProfileButton_onMouseOut() { if (SaveProfileButton) return SaveProfileButton.onMouseOut(); }

