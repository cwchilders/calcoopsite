// Support Script (554)
function AddToValidateArray(strElementName)
{
    var strName = strElementName

    if (!document.ValidateArray) 
    {
        document.ValidateArray = new Array
    }

    document.ValidateArray[document.ValidateArray.length] = strName
}

// Support Script (582)
function ValidateNonBlank()
{
  var msg = "";
  var val = this.getText();  

  if (StripChars(" \n\t\r",val).length == 0)
  {
    if (Trim(this.ErrorMsg) != "")
      msg = "Required field. " + this.ErrorMsg
    else
      msg = "Required field. Please enter an appropriate value."
  }

  return msg;
}

// Support Script (555)
function StripChars(theFilter,theString)
{
	var strOut,i,curChar

	strOut = ""
	for (i=0;i < theString.length; i++)
	{		
		curChar = theString.charAt(i)
		if (theFilter.indexOf(curChar) < 0)	// if it's not in the filter, send it thru
			strOut += curChar		
	}	
	return strOut
}

function AllInRange(x,y,theString)
{
	var i, curChar
	
	for (i=0; i < theString.length; i++)
	{
		curChar = theString.charAt(i)
		if (curChar < x || curChar > y) //the char is not in range
			return false
	}
	return true
}


function reformat (s)
{
    var arg;
    var sPos = 0;
    var resultString = "";

    for (var i = 1; i < reformat.arguments.length; i++) {
       arg = reformat.arguments[i];
       if (i % 2 == 1) 
           resultString += arg;
       else 
       {
           resultString += s.substring(sPos, sPos + arg);
           sPos += arg;
       }
    }
    return resultString;
}

function Trim(theString)
{
 var i,firstNonWhite

 if (StripChars(" \n\r\t",theString).length == 0 ) return ""

	i = -1
	while (1)
	{
		i++
		if (theString.charAt(i) != " ")
			break	
	}
	firstNonWhite = i
	//Count the spaces at the end
	i = theString.length
	while (1)
	{
		i--
		if (theString.charAt(i) != " ")
			break	
	}	

	return theString.substring(firstNonWhite,i + 1)

}
// Support Script (580)
function ValidateInteger()
{
	var theString = this.getText()
	theString = StripChars(" ",theString);
	var min = this.intMin;
	var max = this.intMax;
	var pean = "Please enter a number.";
	
	if (theString.length == 0)	
	{
		if (!this.Required) return ""		
		else return "Required field.  " + pean;
	}

	// remove leading zeros (zeros are only leading if there is more than one char)
	while (theString.length > 1 && theString.substring(0,1) == "0")
	{
		theString = theString.substring(1, theString.length);
	}

	var val = parseInt(theString);
	if (isNaN(val)) return pean;
	
	// check for non-digits (and minus sign). Do this by converting number
	// back to a string and comparing it to original string.
	if (val.toString() != theString) return pean;
	
	if (min < max)
	{
		if ((val < min) || (val > max))
		{
			return "Please enter a number between " + min + " and " + max + ".";
		}
	}
	   
	// reset the entered string after removal of spaces and leading zeros.
	this.setText(theString);
	return "";
}

// Support Script (579)
function ValidateHugeInteger()
{
	var theString = this.getText()
	theString = StripChars(" ",theString);
	var minDigits = this.intMinDigits;
	var maxDigits = this.intMaxDigits;
	var pean = "Please enter a number.";
	
	if (theString.length == 0)	
	{
		if (!this.Required) return ""		
		else return "Required field.  " + pean;
	}

    negative = false;
    if (theString.charAt(0) == "-")
    {
        negative = true;
        theString = theString.substring(1,theString.length)
    }

    for (i=0; i<theString.length; i++)
    {
        if (theString.charAt(i) < "0" || theString.charAt(i) > "9")
            return pean;
    }

    if (theString.length < minDigits) return "You must enter at least " + minDigits + " digits.";
    if (theString.length > maxDigits) return "You must enter no more than " + maxDigits + " digits.";

    if (negative) theString = "-" + theString;
	   
	// reset the entered string after removal of spaces and leading zeros.
 // preserve original string for this contract
	//this.setText(theString);
	return "";
}

// Support Script (557)
function ValidateMask()
{
	var msg = ""
	var i, m, s, firstNonWhite
	var theString = this.getText()
	var theMask = this.Mask	
	var NumMask = this.NumMask
	var AlphaMask = this.AlphaMask
	var AlphaNumMask = this.AlphaNumMask
 var msgInvalid

 if (StripChars(" \n\t\r",this.ErrorMsg).length > 0) 
   msgInvalid = this.ErrorMsg
 else
   msgInvalid = "Please enter a value\nusing the format\n\n " + theMask + "\n\nwhere\n\n" + NumMask + "\tis a digit\n" + AlphaMask + "\tis a letter and\n" + AlphaNumMask + "\tis a letter or digit"
	
	if (StripChars(" ",theString).length == 0)	
		if (!this.Required) return ""		
		else return "Required field.  " + msgInvalid

	//Strip spaces off of the sides of the string
 theString = Trim(theString)

	if (theString.length != theMask.length)
		return msgInvalid


	blnOk = true
	for (i = 0; i < theMask.length; i++)
	{
		m = theMask.charAt(i)
		s = theString.charAt(i)
		if (m == NumMask)
  {
				if (!AllInRange("0","9",s))	blnOk = false
  }
  else if (m == AlphaMask)
  {
				if (!AllInRange("A","Z",s.toUpperCase())) blnOk = false
  }
		else if (m == AlphaNumMask)
  {
				if (!AllInRange("0","9",s) && !AllInRange("A","Z",s.toUpperCase()))	blnOk = false
  }
		else //It should be literal
				if (m != s)	blnOk = false		
	
		if (!blnOk) break; // exit for loop because the string is already wrong
	}

	if (!blnOk) return msgInvalid

	this.setText(theString)

	return msg		
}

// Support Script (603)
function getForm()
{
	var f;
	if (document.forms && document.forms[0])
	{
		// This is the case where there are only hidden form elements in an
		// all 4.0 browser page and Comm is hitting the page - and all IE cases.
		f = document.forms[0];
	}

	if (f == null)
	{
		if ((document.layers) && (document.layers.length > 0))
		{
			var d;
			iLayer = 0;
			while (true)
			{
				d = document.layers[iLayer].document.layers[0].document;
				if (d.forms && d.forms[0]) // normal case
				{
					f = d.forms[0];
					break;
				}
				iLayer = iLayer + 2;
			
				if (document.layers[iLayer] == null)
					break;
			}
		}
	}

	if (f == null)
		alert("Form not submitted - internal error: Drumbeat can not find Form object in DOM. There may not be a form on this page.");

	return f;
}

// Support Script (604)
function subAwithBinC(a,b,c)
{

	var i = c.indexOf(a);
	var l = b.length;

	while (i != -1)	{
		c = c.substring(0,i) + b + c.substring(i + a.length,c.length);
		i = c.indexOf(a,i);
	}
	return c;

}
// Support Script (589)
function Validate(stopOnFailure)
{
	var ErrorMsg = "";
	var i
	var msg
	var tofocus = true;
	var ErrorMsg = "";
	
	// Go through the Validate Array that may or may not exist
	// and call the Validate function for all elements that have one.
	if (document.ValidateArray)
	{
		for (i = 0; i < document.ValidateArray.length; i ++)
		{
			msg = eval( document.ValidateArray[i] + ".Validate()")
			if (msg != "")
			{
				ErrorMsg += "\n\n" + document.ValidateArray[i] + ":  " + msg;
				if (tofocus) 
				{
					eval(document.ValidateArray[i] + ".focus()")
					tofocus = false;
				}
				
				if (stopOnFailure == "1") return ErrorMsg;
			}
  	}
  }
	return ErrorMsg;
}

function document_onLoad() {
edtSample_no.Validate=ValidateNonBlank;
edtSample_no.ErrorMsg = "This is required"
AddToValidateArray("edtSample_no")
Recordset38_mark_cat.Validate = ValidateInteger;
Recordset38_mark_cat.intMin = parseInt("0");
Recordset38_mark_cat.intMax = parseInt("999");
Recordset38_mark_cat.Required = Number("0");
AddToValidateArray("Recordset38_mark_cat")
port_complex_out.setText(port_complex_in.getText())
Recordset38_total_wgt.Validate = ValidateHugeInteger;
Recordset38_total_wgt.intMinDigits = parseInt("0");
Recordset38_total_wgt.intMaxDigits = parseInt("7");
Recordset38_total_wgt.Required = Number("0");
AddToValidateArray("Recordset38_total_wgt")
Recordset38_block.Validate = ValidateInteger;
Recordset38_block.intMin = parseInt("0");
Recordset38_block.intMax = parseInt("1500");
Recordset38_block.Required = Number("0");
AddToValidateArray("Recordset38_block")
Recordset38_Number_sets.Validate = ValidateInteger;
Recordset38_Number_sets.intMin = parseInt("0");
Recordset38_Number_sets.intMax = parseInt("50");
Recordset38_Number_sets.Required = Number("0");
AddToValidateArray("Recordset38_Number_sets")
Edit2.setText(Edit1.getText())
Recordset38_hooks.Validate = ValidateInteger;
Recordset38_hooks.intMin = parseInt("0");
Recordset38_hooks.intMax = parseInt("9999");
Recordset38_hooks.Required = Number("0");
AddToValidateArray("Recordset38_hooks")
Recordset38_sampler.Validate=ValidateNonBlank;
Recordset38_sampler.ErrorMsg = "required field"
AddToValidateArray("Recordset38_sampler")
Recordset38_sample_date.ErrorMsg = "";
Recordset38_sample_date.Mask = "##/##/####";
Recordset38_sample_date.NumMask = "#";
Recordset38_sample_date.AlphaMask = "A";
Recordset38_sample_date.AlphaNumMask = "?";
Recordset38_sample_date.Required = Number("1");
Recordset38_sample_date.Validate = ValidateMask;
AddToValidateArray("Recordset38_sample_date")
txtSpeciesCountC1.Validate=ValidateNonBlank;
txtSpeciesCountC1.ErrorMsg = "You must fill in a Count of Species for Cluster #1"
AddToValidateArray("txtSpeciesCountC1")
txtSpeciesWeightC1.Validate=ValidateNonBlank;
txtSpeciesWeightC1.ErrorMsg = "You must fill in a Total Weight for Cluster #1."
AddToValidateArray("txtSpeciesWeightC1")
 }
function cmdEnterCluster__onClick() {
// We do not call onSubmit() with this contract

var formObj = getForm()
if (formObj != null)
{
var addChar = "?" 
var j
var Result
var okToSubmit = false

if ("".length > 1)
{
    formObj.action = subAwithBinC(" ", "%20", "")
}

// We have to
// put the source in the query string so the generic database contracts
// still work.

// NOTE: this only works if the method of the form is POST

act = formObj.action
if (act.indexOf("?") != -1)
{    
    addChar = "&"
}

act += addChar + "cmdEnterCluster=1"
formObj.action = act


if ("1" == "1")
{
    Result = Validate("0"); // don't stop on first error
    if (Result == "") okToSubmit = true;
    else alert("The form could not be submitted:" + Result);
}
else 
{
    okToSubmit = true
}


if (okToSubmit) formObj.submit();
}
 }
function _cmdEnterCluster__onClick() { if (cmdEnterCluster) return cmdEnterCluster.onClick(); }
function FormButton1__onClick() {
if ("".length > 0)
    document.location.href="";
else
    document.location.href="code_list.asp";
 }
function _FormButton1__onClick() { if (FormButton1) return FormButton1.onClick(); }

