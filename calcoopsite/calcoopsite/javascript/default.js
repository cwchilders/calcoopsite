// Support Script (604)
function subAwithBinC(a,b,c)
{

	var i = c.indexOf(a);
	var l = b.length;

	while (i != -1)	{
		c = c.substring(0,i) + b + c.substring(i + a.length,c.length);
		i = c.indexOf(a,i);
	}
	return c;

}
// Support Script (589)
function Validate(stopOnFailure)
{
	var ErrorMsg = "";
	var i
	var msg
	var tofocus = true;
	var ErrorMsg = "";
	
	// Go through the Validate Array that may or may not exist
	// and call the Validate function for all elements that have one.
	if (document.ValidateArray)
	{
		for (i = 0; i < document.ValidateArray.length; i ++)
		{
			msg = eval( document.ValidateArray[i] + ".Validate()")
			if (msg != "")
			{
				ErrorMsg += "\n\n" + document.ValidateArray[i] + ":  " + msg;
				if (tofocus) 
				{
					eval(document.ValidateArray[i] + ".focus()")
					tofocus = false;
				}
				
				if (stopOnFailure == "1") return ErrorMsg;
			}
  	}
  }
	return ErrorMsg;
}

// Support Script (554)
function AddToValidateArray(strElementName)
{
    var strName = strElementName

    if (!document.ValidateArray) 
    {
        document.ValidateArray = new Array
    }

    document.ValidateArray[document.ValidateArray.length] = strName
}

// Support Script (582)
function ValidateNonBlank()
{
  var msg = "";
  var val = this.getText();  

  if (StripChars(" \n\t\r",val).length == 0)
  {
    if (Trim(this.ErrorMsg) != "")
      msg = "Required field. " + this.ErrorMsg
    else
      msg = "Required field. Please enter an appropriate value."
  }

  return msg;
}

// Support Script (555)
function StripChars(theFilter,theString)
{
	var strOut,i,curChar

	strOut = ""
	for (i=0;i < theString.length; i++)
	{		
		curChar = theString.charAt(i)
		if (theFilter.indexOf(curChar) < 0)	// if it's not in the filter, send it thru
			strOut += curChar		
	}	
	return strOut
}

function AllInRange(x,y,theString)
{
	var i, curChar
	
	for (i=0; i < theString.length; i++)
	{
		curChar = theString.charAt(i)
		if (curChar < x || curChar > y) //the char is not in range
			return false
	}
	return true
}


function reformat (s)
{
    var arg;
    var sPos = 0;
    var resultString = "";

    for (var i = 1; i < reformat.arguments.length; i++) {
       arg = reformat.arguments[i];
       if (i % 2 == 1) 
           resultString += arg;
       else 
       {
           resultString += s.substring(sPos, sPos + arg);
           sPos += arg;
       }
    }
    return resultString;
}

function Trim(theString)
{
 var i,firstNonWhite

 if (StripChars(" \n\r\t",theString).length == 0 ) return ""

	i = -1
	while (1)
	{
		i++
		if (theString.charAt(i) != " ")
			break	
	}
	firstNonWhite = i
	//Count the spaces at the end
	i = theString.length
	while (1)
	{
		i--
		if (theString.charAt(i) != " ")
			break	
	}	

	return theString.substring(firstNonWhite,i + 1)

}
function document_onLoad() {
/*if (String(PasswordCookie.getValue("UserName"))!="null")
  UserName.setText(PasswordCookie.getValue("UserName"));
if (String(PasswordCookie.getValue("Password"))!="null")
  Password.setText(PasswordCookie.getValue("Password"));*/
Password.Validate=ValidateNonBlank;
Password.ErrorMsg = "You must enter a password."
AddToValidateArray("Password")
UserName.Validate=ValidateNonBlank;
UserName.ErrorMsg = "You must enter a user name."
AddToValidateArray("UserName")
 }


