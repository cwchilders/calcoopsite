<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RSSpeciesCodes
Dim RSSpeciesCodes_cmd
Dim RSSpeciesCodes_numRows

Set RSSpeciesCodes_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes_cmd.CommandText = "SELECT * FROM calcom.dbo.reg_species_groups order by common_name" 
RSSpeciesCodes_cmd.Prepared = true

Set RSSpeciesCodes = RSSpeciesCodes_cmd.Execute
RSSpeciesCodes_numRows = 0
%>

<%
Dim RSSpeciesCodes1
Dim RSSpeciesCodes1_cmd
Dim RSSpeciesCodes1_numRows

Set RSSpeciesCodes1_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes1_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes1_cmd.CommandText = "SELECT distinct species_or_group FROM calcom.dbo.reg_oy_abc_ofl order by species_or_group" 
RSSpeciesCodes1_cmd.Prepared = true

Set RSSpeciesCodes1 = RSSpeciesCodes1_cmd.Execute
RSSpeciesCodes1_numRows = 0
%>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Commercial Regulations Data Retrieval Page</title>

<style type="text/css">
<!--
body {
	background-color: #00FFFF;
	text-align: center;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
.style2 {
	color: #0000FF;
    font-size:18px
	font-weight: bold;
}
.style4 {
	font-size: 24px;
	font-weight: bold;
}
-->
</style>

</head>

<body bgcolor="#00FFFF">

<p align="center" class="BasicHeading1">Commercial Regulations Data Retrieval Page</p>
<p align="center" class="BasicHeading1">&nbsp;</p>

<p align="center" class="BasicHeading1">Regulations By Species and Various Parameters</p>
<p align="left" class="CoolSubhead">This routine currently does not work - hopefully in the near future it will.</p>
<form id="form9" name="form9" method="post" action="reg_multi_param_results.asp">
  <table width="700" border="5">
    <tr>
      <td width="150">Common Name</td>
 
      <td><select name="common_name_qry" id="common_name_qry">
        <%
While (NOT RSSpeciesCodes.EOF)
%>
       <option value="<%=(RSSpeciesCodes.Fields.Item("common_name_qry").Value)%>"><%=(RSSpeciesCodes.Fields.Item("common_name").Value)%></option>
       
        <%
  RSSpeciesCodes.MoveNext()
Wend
If (RSSpeciesCodes.CursorType > 0) Then
  RSSpeciesCodes.MoveFirst
Else
  RSSpeciesCodes.Requery
End If
%>
      </select></td>
</tr>

   <tr>
<td>South Latitude as ddmm.mm:</td>
      <td><input name="south_lat" type="integer" id="south_lat" maxlength="15" /></td>
</tr>
<tr>
      <td>North Latitude as ddmm.mm:</td>
      <td><input name="north_lat" type="integer" id="north_lat" maxlength="15" /></td>
</tr>

<td>Start Year (minimum = 1983):</td>
      <td><input name="Start_year" type="integer" id="start_year" maxlength="15" /></td>
</tr>
<tr>
      <td>End Year (maximum = 2014):</td>
      <td><input name="end_year" type="integer" id="end_year" maxlength="15" /></td>
</tr>

<tr>
      <td>Sector:</td>
      <td><select name="Sect" id="Sect">
        <option value="Open and Access">OPEN ACCESS</option>
        <option value="Limited and Entry">LIMITED ENTRY</option>
      </select></td>
</tr>


<tr>

      <td><input type="submit" name="fr9" id="fr9" value="Submit" /></td>
    </tr>
  </table>
</form>




<p align="center" class="BasicHeading1">Regulations By Species and Management Group</p>
<p align="left" class="CoolSubhead">This dataset consists of species specific regulations and any regulations for complexes they may be a part of, for example: chilipepper are managed for some years as part of Sebastes Complex. This dataset WILL include Sebastes Complex regulations where relevant. As a result, not all regulations returned will have the name of the species included in the search. Note:  regulations will be sorted by effective date.</p>
<form id="form2" name="form2" method="post" action="reg_species_and_grp_results.asp">
  <table width="700" border="5">
    <tr>
      <td width="150">Common Name</td>
 
      <td><select name="common_name_qry" id="common_name_qry">
        <%
While (NOT RSSpeciesCodes.EOF)
%>
       <option value="<%=(RSSpeciesCodes.Fields.Item("common_name_qry").Value)%>"><%=(RSSpeciesCodes.Fields.Item("common_name").Value)%></option>
       
        <%
  RSSpeciesCodes.MoveNext()
Wend
If (RSSpeciesCodes.CursorType > 0) Then
  RSSpeciesCodes.MoveFirst
Else
  RSSpeciesCodes.Requery
End If
%>
      </select></td>
</tr>

<tr>

      <td><input type="submit" name="fr2" id="fr2" value="Submit" /></td>
    </tr>
  </table>
</form>

<p align="center" class="BasicHeading1">Regulations By Latitude For All Species</p>
<p align="left" class="CoolSubhead">This dataset consists of regulations for ALL species within a specific geographic range. More complex queries can be performed via ODBC connections to return INPFC areas and states. Note: both south and north latitude must be specified. If the U.S./Canada Border is desired - enter 5000 or greater for North latitude, if U.S./Mexico Border is desired - enter 3100or less for South latitude.</p>
<form id="form3" name="form3" method="post" action="reg_lat_results.asp">
  <table width="700" border="5">
    <tr>
<td>South Latitude as ddmm.mm:</td>
      <td><input name="south_lat" type="text" id="south_lat" maxlength="15" /></td>
</tr>
<tr>
      <td>North Latitude as ddmm.mm:</td>
      <td><input name="north_lat" type="text" id="north_lat" maxlength="15" /></td>
</tr>
 <tr>
      <td><input type="submit" name="fr3" id="fr3" value="Submit" /></td>
    </tr>
  </table>
</form>


<p align="center" class="BasicHeading1">Regulations By Annual Interval For All Species</p>
<p align="left" class="CoolSubhead">This dataset consists of regulations for ALL species within a specific yearly range. More complex queries can be performed via ODBC connections to return INPFC areas and states. Note: both south and north latitude must be specified. If the U.S./Canada Border is desired - enter 5000 or greater for North latitude, if U.S./Mexico Border is desired - enter 3100or less for South latitude.</p>
<form id="form8" name="form8" method="post" action="reg_annual_results.asp">
  <table width="700" border="5">
    <tr>
<td>Start Year (minimum = 1983):</td>
      <td><input name="start_year" type="text" id="start_year" maxlength="15" /></td>
</tr>
<tr>
      <td>End Year (maximum = 2014):</td>
      <td><input name="end_year" type="text" id="end_year" maxlength="15" /></td>
</tr>
 <tr>
      <td><input type="submit" name="fr8" id="fr8" value="Submit" /></td>
    </tr>
  </table>
</form>







<p align="center" class="BasicHeading1">Regulations By Sector (Open Access or Limited Entry)</p>
<p align="left" class="CoolSubhead">This dataset consists of all regulations for all species for either Open Access or Limited Entry fisheries.</p>
<form id="form4" name="form4" method="post" action="reg_sector_results.asp">
  <table width="700" border="5">
    <tr>
      <td>Sector:</td>
      <td><select name="Sector" id="Sector">
        <option value="Open and Access">OPEN ACCESS</option>
        <option value="Limited and Entry">LIMITED ENTRY</option>
      </select></td>
 <tr>
      <td><input type="submit" name="fr4" id="fr4" value="Submit" /></td>
    </tr>
  </table>
</form>

<p align="center" class="BasicHeading1">OYs, ABCs, ACLs, and HGs by Species</p>
<p align="left" class="CoolSubhead">This dataset will return ABCs, OYs, OFLs, ACLs, and HGs for a specified species. The user is responsible for examining the documentation to determine the meaning of these values.</p>
<form id="form5" name="form5" method="post" action="reg_oy_abc_hg_results.asp">

  <table width="700" border="5">
    <tr>
      <td width="150"><p class="basicheading1">Common Name</p></td>
      <td><select name="species_or_group" id="species_or_group">
        <%
While (NOT RSSpeciesCodes1.EOF)
%>
        <option value="<%=(RSSpeciesCodes1.Fields.Item("species_or_group").Value)%>"><%=(RSSpeciesCodes1.Fields.Item("species_or_group").Value)%></option>
        <%
  RSSpeciesCodes1.MoveNext()
Wend
If (RSSpeciesCodes1.CursorType > 0) Then
  RSSpeciesCodes1.MoveFirst
Else
  RSSpeciesCodes1.Requery
End If
%>
      </select></td>
</tr>
<tr>
      <td><input type="submit" name="fr5" id="fr5" value="Submit" /></td>
    </tr>
  </table>
</form>

<p align="center" class="BasicHeading1">Rockfish Conservation Areas</p>
<p align="left" class="CoolSubhead">This dataset consists the general boundaries and timing of Rockfish Conservation Areas by Trawl or Non-trawl sectors.</p>
<form id="form6" name="form6" method="post" action="reg_RCA_results.asp">

  <table width="700" border="5">
    <tr>
      <td>RCA Sector:</td>
      <td><select name="RCA_Sector" id="RCA_Sector">
        <option value="Trawl">Trawl</option>
        <option value-"Non-Trawl">Non-Trawl</option>
      </select></td>
</tr>
 <tr>
      <td><input type="submit" name="fr6" id="fr6" value="Submit" /></td>
    </tr>
  </table>
</form>
<p>&nbsp;</p>
<p align="center" class="style4"><a href="query_all_home.asp">Return to Data Retrieval Main Page</a></p> 


</body>
</html>
