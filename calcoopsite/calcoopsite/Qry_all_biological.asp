<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RSSpeciesCodes
Dim RSSpeciesCodes_cmd
Dim RSSpeciesCodes_numRows

Set RSSpeciesCodes_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes order by common_name" 
RSSpeciesCodes_cmd.Prepared = true

Set RSSpeciesCodes = RSSpeciesCodes_cmd.Execute
RSSpeciesCodes_numRows = 0
%>
<%
Dim RSSpeciesCodes0
Dim RSSpeciesCodes0_cmd
Dim RSSpeciesCodes0_numRows

Set RSSpeciesCodes0_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes0_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes0_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes where age_comp='Y' order by common_name" 
RSSpeciesCodes0_cmd.Prepared = true

Set RSSpeciesCodes0 = RSSpeciesCodes0_cmd.Execute
RSSpeciesCodes0_numRows = 0
%>
<%
Dim RSSpeciesCodes1
Dim RSSpeciesCodes1_cmd
Dim RSSpeciesCodes1_numRows

Set RSSpeciesCodes1_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes1_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes1_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.species_codes where len_comp='Y' order by common_name" 
RSSpeciesCodes1_cmd.Prepared = true

Set RSSpeciesCodes1 = RSSpeciesCodes1_cmd.Execute
RSSpeciesCodes1_numRows = 0
%>
<%
Dim RSSpeciesCodes3
Dim RSSpeciesCodes3_cmd
Dim RSSpeciesCodes3_numRows

Set RSSpeciesCodes3_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes3_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes3_cmd.CommandText = "SELECT mark_cat, description FROM calcom.dbo.market_categories where block_summary='Y' order by description" 
RSSpeciesCodes3_cmd.Prepared = true

Set RSSpeciesCodes3 = RSSpeciesCodes3_cmd.Execute
RSSpeciesCodes3_numRows = 0
%>
<%
Dim RSSpeciesCodes4
Dim RSSpeciesCodes4_cmd
Dim RSSpeciesCodes4_numRows

Set RSSpeciesCodes4_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes4_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes4_cmd.CommandText = "SELECT mark_cat, description FROM calcom.dbo.market_categories where trawl_block='Y' order by description" 
RSSpeciesCodes4_cmd.Prepared = true

Set RSSpeciesCodes4 = RSSpeciesCodes4_cmd.Execute
RSSpeciesCodes4_numRows = 0
%>

<%
Dim RSOTOSPECIES
Dim RSOTOSPECIES_cmd
Dim RSOTOSPECIES_numRows

Set RSOTOSPECIES_cmd = Server.CreateObject ("ADODB.Command")
RSOTOSPECIES_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSOTOSPECIES_cmd.CommandText = "select * from calcom.dbo.web_oto_sp_vu order by common_name" 
RSOTOSPECIES_cmd.Prepared = true

Set RSOTOSPECIES = RSOTOSPECIES_cmd.Execute
RSOTOSPECIES_numRows = 0
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Groundfish Data</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling data">
<meta name="description" content="This page is the commercial fishery data page for the California Cooperative Groundfish Survey (CCGS) and CALCOM." />

<style type="text/css">
<!--
body {
	background-color: #CCC;
	text-align: center;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	color: #333;
	font-weight: bold;
}
.style2 {
	color: #0000FF;
	font-weight: bold;
}
.style4 {
	font-size: 24px;
	font-weight: bold;
}
body,td,th {
	color: #000;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<p align="center" class="BasicHeading1">Biological Data Page</p>
<p align="left" class="BasicSubhead">From this page you can access the OTOLITH_INVENTORY table, and the AGE and LENGTH EXPANSIONS. Each set of data which is accessible from this page can be downloaded into a comma delimited, ASCII file. </p>
<p>&nbsp;</p>

<p align="center" class="BasicSubhead style1"><a href="mailto:Don.pearson@noaa.gov">Contact the DBA</a></p>

<p align="center" class="BasicSubhead style1"><a href="documentation.asp">DOCUMENTATION PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="code_list_home.asp">CODE LISTS</a></p>
<p align="center" class="BasicSubHead style1"><a href="default.asp">RETURN TO HOME PAGE</a></p>

<p>&nbsp;</p>
<p align="left" class="BasicSubhead">OTOLITH INVENTORY: The otolith inventory dataset consists of a list of otoliths available in our archives.  The otoliths were collected in large part by the California Commercial Groundfish Survey. The information provide comes from the OTOLITH_INVENTORY table in CALCOM. Some of the otoliths have been aged. We provide a list of species to choose from and the output file contains year and number of otoliths. It is possible to arrange to borrow otoliths from our collection by contacting us. We have nearly 500,000 otoliths from more than 75 species in our collection. Currently we are in the process of transferring the otoliths from coin envelopes to plastic trays, a process that will take many years.
<form id="form9" name="form9" method="post" action="otolith_results.asp">
  <table width="700" border="5">
    <tr>
    <td width="507"><p class="CoolSubhead">Common Name</p></td>
    
    </tr>
    <tr>
      <td><select name="OTOSP" id="OTOSP">
        <%
While (NOT RSOTOSPECIES.EOF)
%>
        <option value="<%=(RSOTOSPECIES.Fields.Item("species").Value)%>"><%=(RSOTOSPECIES.Fields.Item("common_name").Value)%></option>
        <%
  RSOTOSPECIES.MoveNext()
Wend
If (RSOTOSPECIES.CursorType > 0) Then
  RSOTOSPECIES.MoveFirst
Else
  RSOTOSPECIES.Requery
End If
%>
      </select></td>
      <td width="169"><input type="submit" name="fr9" id="fr9" value="Submit" /></td>
    </tr>
  </table>
</form>
<p class="style4">&nbsp;</p>
<p align="left" class="BasicSubhead">AGE AND LENGTH COMPOSITIONS: This dataset contains age and length composition data for many species which have been &quot;expanded&quot; using landings. The information is retrieved by choosing the species. Output file contains: year, port complex, gear group, length (or age), sex, and total number estimated. Direct access to the database can be used to obtain more detailed datasets including: market category and information about how the estimates were obtained. Details on how the ages lengths are &quot;expanded&quot; can be found in the documentation.</p>
<p align="Center" class="CoolSubhead">Age Composition</p>
<form id="form10" name="form10" method="post" action="age_com_RESULTS.asp">
  <table width="700" border="5">
    <tr>
    <td width="507"><p class="CoolSubhead">Common Name</p></td>
    </tr>
    <tr>
      <td><select name="AGESP" id="AGESP">
        <%
While (NOT RSSpeciesCodes0.EOF)
%>
        <option value="<%=(RSSpeciesCodes0.Fields.Item("species").Value)%>"><%=(RSSpeciesCodes0.Fields.Item("common_name").Value)%></option>
        <%
  RSSpeciesCodes0.MoveNext()
Wend
If (RSSpeciesCodes0.CursorType > 0) Then
  RSSpeciesCodes0.MoveFirst
Else
  RSSpeciesCodes0.Requery
End If
%>
      </select></td>
      <td width="169"><input type="submit" name="FR10" id="FR10" value="Submit" /></td>
    </tr>
  </table>
</form>
<p align="center" class="CoolSubhead">Length Composition</p>
<form id="form11" name="form11" method="post" action="len_com_RESULTS.asp">
  <table width="700" border="5">
    <tr>
    <td width="507"><p class="CoolSubhead">Common Name</p></td>
    </tr>
    <tr>
      <td><select name="LENSP" id="LENSP">
        <%
While (NOT RSSpeciesCodes1.EOF)
%>
        <option value="<%=(RSSpeciesCodes1.Fields.Item("species").Value)%>"><%=(RSSpeciesCodes1.Fields.Item("common_name").Value)%></option>
        <%
  RSSpeciesCodes1.MoveNext()
Wend
If (RSSpeciesCodes1.CursorType > 0) Then
  RSSpeciesCodes1.MoveFirst
Else
  RSSpeciesCodes1.Requery
End If
%>
      </select></td>
      <td width="169"><input type="submit" name="FR11" id="FR11" value="Submit" /></td>
    </tr>
  </table>
</form>

</body>
</html>
<%
RSSpeciesCodes.Close()
Set RSSpeciesCodes = Nothing
%>
<%
RSSpeciesCodes1.Close()
Set RSSpeciesCodes1 = Nothing
%>
<%
RSOTOSPECIES.Close()
Set RSOTOSPECIES = Nothing
%>
<%
RSSPECIEScodes3.Close()
Set RSSPECIEScodes3 = Nothing
%>
<%
RSSPECIEScodes4.Close()
Set RSSPECIEScodes4 = Nothing
%>