<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RSSpeciesCodes
Dim RSSpeciesCodes_cmd
Dim RSSpeciesCodes_numRows

Set RSSpeciesCodes_cmd = Server.CreateObject ("ADODB.Command")
RSSpeciesCodes_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RSSpeciesCodes_cmd.CommandText = "SELECT common_name, species FROM calcom.dbo.expanded_landings_species_vu order by common_name" 
RSSpeciesCodes_cmd.Prepared = true

Set RSSpeciesCodes = RSSpeciesCodes_cmd.Execute
RSSpeciesCodes_numRows = 0
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<TITLE>CALCOM - California Commercial Groundfish Data</TITLE>
<meta name="keywords" content="CALCOM, California Commercial Groundfish, CCGS, California, Groundfish, Port sampling data">
<meta name="description" content="This page is the commercial fishery data page for the California Cooperative Groundfish Survey (CCGS) and CALCOM." />

<style type="text/css">
<!--
body {
	background-color: #CCC;
	text-align: center;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
.style2 {
	color: #0000FF;
	font-weight: bold;
}
.style4 {
	font-size: 24px;
	font-weight: bold;
}
-->
</style>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<p align="center" class="BasicHeading1">Retrieve Commercial Landings</p>
<p align="left" class="BasicSubhead">From this page you can commercial landings of groundfish from the CALCOM database.  <strong>Catch Reconstruction data is no longer available from CALCOM</strong>.  To obtain Catch Reconstruction data, contact Don Pearson at don.pearson@noaa.gov or John Field at John.Field@noaa.gov.  After November 2018, Don Pearson will no longer be available to assist you.</p>
<p align="center" class="BasicSubhead style1"><a href="documentation.asp">DATABASE DOCUMENTATION PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="code_list_home.asp">CODE LISTS</a></p>

<p align="center" class="BasicSubhead style1"><a href="query_all_home.asp">RETURN TO DATA ACCESS PAGE</a></p>
<p align="center" class="BasicSubhead style1"><a href="default.asp">RETURN TO HOME PAGE</a></p>
<p align="left" class="BasicSubhead">ANNUAL COMMERCIAL LANDINGS:  This dataset has annual commercial landings from 1969 to the current year for all groundfish.  No Catch Reconstruction Data is presented.

</p>
<form id="form1" name="form1" method="post" action="com_lands_results.asp">
  <table width="700" border="5">
    <tr>
      <td width="507"><p class="CoolSubhead">Common Name</p></td>
      <td width="169"></td>
      </tr>
      <tr>
      <td><select name="COMLANDSP" id="COMLANDSP">
        <%
While (NOT RSSpeciesCodes.EOF)
%>
        <option value="<%=(RSSpeciesCodes.Fields.Item("species").Value)%>"><%=(RSSpeciesCodes.Fields.Item("common_name").Value)%></option>
        <%
  RSSpeciesCodes.MoveNext()
Wend
If (RSSpeciesCodes.CursorType > 0) Then
  RSSpeciesCodes.MoveFirst
Else
  RSSpeciesCodes.Requery
End If
%>
      </select></td>
      <td><input type="submit" name="fr1" id="fr1" value="Submit" /></td>
    </tr>
  </table>
</form>

</body>
</html>
<%
RSSpeciesCodes.Close()
Set RSSpeciesCodes = Nothing
%>
