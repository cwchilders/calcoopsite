<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim rsSampleList__portcomp
rsSampleList__portcomp = "1"
If (Session("port_complex") <> "") Then 
  rsSampleList__portcomp = Session("port_complex")
End If
%>
<%
Dim rsSampleList
Dim rsSampleList_cmd
Dim rsSampleList_numRows

Set rsSampleList_cmd = Server.CreateObject ("ADODB.Command")
rsSampleList_cmd.ActiveConnection = MM_CALCOM_DB_STRING
rsSampleList_cmd.CommandText = "SELECT sampler, pink_ticket, dressed, live_fish, otoliths, comments, size, sample_no, sample_date, cal_port, boat_no, gear, mark_cat, total_wgt, observed_trip, efp_trip, ifq_trip FROM calcom.dbo.current_samples WHERE port_complex=? ORDER BY sample_no" 
rsSampleList_cmd.Prepared = true
rsSampleList_cmd.Parameters.Append rsSampleList_cmd.CreateParameter("param1", 200, 1, 15, rsSampleList__portcomp) ' adVarChar

Set rsSampleList = rsSampleList_cmd.Execute
rsSampleList_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
rsSampleList_numRows = rsSampleList_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample List</title>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 18px;
	font-weight: bold;
}
-->
</style></head>

<body>
<p>&nbsp;</p>
<P class="style1">List of Samples
<form id="form1" name="form1" method="post" action="">
  <table border="1">
    <tr>
      <td>sampler</td>
      <td>sample#</td>
      <td>sample date</td>
      <td>port</td>
      <td>boat#</td>
      <td>mark cat</td>
      <td>gear</td>
      <td>tot wgt</td>
      <td>lrcpt #1</td>
      <td>otos</td>
      <td>live</td>
      <td>observed</td>
      <td>efp</td>
      <td>ifq</td>
      <td>dressed</td>
      <td>size</td>
      <td>comments</td>
    </tr>
    <% While ((Repeat1__numRows <> 0) AND (NOT rsSampleList.EOF)) %>
      <tr>
        <td><%=(rsSampleList.Fields.Item("sampler").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("sample_no").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("sample_date").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("cal_port").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("boat_no").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("mark_cat").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("gear").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("total_wgt").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("pink_ticket").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("otoliths").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("live_fish").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("observed_trip").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("efp_trip").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("ifq_trip").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("dressed").Value)%></td>
        <td><%=(rsSamplelist.Fields.Item("size").Value)%></td>
        <td><%=(rsSampleList.Fields.Item("comments").Value)%></td>
      </tr>
      <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  rsSampleList.MoveNext()
Wend
%>
  </table>
</form>
<p>&nbsp;</p>
</body>
</html>
<%
rsSampleList.Close()
Set rsSampleList = Nothing
%>
