<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RS_SPECIES
Dim RS_SPECIES_cmd
Dim RS_SPECIES_numRows
dim filesys, txtfile
Set RS_SPECIES_cmd = Server.CreateObject ("ADODB.Command")
RS_SPECIES_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RS_SPECIES_cmd.CommandText = "select species, common_name, sci_name, species_grp from calcom.dbo.SPECIES_codes order by common_name" 
RS_SPECIES_cmd.Prepared = true

Set RS_SPECIES = RS_SPECIES_cmd.Execute
RS_SPECIES_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RS_SPECIES_numRows = RS_SPECIES_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Species Codes</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}
.style2 {
	font-size: 24px;
	color: #FF0000;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">Species Codes</p>
<p>&nbsp;</p>
<p class="style1">After clicking the download link, right click on the file and select SAVE AS... Use the back button on your browser to return to the previous page</p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>
<p>&nbsp;</p>
<p class="style1"><a href="user_download/species_code.txt" class="style2">download the file</a></p>

<form id="form1" name="form1" method="post" action="">
  
  <table border="1">
    <tr>
      <td>CALCOM Code</td>
      <td>Common Name</td>
      <td>Sci Name</td>
      <td>Species Group</td>
    </tr>
    <% set filesys = createobject("scripting.filesystemobject")
	   set txtfile= filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/species_code.txt")
	   txtfile.WriteLine("species,common_name,sci_name,species_grp")%>
	   
	   
     <%While ((Repeat1__numRows <> 0) AND (NOT RS_SPECIES.EOF)) %>
      <tr>
        <td><%=(RS_SPECIES.Fields.Item("SPECIES").Value)%></td>
        <td><%=(RS_SPECIES.Fields.Item("common_name").Value)%></td>
        <td><%=(RS_SPECIES.Fields.Item("sci_name").Value)%></td>
        <td><%=(RS_SPECIES.Fields.Item("species_grp").Value)%></td>
      </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.WriteLine(RS_SPECIES.Fields.Item("species").Value &","& RS_SPECIES.Fields.Item("common_name").Value &","& RS_SPECIES.Fields.Item("sci_name").Value &","& RS_SPECIES.Fields.Item("species_grp").Value)
  RS_SPECIES.MoveNext()
Wend
%>
  </table>
</form>

</body>
</html>
<%
RS_SPECIES.Close()
Set RS_SPECIES = Nothing
%>
