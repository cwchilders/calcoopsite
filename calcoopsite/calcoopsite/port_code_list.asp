<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp" -->
<%
Dim RS_PORT
Dim RS_PORT_cmd
Dim RS_PORT_numRows
dim filesys, txtfile
Set RS_PORT_cmd = Server.CreateObject ("ADODB.Command")
RS_PORT_cmd.ActiveConnection = MM_CALCOM_DB_STRING
RS_PORT_cmd.CommandText = "select * from calcom.dbo.PORT_codes order by calcom_code" 
RS_PORT_cmd.Prepared = true

Set RS_PORT = RS_PORT_cmd.Execute
RS_PORT_numRows = 0
%>
<%
Dim Repeat1__numRows
Dim Repeat1__index

Repeat1__numRows = -1
Repeat1__index = 0
RS_PORT_numRows = RS_PORT_numRows + Repeat1__numRows
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Port Codes</title>
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style2 {
	font-size: 24px;
	color: #FF0000;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body>
<p align="center" class="BasicHeading1">Port Codes</p>
<p>&nbsp;</p>
<p class="style1">After clicking the download link, right click on the file and select SAVE AS...Use the back button on your browser to return to the previous page</p>
<p>&nbsp;</p>
<FORM><div align="Center"><INPUT type=button value=" Back to Previous Page" onClick="history.back();"></div></FORM>
<p>&nbsp;</p>
<p class="style1"><a href="user_download/port_code.txt" class="style2">download the file</a></p>
<form id="form1" name="form1" method="post" action="">
  
  <table border="1">
    <tr>
      <td>CALCOM Code</td>
      <td>Description</td>
      <td>Port Complex</td>
      <td>INPFC</td>
    </tr>
    <% set filesys = createobject("scripting.filesystemobject")
	   set txtfile= filesys.createtextfile("c:/inetpub/wwwroot/calcoopsite/user_download/port_code.txt")
	   txtfile.WriteLine("calcom_code,description,port_complex, INPFC")%>
	   
	   
     <%While ((Repeat1__numRows <> 0) AND (NOT RS_PORT.EOF)) %>
      <tr>
        <td><%=(RS_PORT.Fields.Item("calcom_code").Value)%></td>
        <td><%=(RS_PORT.Fields.Item("description").Value)%></td>
        <td><%=(RS_PORT.Fields.Item("port_complex").Value)%></td>
        <td><%=(RS_PORT.Fields.Item("INPFC").Value)%></td>
      </tr>
    <% 
  Repeat1__index=Repeat1__index+1
  Repeat1__numRows=Repeat1__numRows-1
  txtfile.WriteLine(RS_PORT.Fields.Item("calcom_code").Value &","& RS_PORT.Fields.Item("description").Value &","& RS_PORT.Fields.Item("port_complex").Value &","& RS_PORT.Fields.Item("INPFC").Value)
  RS_PORT.MoveNext()
Wend
%>
  </table>
</form>

</body>
</html>
<%
RS_PORT.Close()
Set RS_PORT = Nothing
%>
