<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!--#include file="Connections/CALCOM_DB.asp"-->
<%
Dim MM_editAction
MM_editAction = CStr(Request.ServerVariables("SCRIPT_NAME"))
If (Request.QueryString <> "") Then
  MM_editAction = MM_editAction & "?" & Server.HTMLEncode(Request.QueryString)
End If
' boolean to abort record edit
Dim MM_abortEdit
MM_abortEdit = false
%>
<%
' IIf implementation
Function MM_IIf(condition, ifTrue, ifFalse)
  If condition = "" Then
    MM_IIf = ifFalse
  Else
    MM_IIf = ifTrue
  End If
End Function
%>
<%
If (CStr(Request("MM_insert")) = "data_entry") Then
  If (Not MM_abortEdit) Then
    ' execute the insert
    Dim MM_editCmd

    Set MM_editCmd = Server.CreateObject ("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_CALCOM_DB_STRING
    MM_editCmd.CommandText = "INSERT INTO calcom.dbo.current_samples (sampler, check_me, sample_no, cal_port, boat_no, sample_date, gear, mark_cat, total_wgt, pink_ticket, pink_ticket2, pink_ticket3, otoliths, live_fish, observed_trip, efp_trip, ifq_trip,dressed, [size], comments, port_complex) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" 
    MM_editCmd.Prepared = true
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param1", 201, 1, 15, Request.Form("Sampler_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param2", 201, 1, 1, Request.Form("CHECK_ME")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param3", 201, 1, 11, MM_IIF(Request.Form("SampleNo_text"), Request.Form("SampleNo_text"), null)) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param4", 201, 1, 15, Request.Form("port_list")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param5", 5, 1, -1, Request.Form("boat_text")) ' addouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param6", 135, 1, -1, MM_IIF(Request.Form("ldate_text"), Request.Form("ldate_text"), null)) ' adDBTimeStamp
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param7", 201, 1, 5, Request.Form("GEAR_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param8", 5, 1, -1, MM_IIF(Request.Form("mcat_text"), Request.Form("mcat_text"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param9", 5, 1, -1, MM_IIF(Request.Form("lwgt_text"), Request.Form("lwgt_text"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param10", 201, 1, 7, Request.Form("rcpt1_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param11", 201, 1, 7, Request.Form("rcpt2_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param12", 201, 1, 7, Request.Form("rcpt3_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param13", 201, 1, 1, Request.Form("OTOLITH_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param14", 201, 1, 1, Request.Form("LIVE_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param15", 201, 1, 1, Request.Form("OBSERVED_LIST")) ' adLongVarChar
	MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param16", 201, 1, 1, Request.Form("EFP_LIST")) ' adLongVarChar
	MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param17", 201, 1, 1, Request.Form("IFQ_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param18", 201, 1, 1, Request.Form("DRESSED_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param19", 201, 1, 3, Request.Form("SIZE_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param20", 201, 1, 250, Request.Form("Comments_field")) ' adLongVarChar
	MM_editCmd.parameters.Append MM_editCmd.CreateParameter("param21", 201, 1, 3, session("port_complex"))
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close
    session("xxtxtSpeciesCountC1")=request.form("CL1_CT")
	session("xxtxtSpeciesWeightC1")=request.form("CL1_WGT")
	session("xxtxtSpeciesCountC2")=request.form("CL2_CT")
	session("xxtxtSpeciesWeightC2")=request.form("CL2_WGT")
	Session("Sample_no")=request.form("SampleNo_text")
	Session("PageVisit") = 1
    ' append the query string to the redirect URL
    Dim MM_editRedirectUrl
    MM_editRedirectUrl = "CLUSTER.ASP"
    If (Request.QueryString <> "") Then
      If (InStr(1, MM_editRedirectUrl, "?", vbTextCompare) = 0) Then
        MM_editRedirectUrl = MM_editRedirectUrl & "?" & Request.QueryString
      Else
        MM_editRedirectUrl = MM_editRedirectUrl & "&" & Request.QueryString
      End If
    End If
    Response.Redirect(MM_editRedirectUrl)
  End If
End If
%>
<%
If (CStr(Request("MM_insert")) = "data_entry") Then
  If (Not MM_abortEdit) Then
    ' execute the insert
     Session("xxtxtSpeciesCountC1")=request("CL1_CT")
	 Session("xxtxtSpeciesWeightC1")=request("CL1_WGT")

    Set MM_editCmd = Server.CreateObject ("ADODB.Command")
    MM_editCmd.ActiveConnection = MM_CALCOM_DB_STRING
    MM_editCmd.CommandText = "INSERT INTO calcom.dbo.current_samples (sampler, check_me, sample_no, cal_port, boat_no, sample_date, gear, mark_cat, total_wgt, pink_ticket, pink_ticket2, pink_ticket3, otoliths, live_fish, observed_trip, efp_trip, ifq_trip, dressed, [size], comments, port_complex) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" 
    MM_editCmd.Prepared = true
	
	
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param1", 201, 1, 15, Request.Form("Sampler_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param2", 201, 1, 1, Request.Form("CHECK_ME")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param3", 201, 1, 11, MM_IIF(Request.Form("SampleNo_text"), Request.Form("SampleNo_text"), null)) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param4", 201, 1, 15, Request.Form("port_list")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param5", 5, 1, -1, Request.Form("boat_text")) ' addouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param6", 135, 1, -1, MM_IIF(Request.Form("ldate_text"), Request.Form("ldate_text"), null)) ' adDBTimeStamp
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param7", 201, 1, 5, Request.Form("GEAR_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param8", 5, 1, -1, MM_IIF(Request.Form("mcat_text"), Request.Form("mcat_text"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param9", 5, 1, -1, MM_IIF(Request.Form("lwgt_text"), Request.Form("lwgt_text"), null)) ' adDouble
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param10", 201, 1, 7, Request.Form("rcpt1_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param11", 201, 1, 7, Request.Form("rcpt2_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param12", 201, 1, 7, Request.Form("rcpt3_text")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param13", 201, 1, 1, Request.Form("OTOLITH_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param14", 201, 1, 1, Request.Form("LIVE_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param15", 201, 1, 1, Request.Form("OBSERVED_LIST")) ' adLongVarChar
	MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param16", 201, 1, 1, Request.Form("EFP_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param17", 201, 1, 1, Request.Form("IFQ_LIST")) ' adLongVarChar
	
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param18", 201, 1, 1, Request.Form("DRESSED_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param19", 201, 1, 3, Request.Form("SIZE_LIST")) ' adLongVarChar
    MM_editCmd.Parameters.Append MM_editCmd.CreateParameter("param20", 201, 1, 250, Request.Form("Comments_field")) ' adLongVarChar
	MM_editCmd.parameters.Append MM_editCmd.CreateParameter("param21", 201, 1, 3, session("port_complex"))
    MM_editCmd.Execute
    MM_editCmd.ActiveConnection.Close
	session("xxtxtSpeciesCountC1")=request.form("CL1_CT")
	session("xxtxtSpeciesWeightC1")=request.form("CL1_WGT")
	session("xxtxtSpeciesCountC2")=request.form("CL2_CT")
	session("xxtxtSpeciesWeightC2")=request.form("CL2_WGT")
	Session("Sample_no")=request.form("SampleNo_text")
	Session("PageVisit") = 1
  End If
End If


%>
<%
Dim cc_query
Dim cc_query_cmd
Dim cc_query_numRows

Set cc_query_cmd = Server.CreateObject ("ADODB.Command")
cc_query_cmd.ActiveConnection = MM_CALCOM_DB_STRING
cc_query_cmd.CommandText = "SELECT * FROM calcom.dbo.current_samples" 
cc_query_cmd.Prepared = true

Set cc_query = cc_query_cmd.Execute
cc_query_numRows = 0
%><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Header Entry</title>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	background-color: #CCC;
}
.style1 {
	font-size: 16px;
	font-weight: bold;
}
.style3 {color: rgb(0,0,255); font-weight: bold; font-size: 20pt;}
-->
</style>
<script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
<script src="SpryAssets/SpryValidationSelect.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function MM_validateForm() { //v4.0
  if (document.getElementById){
    var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
    for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=document.getElementById(args[i]);
      if (val) { nm=val.name; if ((val=val.value)!="") {
        if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
          if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
        } else if (test!='R') { num = parseFloat(val);
          if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
          if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
            min=test.substring(8,p); max=test.substring(p+1);
            if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
      } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
    } if (errors) alert('The following error(s) occurred:\n'+errors);
    document.MM_returnValue = (errors == '');
} }
function MM_popupMsg(msg) { //v1.0
  alert(msg);
}

function lettersonly(e)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// alphas and numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return false;
else
   return true;
}


function allowonlynumbers(e)
{
var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);
keychar = keychar.toLowerCase();

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// alphas and numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;
else
{ 
alert("This field accepts only numbers");
return false;
}




<!--

var key;
var keychar;

if (window.event)
   key = window.event.keyCode;
else if (e)
   key = e.which;
else
   return true;
keychar = String.fromCharCode(key);
//alert(keychar);

// control keys
if ((key==null) || (key==0) || (key==8) || 
    (key==9) || (key==13) || (key==27) )
   return true;

// numbers
else if ((("0123456789").indexOf(keychar) > -1))
   return true;
-->
}
//-->
</script>
<link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
<link href="SpryAssets/SpryValidationSelect.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style4 {
	font-size: 9px;
	font-weight: bold;
}
-->
</style>
</head>

<body>

<table width="900" border="1" align="center">
  <caption align="top" class="BasicHeading1">
    Port Sample Data Entry<br />
  </caption>

  <tr>
    <td height="39" class="CoolSubhead"><div align="center">
    <input type="button" name="headhelp" id="headhelp" onclick="MM_openBrWindow('sample_header_help.asp','','scrollbars=yes,width=600,height=500')"value="Page Help" /></div></td>
    <td valign="middle" class="CoolSubhead"></a>
    <div align="center">
    <input type="button" name="samplist" id="samplist" onclick="MM_openBrWindow('sample_list.asp','','scrollbars=yes,width=1000,height=500')"value="Sample List" /></div></td>
    <td><div align="center">
    <input type="button" name="codelist" id="codelist" onclick="MM_openBrWindow('code_list_home.asp','','scrollbars=yes,width=600,height=500')"value="Code List" /></div></td>
    
  </tr>
</table>

<p>NOTE: all fields in BOLD are required!</p>

<form ACTION="<%=MM_editAction%>" method="POST" name="data_entry" id="data_entry" onsubmit="MM_validateForm('Sampler_text','','R','SampleNo_text','','RisNum','ldate_text','','R','mcat_text','','isNum','CL1_WGT','','RisNum','CL1_CT','','RisNum');return document.MM_returnValue">
  <table align="center" width="1200" border="1">
<caption align="top" class="BasicHeading1">
      Header Data Entry
    </caption>
    <tr>
      <td width="68"><strong>Sampler:</strong></td>
      <td><span id="sprytextfield2">
      <input type="text" name="Sampler_text" id="Sampler_text" onKeyPress="return lettersonly(event)"/>
      <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldMinCharsMsg">Minimum number of characters not met.</span><span class="textfieldMaxCharsMsg">Exceeded maximum number of characters.</span></span></td>
      <td width="53">&nbsp;</td>
      <td width="144">&nbsp;</td>
      <td width="63">&nbsp;</td>
      <td width="144">&nbsp;</td>
      <td width="173">&nbsp;</td>
      <td width="144">&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Sample#:</strong></td>
      <td><span id="sprytextfield3">
      <input type="text" name="SampleNo_text" id="SampleNo_text" onKeyPress="return allowonlynumbers(event) " />
      <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldMinCharsMsg">Minimum number of characters not met.</span><span class="textfieldMaxCharsMsg">Exceeded maximum number of characters.</span></span></td>
      <td><strong>Port:</strong></td>
      <td><span id="spryselect1">
        <select name="port_list" id="port_list">
        <option value="" selected="selected"> </option>
        <option value="ALB">ALB</option>
        <option value="AVL">AVL</option>
        <option value="BCR">BCR</option>
        <option value="BDG">BDG</option>
        <option value="BOL">BOL</option>
        <option value="BRG">BRG</option>
        <option value="CRS">CRS</option>
        <option value="CRZ">CRZ</option>
        <option value="DNA">DNA</option>
        <option value="ERK">ERK</option>
        <option value="FLN">FLN</option>
        <option value="MCR">MCR</option>
        <option value="MNT">MNT</option>
        <option value="MOS">MOS</option>
        <option value="MRO">MRO</option>
        <option value="NWB">NWB</option>
        <option value="OCA">OCA</option>
        <option value="OCM">OCM</option>
        <option value="OCN">OCN</option>
        <option value="OLA">OLA</option>
        <option value="OSD">OSD</option>
        <option value="OSL">OSL</option>
        <option value="OXN">OXN</option>
        <option value="PRN">PRN</option>
        <option value="SB">SB</option>
        <option value="SF">SF</option>
        <option value="SIM">SIM</option>
        <option value="SP">SP</option>
        <option value="TRM">TRM</option>
        <option value="VEN">VEN</option>
        <option value="WLM">WLM</option>
       </select>
      <span class="selectRequiredMsg">Please select an item.</span></span></td>
      <td>Boat#:</td>
      <td><input name="boat_text" type="text" id="boat_text" maxlength="6" /></td>
      <td>Land Date (mm/dd/yyyy)</span></td>
      <td><span id="sprytextfield1">
      <input type="text" name="ldate_text" id="ldate_text" />
      <span class="textfieldRequiredMsg">A value is required.</span><span class="textfieldInvalidFormatMsg">Invalid date format.</span></span></td>
    </tr>
    <tr>
      <td><strong>Gear:</strong></td>
      <td><span id="spryselect2">
        <select name="GEAR_LIST" id="GEAR_LIST">
        <option value="" selected="selected"> </option>
        <option value="DGN">DGN</option>
        <option value="DNT">DNT</option>
        <option value="DVG">DVG</option>
        <option value="FPT">FPT</option>
        <option value="FTS">FTS</option>
        <option value="GFL">GFL</option>
        <option value="GFS">GFS</option>
        <option value="GFT">GFT</option>
        <option value="GLN">GLN</option>
        <option value="HKL">HKL</option>
        <option value="LGL">LGL</option>
        <option value="MDT">MDT</option>
        <option value="OTH">OTH</option>
        <option value="RLT">RLT</option>
        <option value="SST">SST</option>
        <option value="TRL">TRL</option>
        <option value="UNK">UNK</option>
        <option value="VHL">VHL</option>
        </select>
      <span class="selectRequiredMsg">Please select an item.</span></span></td>
      <td>Mark Cat:</td>
      <td><input name="mcat_text" type="text" id="mcat_text" maxlength="3" /></td>
      <td>Land Wgt:</td>
      <td><input name="lwgt_text" type="text" id="lwgt_text" maxlength="6" /></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Rcpt#1:</td>
      <td><input name="rcpt1_text" type="text" id="rcpt1_text" maxlength="10" /></td>
      <td>Rcpt#2:</td>
      <td><input name="rcpt2_text" type="text" id="rcpt2_text" maxlength="10" /></td>
      <td>Rcpt#3:</td>
      <td><input name="rcpt3_text" type="text" id="rcpt3_text" maxlength="10" /></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><strong>Otoliths:</strong></td>
      <td><select name="OTOLITH_LIST" id="OTOLITH_LIST">
        <option value="Y">Y</option>
        <option value="N" selected="selected">N</option>
      </select></td>
      <td><strong>Live:</strong></td>
      <td><select name="LIVE_LIST" id="LIVE_LIST">
        <option value="Y">Y</option>
        <option value="P">P</option>

        <option value="N" selected="selected">N</option>
      </select></td>
      <td>Observed:</td>
      <td><select name="OBSERVED_LIST" id="OBSERVED_LIST">
        <option value="Y">Y</option>
        <option value="N" selected="selected">N</option>
        <option value="C">C</option>
        <option value="U">UNKNOWN</option>
      </select></td>
      <td>EFP Trip:</td>
      <td><select name="EFP_LIST" id="EFP_LIST">
          <option selected value="N">No</option>
          <option value="Y">Yes</option>
          <option value="U">UNKNOWN</option>
          </select></td>
    </tr>
    <tr>
    <td>IFQ Trip:</td>
      <td><select name="IFQ_LIST" id="IFQ_LIST">
          <option selected value="N">N</option>
          <option value="Y">Y</option>
          <option value="U">UNKNOWN</option>
          </select></td>
      <td>Dressed:</td>
      <td><select name="DRESSED_LIST" id="DRESSED_LIST">
        <option value="Y">Y</option>
        <option value="N" selected="selected">N</option>
        <option value="W">WINGS</option>
      </select></td>
      <td>Size:</td>
      <td><select name="SIZE_LIST" id="SIZE_LIST">
        <option value="OR">OCEAN RUN</option>
        <option value-"XXL">XXL</option>
        <option value="XL">XL</option>
        <option value="L">L</option>
        <option value="M">M</option>
        <option value="S">S</option>
        <option value="XS">XS</option>
        <option value="XXS">XXS</option>
        <option value="UNK">UNKNOWN</option>
        <option value="DIS">DISCARD</option>
      </select></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>Comments:</td>
      <td><textarea name="Comments_field" cols="30" rows="3" id="Comments_field" colspan="2"></textarea></td>
      <td>Check Me:</td>
      <td> <select name="check_me" id="CHECK_ME" >
        <option value="N" selected="selected">N</option>
        <option value="Y">Y</option>
      </select></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="style1">CLUSTER DATA</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><div align="right"><strong>Cluster #1</strong></div></td>
      <td><strong>Weight:</strong></td>
      <td><input name="CL1_WGT" type="text" id="CL1_WGT" maxlength="3"/></td>
      <td><strong># of Species:</strong></td>
      <td><input name="CL1_CT" type="text" id="CL1_CT"/></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><div align="right">Cluster #2</div></td>
      <td>Weight:</td>
      <td><input name="CL2_WGT"/></td>
      <td># of Species:</td>
      <td><input name="CL2_CT"/></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="SUBMIT_BUTTON" id="SUBMIT_BUTTON" value="Submit and Enter Cluster Data" /></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>
  <input type="hidden" name="MM_insert" value="data_entry" />
  

<p>&nbsp;</p>
</form>

<p align="center" class="style3"><a href="PortSamplerPage.asp">Return To Port Sampler Homepage</a></p>

<p align="center" class="style3">&nbsp;</p>
<script type="text/javascript">
<!--
var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "date", {format:"mm/dd/yyyy", validateOn:["blur"]});
var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "none", {validateOn:["blur"], minChars:3, maxChars:12});
var sprytextfield3 = new Spry.Widget.ValidationTextField("sprytextfield3", "none", {validateOn:["blur"], minChars:3, maxChars:5});
var spryselect1 = new Spry.Widget.ValidationSelect("spryselect1", {validateOn:["blur"]});
var spryselect2 = new Spry.Widget.ValidationSelect("spryselect2", {validateOn:["blur"]});
//-->
</script>
</body>
</html>
<%
cc_query.Close()
Set cc_query = Nothing
%>
