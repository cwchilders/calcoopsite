﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Commercial Regulations Home Page</title>

<style type="text/css">
<!--
body {
	background-color: #CCC;
	text-align: center;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
.style1 {
	color: #FF0000;
	font-weight: bold;
}
.style2 {
	color: #0000FF;
    font-size:18px
	font-weight: bold;
	font-size: 16pt;
}
.style4 {
	font-size: 24px;
	font-weight: bold;
}
-->
</style>

</head>

<body>

<p align="center" class="BasicHeading1">Commercial Regulations Home Page</p>
<p>&nbsp;</p>

<p align="left" class="BasicSubhead"> This database contains federal commercial groundfish regulations from 1983 through the most recent complete year. This will be updated annually. This database contains three types of data: regulations governing trip  limits; Rockfish Conservation Area (RCA) data; and OY, ABCs for groundfish species  and species groups managed by the Pacific Fisheries Management Council. This  database is NOT to be used for law enforcement purposes - its sole purpose is  for support of research and stock assessments.
  The data are stored in a series of SQL Server tables in the CALCOM database.  These data come from Federal Registers and from documents provided by John Devore (PSMFC, Portland, OR).  The tables use a technology called FullText Indexing to search large text fields.  Obtaining data from the tables can be done in a variety of ways.  The majority of data retrievals can be made via this site and the data can be downloaded to text files by  the user.  For those with good SQL  skills, they can connect directly to the database using a variety of software  packages including:  R, Excel, and other  ODBC compliant software packages.  Finally, for special requests, the user can contact Don Pearson and request a data  retrieval.
</p>
<p align="left" class="BasicSubhead">This database will not be updated after December 2017.  PacFIN may replace store regulations in the future.</p>
<p align="center" class="BasicSubhead style2"><a href="download/regulation_documentation.pdf">Regulation Documentation</a></p>

<p align="center" class="BasicSubhead style2"><a href="Regulation_retrieval_main.asp">Data Retrievals</a></p>

<p align="center" class="BasicSubhead style2"><a href="query_all_home.asp">Return to Data Retrieval Main Page</a></p>
</body>

</html>
