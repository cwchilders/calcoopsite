<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Fish Help</title>

<style type="text/css">
<!--
body {
	background-color: #CCC;
}
-->
</style>
<link href="User Login_IE4.css" rel="stylesheet" type="text/css" />
</head>

<body class="BasicHeading1">
<div align="center" class="BasicHeading1">FISH DATA ENTRY HELP</div>
<p class="BasicSubhead">All lengths are forklength (mm) except for skates (disk width) and sharks (total length).</p>
<p class="BasicSubhead">Sex 1=male, 2=female, 9=unknown</p>
<p class="BasicSubhead">Weights are to the nearest tenth of a pound.</p>
<p class="BasicSubhead">Only length, sex, weight and maturity can be edited.</p>
<p class="BasicSubhead">To change the number of fish or species codes, the whole sample must be deleted and reentered.</p>
<p class="CoolSubhead">&nbsp;</p>
<p class="CoolSubhead">&nbsp;</p>
</body>
</html>
